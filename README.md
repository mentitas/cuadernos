# Cuadernos

Acá voy a ir subiendo mis cuadernos virtuales, que basicamente son los ejercicios que hice durante las cursadas.

Para abrir los archivos .xoj probablemente tengas que usar Xournal (el programa desde donde
los escribí). Es open-source y se puede descargar desde acá: http://xournal.sourceforge.net.
Una vez abierto desde xournal, puede exportarse a pdf.