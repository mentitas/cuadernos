!classDefinition: #TestDePerforación category: #'ISW1-Perforaciones'!
TestCase subclass: #TestDePerforación
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!TestDePerforación methodsFor: 'test'!
test01CuandoExcavaSueloArenosoConMechaSoftRemueveCapaArenosa
	| temp1 temp2 |
	temp2 := {CapaDeSuelo arenoso. CapaDeSuelo tierra. CapaDeSuelo concretoConResistencia: 5}.
	temp1 := Perforadora sobreSuelo: temp2.
	temp1 ponerMechaSoft.
	temp1 darGolpeDeTaladro.
	self
		assert: CapaDeSuelo tierra
		equals: temp1 capaDeSueloInmediata.! !

!TestDePerforación methodsFor: 'test'!
test02CuandoExcavaSueloTierraConMechaSoftNoModificaElSuelo
	| temp1 temp2 |
	temp2 := {CapaDeSuelo tierra. CapaDeSuelo concretoConResistencia: 5}.
	temp1 := Perforadora sobreSuelo: temp2.
	temp1 ponerMechaSoft.
	temp1 darGolpeDeTaladro.
	self
		assert: CapaDeSuelo tierra
		equals: temp1 capaDeSueloInmediata.! !

!TestDePerforación methodsFor: 'test'!
test03CuandoExcavaConcretoConMechaSoftSeRompeLaMecha
	| temp1 temp2 |
	temp2 := {CapaDeSuelo concretoConResistencia: 5. CapaDeSuelo tierra}.
	temp1 := Perforadora sobreSuelo: temp2.
	temp1 ponerMechaSoft.
	self
		should: [ temp1 darGolpeDeTaladro ]
		raise: Error
		withExceptionDo: [ :argm3_3 |
			self
				assert: 'Se rompió'
				equals: argm3_3 messageText.
			self assert: temp1 conMechaRota ].! !

!TestDePerforación methodsFor: 'test'!
test04CuandoExcavaSueloArenosoConMechaWidiaRemueveCapaArenosa
	| temp1 temp2 |
	temp2 := {CapaDeSuelo arenoso. CapaDeSuelo tierra. CapaDeSuelo concretoConResistencia: 5}.
	temp1 := Perforadora sobreSuelo: temp2.
	temp1 ponerMechaWidia.
	temp1 darGolpeDeTaladro.
	self
		assert: CapaDeSuelo tierra
		equals: temp1 capaDeSueloInmediata.! !

!TestDePerforación methodsFor: 'test'!
test05CuandoExcavaSueloTierraConMechaWidiaEnDosImpactosTransformaLaCapaEnArenoso
	| temp1 temp2 |
	temp2 := {CapaDeSuelo tierra. CapaDeSuelo concretoConResistencia: 5}.
	temp1 := Perforadora sobreSuelo: temp2.
	temp1 ponerMechaWidia.
	temp1 darGolpeDeTaladro.
	temp1 darGolpeDeTaladro.
	self
		assert: CapaDeSuelo arenoso
		equals: temp1 capaDeSueloInmediata.! !

!TestDePerforación methodsFor: 'test'!
test06CuandoExcavaSueloConcretoConMechaWidiaEnTresImpactosRompeLaMecha
	| temp1 temp2 |
	temp2 := {CapaDeSuelo concretoConResistencia: 5. CapaDeSuelo tierra}.
	temp1 := Perforadora sobreSuelo: temp2.
	temp1 ponerMechaWidia.
	temp1 darGolpeDeTaladro.
	temp1 darGolpeDeTaladro.
	self
		should: [ temp1 darGolpeDeTaladro ]
		raise: Error
		withExceptionDo: [ :argm5_3 |
			self
				assert: 'Se rompió'
				equals: argm5_3 messageText.
			self assert: temp1 conMechaRota ].! !

!TestDePerforación methodsFor: 'test'!
test07CuandoExcavaSueloArenosoConMechaDiamanteRompeLaMecha
	| temp1 temp2 |
	temp2 := {CapaDeSuelo arenoso. CapaDeSuelo tierra. CapaDeSuelo concretoConResistencia: 5}.
	temp1 := Perforadora sobreSuelo: temp2.
	temp1 ponerMechaDiamante.
	self
		should: [ temp1 darGolpeDeTaladro ]
		raise: Error
		withExceptionDo: [ :argm3_3 |
			self
				assert: 'Se rompió'
				equals: argm3_3 messageText.
			self assert: temp1 conMechaRota ].! !

!TestDePerforación methodsFor: 'test'!
test08CuandoExcavaSueloTierraConMechaDiamanteRemueveCapa
	| temp1 temp2 |
	temp2 := {CapaDeSuelo tierra. CapaDeSuelo concretoConResistencia: 5}.
	temp1 := Perforadora sobreSuelo: temp2.
	temp1 ponerMechaDiamante.
	temp1 darGolpeDeTaladro.
	self
		assert: (CapaDeSuelo concretoConResistencia: 5)
		equals: temp1 capaDeSueloInmediata.! !

!TestDePerforación methodsFor: 'test'!
test09CuandoExcavaSueloConcretoConMechaDiamanteBajaUnDecimoDeResistencia
	| temp1 temp2 |
	temp2 := {CapaDeSuelo concretoConResistencia: 5. CapaDeSuelo tierra}.
	temp1 := Perforadora sobreSuelo: temp2.
	temp1 ponerMechaDiamante.
	temp1 darGolpeDeTaladro.
	temp1 darGolpeDeTaladro.
	self
		assert: (CapaDeSuelo concretoConResistencia: 4)
		equals: temp1 capaDeSueloInmediata.! !


!classDefinition: #CapaDeSuelo category: #'ISW1-Perforaciones'!
Object subclass: #CapaDeSuelo
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!


!CapaDeSuelo methodsFor: 'comparing'!
= arg1
	^ arg1 class = self class.! !


!CapaDeSuelo methodsFor: 'bajar resistencia'!
sinResistencia
	self subclassResponsibility.! !


!CapaDeSuelo methodsFor: 'recibir golpes'!
recibirGolpeDeTaladroCon: arg1
	self subclassResponsibility.! !


!CapaDeSuelo methodsFor: 'accessing'!
resistencia
	self subclassResponsibility.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CapaDeSuelo class' category: #'ISW1-Perforaciones'!
CapaDeSuelo class
	instanceVariableNames: ''!

!CapaDeSuelo class methodsFor: 'instance creation'!
arenoso
	^ Arenoso new.! !

!CapaDeSuelo class methodsFor: 'instance creation'!
concretoConResistencia: arg1
	^ Concreto new initializeConcretoConResistencia: arg1.! !

!CapaDeSuelo class methodsFor: 'instance creation'!
tierra
	^ Tierra new.! !


!classDefinition: #Arenoso category: #'ISW1-Perforaciones'!
CapaDeSuelo subclass: #Arenoso
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Arenoso methodsFor: 'recibir golpes'!
recibirGolpeDeTaladroCon: arg1
	arg1 darGolpeDeTaladroSobreSueloArenoso.! !


!Arenoso methodsFor: 'accessing'!
resistencia
	^ 0.! !


!Arenoso methodsFor: 'bajar resistencia'!
sinResistencia
	^ true.! !


!classDefinition: #Concreto category: #'ISW1-Perforaciones'!
CapaDeSuelo subclass: #Concreto
	instanceVariableNames: 'resistencia'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Concreto methodsFor: 'recibir golpes'!
recibirGolpeDeTaladroCon: arg1
	arg1 darGolpeDeTaladroSobreSueloDeConcreto.! !


!Concreto methodsFor: 'accessing'!
resistencia
	^ resistencia.! !


!Concreto methodsFor: 'comparing'!
= arg1
	^ (arg1 isKindOf: self class) and: [ resistencia = arg1 resistencia ].! !


!Concreto methodsFor: 'bajar resistencia'!
bajaUnDecimoDeResistencia
	resistencia := resistencia - 1.! !

!Concreto methodsFor: 'bajar resistencia'!
sinResistencia
	^ false.! !


!Concreto methodsFor: 'initialization'!
initializeConcretoConResistencia: arg1
	resistencia := arg1.! !


!classDefinition: #Tierra category: #'ISW1-Perforaciones'!
CapaDeSuelo subclass: #Tierra
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Tierra methodsFor: 'recibir golpes'!
recibirGolpeDeTaladroCon: arg1
	arg1 darGolpeDeTaladroSobreSueloDeTierra.! !


!Tierra methodsFor: 'accessing'!
resistencia
	^ 0.! !


!Tierra methodsFor: 'bajar resistencia'!
sinResistencia
	^ true.! !


!classDefinition: #Mecha category: #'ISW1-Perforaciones'!
Object subclass: #Mecha
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Mecha methodsFor: 'as yet unclassified'!
esMechaRota
	^ self subclassResponsibility.! !

!Mecha methodsFor: 'as yet unclassified'!
perforarCon: arg1
	arg1 darGolpeDeTaladroConMecha.! !

!Mecha methodsFor: 'as yet unclassified'!
perforarSobreSueloArenosoCon: arg1
	self subclassResponsibility.! !

!Mecha methodsFor: 'as yet unclassified'!
perforarSobreSueloDeConcretoCon: arg1
	self subclassResponsibility.! !

!Mecha methodsFor: 'as yet unclassified'!
perforarSobreSueloDeTierraCon: arg1
	self subclassResponsibility.! !


!classDefinition: #Diamante category: #'ISW1-Perforaciones'!
Mecha subclass: #Diamante
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Diamante methodsFor: 'as yet unclassified'!
esMechaRota
	^ false.! !

!Diamante methodsFor: 'as yet unclassified'!
perforarSobreSueloArenosoCon: arg1
	arg1 darGolpeDeTaladroSobreSueloArenosoConMechaDiamante.! !

!Diamante methodsFor: 'as yet unclassified'!
perforarSobreSueloDeConcretoCon: arg1
	arg1 darGolpeDeTaladroSobreConcretoConMechaDiamante.! !

!Diamante methodsFor: 'as yet unclassified'!
perforarSobreSueloDeTierraCon: arg1
	arg1 darGolpeDeTaladroSobreSueloDeTierraConMechaDiamante.! !


!classDefinition: #Rota category: #'ISW1-Perforaciones'!
Mecha subclass: #Rota
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Rota methodsFor: 'as yet unclassified'!
esMechaRota
	^ true.! !

!Rota methodsFor: 'as yet unclassified'!
perforarSobreSueloArenosoCon: arg1
	arg1 darGolpeDeTaladroConMechaRota.! !

!Rota methodsFor: 'as yet unclassified'!
perforarSobreSueloDeConcretoCon: arg1
	arg1 darGolpeDeTaladroConMechaRota.! !

!Rota methodsFor: 'as yet unclassified'!
perforarSobreSueloDeTierraCon: arg1
	arg1 darGolpeDeTaladroConMechaRota.! !


!classDefinition: #Soft category: #'ISW1-Perforaciones'!
Mecha subclass: #Soft
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Soft methodsFor: 'as yet unclassified'!
esMechaRota
	^ false.! !

!Soft methodsFor: 'as yet unclassified'!
perforarSobreSueloArenosoCon: arg1
	arg1 darGolpeDeTaladroSobreSueloArenosoConMechaSoft.! !

!Soft methodsFor: 'as yet unclassified'!
perforarSobreSueloDeConcretoCon: arg1
	arg1 darGolpeDeTaladroSobreConcretoConMechaSoft.! !

!Soft methodsFor: 'as yet unclassified'!
perforarSobreSueloDeTierraCon: arg1
	arg1 darGolpeDeTaladroSobreSueloDeTierraConMechaSoft.! !


!classDefinition: #Widia category: #'ISW1-Perforaciones'!
Mecha subclass: #Widia
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Widia methodsFor: 'as yet unclassified'!
esMechaRota
	^ false.! !

!Widia methodsFor: 'as yet unclassified'!
perforarSobreSueloArenosoCon: arg1
	arg1 darGolpeDeTaladroSobreSueloArenosoConMechaWidia.! !

!Widia methodsFor: 'as yet unclassified'!
perforarSobreSueloDeConcretoCon: arg1
	arg1 darGolpeDeTaladroSobreConcretoConMechaWidia.! !

!Widia methodsFor: 'as yet unclassified'!
perforarSobreSueloDeTierraCon: arg1
	arg1 darGolpeDeTaladroSobreSueloDeTierraConMechaWidia.! !


!classDefinition: #Perforadora category: #'ISW1-Perforaciones'!
Object subclass: #Perforadora
	instanceVariableNames: 'tipoDeSuelo mechaPuesta vecesQueGolpeoCapaActual capasDeSueloDebajo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Perforadora methodsFor: 'accessing'!
capaDeSueloInmediata
	^ capasDeSueloDebajo first.! !

!Perforadora methodsFor: 'accessing'!
conMechaRota
	^ mechaPuesta esMechaRota.! !

!Perforadora methodsFor: 'accessing'!
sinMecha
	^ mechaPuesta = #SinMecha.! !


!Perforadora methodsFor: 'cambiar capa'!
pasarAProximaCapa
	vecesQueGolpeoCapaActual := 0.
	capasDeSueloDebajo := capasDeSueloDebajo allButFirst.
	capasDeSueloDebajo ifEmpty: [ ^ self ].! !

!Perforadora methodsFor: 'cambiar capa'!
transformarCapaEnArenoso
	capasDeSueloDebajo := capasDeSueloDebajo allButFirst asOrderedCollection
		addFirst: CapaDeSuelo arenoso;
		yourself.! !


!Perforadora methodsFor: 'colocar mecha'!
ponerMechaDiamante
	mechaPuesta := Diamante new.! !

!Perforadora methodsFor: 'colocar mecha'!
ponerMechaSoft
	mechaPuesta := Soft new.! !

!Perforadora methodsFor: 'colocar mecha'!
ponerMechaWidia
	mechaPuesta := Widia new.! !


!Perforadora methodsFor: 'initialization'!
initializeSobreSuelo: arg1
	capasDeSueloDebajo := arg1.
	vecesQueGolpeoCapaActual := 0.! !


!Perforadora methodsFor: 'perforar'!
darGolpeDeTaladro
	mechaPuesta perforarCon: self.! !

!Perforadora methodsFor: 'perforar'!
darGolpeDeTaladroConMecha
	vecesQueGolpeoCapaActual := vecesQueGolpeoCapaActual + 1.
	self capaDeSueloInmediata recibirGolpeDeTaladroCon: self.! !

!Perforadora methodsFor: 'perforar'!
darGolpeDeTaladroConMechaRota
	^ self error: 'No puede perforar'.! !

!Perforadora methodsFor: 'perforar'!
darGolpeDeTaladroSinMecha
	^ self error: 'No puede perforar'.! !


!Perforadora methodsFor: 'perforar - arena'!
darGolpeDeTaladroSobreSueloArenoso
	mechaPuesta perforarSobreSueloArenosoCon: self.! !

!Perforadora methodsFor: 'perforar - arena'!
darGolpeDeTaladroSobreSueloArenosoConMechaDiamante
	mechaPuesta := Rota new.
	self error: 'Se rompió'.! !

!Perforadora methodsFor: 'perforar - arena'!
darGolpeDeTaladroSobreSueloArenosoConMechaSoft
	^ self pasarAProximaCapa.! !

!Perforadora methodsFor: 'perforar - arena'!
darGolpeDeTaladroSobreSueloArenosoConMechaWidia
	^ self pasarAProximaCapa.! !


!Perforadora methodsFor: 'perforar - concreto'!
darGolpeDeTaladroSobreConcretoConMechaDiamante
	vecesQueGolpeoCapaActual even ifTrue: [ capasDeSueloDebajo first bajaUnDecimoDeResistencia ].
	capasDeSueloDebajo first sinResistencia ifTrue: [ ^ self transformarCapaEnArenoso ].
	^ vecesQueGolpeoCapaActual.! !

!Perforadora methodsFor: 'perforar - concreto'!
darGolpeDeTaladroSobreConcretoConMechaSoft
	mechaPuesta := Rota new.
	self error: 'Se rompió'.! !

!Perforadora methodsFor: 'perforar - concreto'!
darGolpeDeTaladroSobreConcretoConMechaWidia
	vecesQueGolpeoCapaActual = 3 ifTrue: [
		mechaPuesta := Rota new.
		self error: 'Se rompió' ].! !

!Perforadora methodsFor: 'perforar - concreto'!
darGolpeDeTaladroSobreSueloDeConcreto
	mechaPuesta perforarSobreSueloDeConcretoCon: self.! !


!Perforadora methodsFor: 'perforar - tierra'!
darGolpeDeTaladroSobreSueloDeTierra
	mechaPuesta perforarSobreSueloDeTierraCon: self.! !

!Perforadora methodsFor: 'perforar - tierra'!
darGolpeDeTaladroSobreSueloDeTierraConMechaDiamante
	^ self pasarAProximaCapa.! !

!Perforadora methodsFor: 'perforar - tierra'!
darGolpeDeTaladroSobreSueloDeTierraConMechaSoft
	^ self.! !

!Perforadora methodsFor: 'perforar - tierra'!
darGolpeDeTaladroSobreSueloDeTierraConMechaWidia
	vecesQueGolpeoCapaActual = 2 ifTrue: [ ^ self transformarCapaEnArenoso ].! !


!Perforadora methodsFor: 'sobre qué suelo'!
sobreSueloArenoso
	^ capasDeSueloDebajo first esArenoso.! !

!Perforadora methodsFor: 'sobre qué suelo'!
sobreSueloConcreto
	^ capasDeSueloDebajo first esConcreto.! !

!Perforadora methodsFor: 'sobre qué suelo'!
sobreSueloTierra
	^ capasDeSueloDebajo first esTierra.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Perforadora class' category: #'ISW1-Perforaciones'!
Perforadora class
	instanceVariableNames: ''!

!Perforadora class methodsFor: 'instance creation'!
sobreSuelo: arg1
	^ self new initializeSobreSuelo: arg1.! !


!classDefinition: #SinMecha category: #'ISW1-Perforaciones'!
Object subclass: #SinMecha
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!SinMecha methodsFor: 'as yet unclassified'!
perforarCon: arg1
	arg1 darGolpeDeTaladroSinMecha.! !
