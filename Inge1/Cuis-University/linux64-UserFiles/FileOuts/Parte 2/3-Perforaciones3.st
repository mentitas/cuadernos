!classDefinition: #TestDePerforación category: #'ISW1-Perforaciones'!
TestCase subclass: #TestDePerforación
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!TestDePerforación methodsFor: 'test' stamp: 'ARM 5/5/2023 17:58:53'!
test01CuandoExcavaSueloArenosoConMechaSoftRemueveCapaArenosa

	| excavadora sueloAExcavar |
	
	sueloAExcavar := {
			CapaDeSuelo arenoso.
			CapaDeSuelo tierra.
			CapaDeSuelo concretoConResistencia: 5 } .
	
	excavadora := Perforadora sobreSuelo: sueloAExcavar.
	
	excavadora ponerMechaSoft.
	
	excavadora darGolpeDeTaladro.
	
	self assert: CapaDeSuelo tierra equals: excavadora capaDeSueloInmediata
	
	! !

!TestDePerforación methodsFor: 'test' stamp: 'ARM 5/5/2023 17:58:58'!
test02CuandoExcavaSueloTierraConMechaSoftNoModificaElSuelo

	| excavadora sueloAExcavar |
	
	sueloAExcavar := {
			CapaDeSuelo tierra.
			CapaDeSuelo concretoConResistencia: 5 } .
	
	excavadora := Perforadora sobreSuelo: sueloAExcavar.
	
	excavadora ponerMechaSoft.
	
	excavadora darGolpeDeTaladro.
	
	self assert: CapaDeSuelo tierra equals: excavadora capaDeSueloInmediata
	
	! !

!TestDePerforación methodsFor: 'test' stamp: 'ARM 5/5/2023 18:02:52'!
test03CuandoExcavaConcretoConMechaSoftSeRompeLaMecha

	| excavadora sueloAExcavar |
	
	sueloAExcavar := {
			CapaDeSuelo concretoConResistencia: 5.
			CapaDeSuelo tierra.} .
	
	excavadora := Perforadora sobreSuelo: sueloAExcavar.
	
	excavadora ponerMechaSoft.
	
	self should: [excavadora darGolpeDeTaladro]
		raise: Error
		withExceptionDo: [:unError |
			self assert: 'Se rompió' equals: unError messageText.
			self assert: excavadora conMechaRota.].
	! !

!TestDePerforación methodsFor: 'test' stamp: 'ARM 5/5/2023 17:59:05'!
test04CuandoExcavaSueloArenosoConMechaWidiaRemueveCapaArenosa

	| excavadora sueloAExcavar |
	
	sueloAExcavar := {
			CapaDeSuelo arenoso.
			CapaDeSuelo tierra.
			CapaDeSuelo concretoConResistencia: 5 } .
	
	excavadora := Perforadora sobreSuelo: sueloAExcavar.
	
	excavadora ponerMechaWidia.
	
	excavadora darGolpeDeTaladro.
	
	self assert: CapaDeSuelo tierra equals: excavadora capaDeSueloInmediata
	
	! !

!TestDePerforación methodsFor: 'test' stamp: 'ARM 5/5/2023 17:59:09'!
test05CuandoExcavaSueloTierraConMechaWidiaEnDosImpactosTransformaLaCapaEnArenoso

	| excavadora sueloAExcavar |
	
	sueloAExcavar := {
			CapaDeSuelo tierra.
			CapaDeSuelo concretoConResistencia: 5 } .
	
	excavadora := Perforadora sobreSuelo: sueloAExcavar.
	
	excavadora ponerMechaWidia.
	
	excavadora darGolpeDeTaladro.
	excavadora darGolpeDeTaladro.	
	
	self assert: CapaDeSuelo arenoso equals: excavadora capaDeSueloInmediata
	
	! !

!TestDePerforación methodsFor: 'test' stamp: 'ARM 5/5/2023 18:02:45'!
test06CuandoExcavaSueloConcretoConMechaWidiaEnTresImpactosRompeLaMecha

	| excavadora sueloAExcavar |
	
	sueloAExcavar := {
			CapaDeSuelo concretoConResistencia: 5.	
			CapaDeSuelo tierra.
} .
	
	excavadora := Perforadora sobreSuelo: sueloAExcavar.
	
	excavadora ponerMechaWidia.
	
	excavadora darGolpeDeTaladro.
	excavadora darGolpeDeTaladro.	
	
	self should: [excavadora darGolpeDeTaladro]
		raise: Error
		withExceptionDo: [:unError |
			self assert: 'Se rompió' equals: unError messageText.
			self assert: excavadora conMechaRota.].
	
	! !

!TestDePerforación methodsFor: 'test' stamp: 'ARM 5/5/2023 18:03:00'!
test07CuandoExcavaSueloArenosoConMechaDiamanteRompeLaMecha

	| excavadora sueloAExcavar |
	
	sueloAExcavar := {
			CapaDeSuelo arenoso.
			CapaDeSuelo tierra.
			CapaDeSuelo concretoConResistencia: 5 } .
	
	excavadora := Perforadora sobreSuelo: sueloAExcavar.
	
	excavadora ponerMechaDiamante.
	
	self should: [excavadora darGolpeDeTaladro]
		raise: Error
		withExceptionDo: [:unError |
			self assert: 'Se rompió' equals: unError messageText.
			self assert: excavadora conMechaRota ].
	
	! !

!TestDePerforación methodsFor: 'test' stamp: 'ARM 5/8/2023 13:10:43'!
test08CuandoExcavaSueloTierraConMechaDiamanteRemueveCapa

	| excavadora sueloAExcavar |
	
	sueloAExcavar := {
			CapaDeSuelo tierra.
			CapaDeSuelo concretoConResistencia: 5 } .
	
	excavadora := Perforadora sobreSuelo: sueloAExcavar.
	
	excavadora ponerMechaDiamante.
	
	excavadora darGolpeDeTaladro.
	
	self assert: (CapaDeSuelo concretoConResistencia: 5) equals: excavadora capaDeSueloInmediata

	
	! !

!TestDePerforación methodsFor: 'test' stamp: 'ARM 5/8/2023 13:10:48'!
test09CuandoExcavaSueloConcretoConMechaDiamanteBajaUnDecimoDeResistencia

	| excavadora sueloAExcavar |
	
	sueloAExcavar := {
			CapaDeSuelo concretoConResistencia: 5.
			CapaDeSuelo tierra.
			} .
	
	excavadora := Perforadora sobreSuelo: sueloAExcavar.
	
	excavadora ponerMechaDiamante.
	
	excavadora darGolpeDeTaladro.
	excavadora darGolpeDeTaladro.	
	
	self assert: (CapaDeSuelo concretoConResistencia: 4) equals: excavadora capaDeSueloInmediata

	
	! !


!classDefinition: #CapaDeSuelo category: #'ISW1-Perforaciones'!
Object subclass: #CapaDeSuelo
	instanceVariableNames: 'tipo resistencia'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!CapaDeSuelo methodsFor: 'initialization' stamp: 'ARM 9/3/2024 16:26:45'!
initializeArenoso
	
	tipo := #Arenoso.! !

!CapaDeSuelo methodsFor: 'initialization' stamp: 'ARM 9/3/2024 16:26:41'!
initializeConcretoConResistencia: resistenciaInicial 

	resistencia := resistenciaInicial.
	tipo := #Concreto.! !

!CapaDeSuelo methodsFor: 'initialization' stamp: 'ARM 9/3/2024 16:26:38'!
initializeTierra
	
	tipo := #Tierra.! !


!CapaDeSuelo methodsFor: 'tipo' stamp: 'ARM 4/27/2023 20:13:01'!
esArenoso
	^tipo = #Arenoso! !

!CapaDeSuelo methodsFor: 'tipo' stamp: 'ARM 4/27/2023 20:15:50'!
esConcreto
	^tipo = #Concreto! !

!CapaDeSuelo methodsFor: 'tipo' stamp: 'ARM 4/27/2023 20:14:55'!
esTierra
	^tipo = #Tierra! !


!CapaDeSuelo methodsFor: 'comparing' stamp: 'ARM 5/8/2023 13:10:22'!
= unaCapaDeSuelo

	^(unaCapaDeSuelo isKindOf: self class) and: [ tipo = unaCapaDeSuelo tipo and: [resistencia = unaCapaDeSuelo resistencia]]! !

!CapaDeSuelo methodsFor: 'comparing' stamp: 'ARM 5/8/2023 13:10:22'!
hash

	^tipo hash + resistencia hash! !


!CapaDeSuelo methodsFor: 'bajar resistencia' stamp: 'ARM 5/8/2023 13:10:22'!
bajaUnDecimoDeResistencia
	resistencia := resistencia - 1! !

!CapaDeSuelo methodsFor: 'bajar resistencia' stamp: 'ARM 5/8/2023 13:10:22'!
sinResistencia
	^resistencia = 0! !


!CapaDeSuelo methodsFor: 'accessing' stamp: 'ARM 5/8/2023 13:10:35'!
resistencia

	^ resistencia.! !

!CapaDeSuelo methodsFor: 'accessing' stamp: 'ARM 4/27/2023 20:18:41'!
tipo

	^tipo! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CapaDeSuelo class' category: #'ISW1-Perforaciones'!
CapaDeSuelo class
	instanceVariableNames: ''!

!CapaDeSuelo class methodsFor: 'instance creation' stamp: 'ARM 4/27/2023 20:02:32'!
arenoso

	^self new initializeArenoso! !

!CapaDeSuelo class methodsFor: 'instance creation' stamp: 'ARM 5/5/2023 18:01:35'!
concretoConResistencia: resistencia 

	^self new initializeConcretoConResistencia: resistencia ! !

!CapaDeSuelo class methodsFor: 'instance creation' stamp: 'ARM 4/27/2023 20:03:15'!
tierra

	^self new initializeTierra! !


!classDefinition: #Arenoso category: #'ISW1-Perforaciones'!
CapaDeSuelo subclass: #Arenoso
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!


!classDefinition: #Concreto category: #'ISW1-Perforaciones'!
CapaDeSuelo subclass: #Concreto
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!


!classDefinition: #Tierra category: #'ISW1-Perforaciones'!
CapaDeSuelo subclass: #Tierra
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!


!classDefinition: #Mecha category: #'ISW1-Perforaciones'!
Object subclass: #Mecha
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Mecha methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:36:36'!
esMechaRota
	
	^ false
	"
	self subclassResponsibility"! !

!Mecha methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:00:58'!
perforarSobreSueloArenosoCon: unaPerforadora

	self subclassResponsibility! !

!Mecha methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:23:18'!
perforarSobreSueloDeConcretoCon: unaPerforadora

	self subclassResponsibility! !

!Mecha methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:10:31'!
perforarSobreSueloDeTierraCon: unaPerforadora

	self subclassResponsibility! !


!classDefinition: #Diamante category: #'ISW1-Perforaciones'!
Mecha subclass: #Diamante
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Diamante methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:32:07'!
esMechaRota

	^ false! !

!Diamante methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:02:16'!
perforarSobreSueloArenosoCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroSobreSueloArenosoConMechaDiamante! !

!Diamante methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:23:52'!
perforarSobreSueloDeConcretoCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroSobreConcretoConMechaDiamante! !

!Diamante methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:20:06'!
perforarSobreSueloDeTierraCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroSobreSueloDeTierraConMechaDiamante! !


!classDefinition: #Rota category: #'ISW1-Perforaciones'!
Mecha subclass: #Rota
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Rota methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:32:15'!
esMechaRota

	^ true! !

!Rota methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:02:39'!
perforarSobreSueloArenosoCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroConMechaRota! !

!Rota methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:28:59'!
perforarSobreSueloDeConcretoCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroConMechaRota ! !

!Rota methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:12:00'!
perforarSobreSueloDeTierraCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroConMechaRota! !


!classDefinition: #Soft category: #'ISW1-Perforaciones'!
Mecha subclass: #Soft
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Soft methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:32:21'!
esMechaRota

	^ false! !

!Soft methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:01:53'!
perforarSobreSueloArenosoCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroSobreSueloArenosoConMechaSoft! !

!Soft methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:24:30'!
perforarSobreSueloDeConcretoCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroSobreConcretoConMechaSoft ! !

!Soft methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:19:58'!
perforarSobreSueloDeTierraCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroSobreSueloDeTierraConMechaSoft! !


!classDefinition: #Widia category: #'ISW1-Perforaciones'!
Mecha subclass: #Widia
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Widia methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:32:28'!
esMechaRota

	^ false! !

!Widia methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:02:07'!
perforarSobreSueloArenosoCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroSobreSueloArenosoConMechaWidia! !

!Widia methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:24:38'!
perforarSobreSueloDeConcretoCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroSobreConcretoConMechaWidia ! !

!Widia methodsFor: 'as yet unclassified' stamp: 'FR 9/18/2024 16:11:23'!
perforarSobreSueloDeTierraCon: unaPerforadora

	unaPerforadora darGolpeDeTaladroSobreSueloDeTierraConMechaWidia! !


!classDefinition: #Perforadora category: #'ISW1-Perforaciones'!
Object subclass: #Perforadora
	instanceVariableNames: 'tipoDeSuelo mechaPuesta vecesQueGolpeoCapaActual capasDeSueloDebajo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-Perforaciones'!

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 16:25:17'!
darGolpeDeTaladro
		
	self sinMecha ifTrue: [self error: 'No puede perforar'].

	vecesQueGolpeoCapaActual := vecesQueGolpeoCapaActual + 1.

	self sobreSueloArenoso ifTrue: [^ self darGolpeDeTaladroSobreSueloArenoso.].
	self sobreSueloTierra    ifTrue: [^ self darGolpeDeTaladroSobreSueloDeTierra.].
	self sobreSueloConcreto ifTrue: [ ^ self darGolpeDeTaladroSobreSueloDeConcreto. ].
	
	self error: 'error'! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 16:28:37'!
darGolpeDeTaladroConMechaRota
	^ self error: 'No puede perforar'! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 16:22:12'!
darGolpeDeTaladroSobreConcretoConMechaDiamante
	vecesQueGolpeoCapaActual even ifTrue: [ capasDeSueloDebajo first bajaUnDecimoDeResistencia].
	capasDeSueloDebajo first sinResistencia ifTrue: [ ^self transformarCapaEnArenoso].
	^vecesQueGolpeoCapaActual ! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 17:21:00'!
darGolpeDeTaladroSobreConcretoConMechaSoft

	mechaPuesta := Rota new. self error: 'Se rompió'! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 16:17:43'!
darGolpeDeTaladroSobreConcretoConMechaWidia
	vecesQueGolpeoCapaActual = 3 ifTrue: [ mechaPuesta := Rota new. self error: 'Se rompió' ].
	^self! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 16:03:42'!
darGolpeDeTaladroSobreSueloArenoso
	
	mechaPuesta perforarSobreSueloArenosoCon: self! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 16:04:35'!
darGolpeDeTaladroSobreSueloArenosoConMechaDiamante

	mechaPuesta := Rota new.
	self error: 'Se rompió'! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 15:44:47'!
darGolpeDeTaladroSobreSueloArenosoConMechaSoft

	^ self pasarAProximaCapa! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 15:45:02'!
darGolpeDeTaladroSobreSueloArenosoConMechaWidia

	^ self pasarAProximaCapa! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 16:23:04'!
darGolpeDeTaladroSobreSueloDeConcreto
	
	mechaPuesta perforarSobreSueloDeConcretoCon: self! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 16:20:22'!
darGolpeDeTaladroSobreSueloDeTierra
	
	mechaPuesta perforarSobreSueloDeTierraCon: self! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 16:20:31'!
darGolpeDeTaladroSobreSueloDeTierraConMechaDiamante

	^ self pasarAProximaCapa! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 16:20:38'!
darGolpeDeTaladroSobreSueloDeTierraConMechaSoft

	^ self! !

!Perforadora methodsFor: 'perforar' stamp: 'FR 9/18/2024 16:20:44'!
darGolpeDeTaladroSobreSueloDeTierraConMechaWidia

	vecesQueGolpeoCapaActual = 2 ifTrue: [ ^self transformarCapaEnArenoso ].
	^ self! !


!Perforadora methodsFor: 'cambiar capa' stamp: 'ARM 9/3/2024 16:45:24'!
pasarAProximaCapa
	vecesQueGolpeoCapaActual := 0.
	capasDeSueloDebajo := capasDeSueloDebajo allButFirst.
	capasDeSueloDebajo ifEmpty: [^self].! !

!Perforadora methodsFor: 'cambiar capa' stamp: 'ARM 4/27/2023 20:46:30'!
transformarCapaEnArenoso
	capasDeSueloDebajo :=
			capasDeSueloDebajo allButFirst asOrderedCollection
			addFirst: CapaDeSuelo arenoso;
			yourself! !


!Perforadora methodsFor: 'initialization' stamp: 'FR 9/18/2024 15:48:26'!
initializeSobreSuelo: capasDeSueloAExcavarInicialmente

	capasDeSueloDebajo := capasDeSueloAExcavarInicialmente.
	vecesQueGolpeoCapaActual := 0.
	"mechaPuesta := #SinMecha"! !


!Perforadora methodsFor: 'accessing' stamp: 'ARM 4/27/2023 20:17:21'!
capaDeSueloInmediata
	^capasDeSueloDebajo first! !

!Perforadora methodsFor: 'accessing' stamp: 'FR 9/18/2024 16:36:44'!
conMechaRota

	^mechaPuesta esMechaRota! !

!Perforadora methodsFor: 'accessing' stamp: 'ARM 4/27/2023 21:54:43'!
sinMecha

	^mechaPuesta = #SinMecha! !


!Perforadora methodsFor: 'sobre qué suelo' stamp: 'ARM 4/27/2023 20:13:17'!
sobreSueloArenoso
	^capasDeSueloDebajo first esArenoso! !

!Perforadora methodsFor: 'sobre qué suelo' stamp: 'ARM 4/27/2023 20:16:14'!
sobreSueloConcreto
	^capasDeSueloDebajo first esConcreto! !

!Perforadora methodsFor: 'sobre qué suelo' stamp: 'ARM 4/27/2023 20:13:58'!
sobreSueloTierra
	^capasDeSueloDebajo first esTierra! !


!Perforadora methodsFor: 'colocar mecha' stamp: 'FR 9/18/2024 15:48:37'!
ponerMechaDiamante
	mechaPuesta := Diamante new! !

!Perforadora methodsFor: 'colocar mecha' stamp: 'FR 9/18/2024 15:48:43'!
ponerMechaSoft
	mechaPuesta := Soft new! !

!Perforadora methodsFor: 'colocar mecha' stamp: 'FR 9/18/2024 15:48:49'!
ponerMechaWidia
	mechaPuesta := Widia new! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Perforadora class' category: #'ISW1-Perforaciones'!
Perforadora class
	instanceVariableNames: ''!

!Perforadora class methodsFor: 'instance creation' stamp: 'ARM 4/27/2023 20:07:05'!
sobreSuelo: capasDeSueloAExcavar 

	^self new initializeSobreSuelo: capasDeSueloAExcavar ! !
