!classDefinition: #I category: #'Ejercicio 4'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: #'Ejercicio 4'!
I class
	instanceVariableNames: 'prev next'!

!I class methodsFor: 'as yet unclassified' stamp: 'hola 8/27/2024 20:18:11'!
+ aNumber
	^ aNumber  next! !

!I class methodsFor: 'as yet unclassified' stamp: 'hola 8/23/2024 17:09:47'!
next
	^ II! !


!I class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := nil.
	next := nil.! !


!classDefinition: #II category: #'Ejercicio 4'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: #'Ejercicio 4'!
II class
	instanceVariableNames: 'prev next'!

!II class methodsFor: '-- all --' stamp: 'hola 8/27/2024 16:35:43'!
* number
	(number == I)
		ifTrue:  [^self]
		ifFalse: [ ^ self + self * (number previous) ]

			! !

!II class methodsFor: '-- all --' stamp: 'hola 8/27/2024 20:19:51'!
+ number
		^ (self previous) + (number next)! !

!II class methodsFor: '-- all --' stamp: 'hola 8/27/2024 16:32:08'!
- number
	(number == I)
		ifTrue:  [^ self previous]
		ifFalse: [	
				(self > number)
					ifTrue: [^ (self previous) - (number previous)]
					ifFalse: [ ^self error: 'The rest does not have an integer solution.' ]
				]! !

!II class methodsFor: '-- all --' stamp: 'hola 8/27/2024 16:30:09'!
/ number

	(number == self)
		ifTrue:  [^ I].
	
	(number == I)
		ifTrue: [^self].
	
	(self > number)
			ifTrue:  [ ^ ((self - number) / number) next]
			ifFalse: [ ^self error: 'The division does not have an integer solution.' ]
			! !

!II class methodsFor: '-- all --' stamp: 'hola 8/27/2024 16:33:40'!
> number
	(self == number)
		ifTrue: [^ false].
	(number == I)
		ifTrue:  [^ true]
		ifFalse: [^ self > number previous].! !

!II class methodsFor: '-- all --' stamp: 'hola 8/27/2024 16:30:22'!
delete_next
	next ifNotNil: [
		next delete_next.
		next := nil.]! !

!II class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := I.
	next := III.! !

!II class methodsFor: '-- all --' stamp: 'hola 8/27/2024 16:31:18'!
next
	next ifNotNil: [^next].
	next := II createChildNamed: self name, 'I'.
	next previous: self.
	^next! !

!II class methodsFor: '-- all --' stamp: 'hola 8/27/2024 14:58:20'!
previous
	^ prev! !

!II class methodsFor: '-- all --' stamp: 'hola 8/27/2024 14:58:17'!
previous: new_prev
	prev := new_prev ! !


!II class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := I.
	next := III.! !


!classDefinition: #III category: #'Ejercicio 4'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: #'Ejercicio 4'!
III class
	instanceVariableNames: ''!

!III class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := II.
	next := IIII.! !


!classDefinition: #IIII category: #'Ejercicio 4'!
II subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: #'Ejercicio 4'!
IIII class
	instanceVariableNames: ''!

!IIII class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := III.
	next := IIIII.! !


!classDefinition: #IIIII category: #'Ejercicio 4'!
II subclass: #IIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIII class' category: #'Ejercicio 4'!
IIIII class
	instanceVariableNames: ''!

!IIIII class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := IIII.
	next := IIIIII.! !


!classDefinition: #IIIIII category: #'Ejercicio 4'!
II subclass: #IIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIII class' category: #'Ejercicio 4'!
IIIIII class
	instanceVariableNames: ''!

!IIIIII class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := IIIII.
	next := IIIIIII.! !


!classDefinition: #IIIIIII category: #'Ejercicio 4'!
II subclass: #IIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIII class' category: #'Ejercicio 4'!
IIIIIII class
	instanceVariableNames: ''!

!IIIIIII class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := IIIIII.
	next := IIIIIIII.! !


!classDefinition: #IIIIIIII category: #'Ejercicio 4'!
II subclass: #IIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIII class' category: #'Ejercicio 4'!
IIIIIIII class
	instanceVariableNames: ''!

!IIIIIIII class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := IIIIIII.
	next := IIIIIIIII.! !


!classDefinition: #IIIIIIIII category: #'Ejercicio 4'!
II subclass: #IIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIII class' category: #'Ejercicio 4'!
IIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIII class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := IIIIIIII.
	next := IIIIIIIIII.! !


!classDefinition: #IIIIIIIIII category: #'Ejercicio 4'!
II subclass: #IIIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIIII class' category: #'Ejercicio 4'!
IIIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIIII class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := IIIIIIIII.
	next := IIIIIIIIIII.! !


!classDefinition: #IIIIIIIIIII category: #'Ejercicio 4'!
II subclass: #IIIIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIIIII class' category: #'Ejercicio 4'!
IIIIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIIIII class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := IIIIIIIIII.
	next := IIIIIIIIIIII.! !


!classDefinition: #IIIIIIIIIIII category: #'Ejercicio 4'!
II subclass: #IIIIIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 4'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIIIIII class' category: #'Ejercicio 4'!
IIIIIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIIIIII class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:19'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	prev := IIIIIIIIIII.
	next := nil.! !

I initializeAfterFileIn!
II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!
IIIII initializeAfterFileIn!
IIIIII initializeAfterFileIn!
IIIIIII initializeAfterFileIn!
IIIIIIII initializeAfterFileIn!
IIIIIIIII initializeAfterFileIn!
IIIIIIIIII initializeAfterFileIn!
IIIIIIIIIII initializeAfterFileIn!
IIIIIIIIIIII initializeAfterFileIn!