!classDefinition: #Bill category: #'Ejercicio 2'!
DenotativeObject subclass: #Bill
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 2'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Bill class' category: #'Ejercicio 2'!
Bill class
	instanceVariableNames: 'energia'!

!Bill class methodsFor: '-- all --' stamp: 'hola 8/19/2024 20:05:40'!
caminar: unaCantidadDeKilometros

	^energia := energia - (3 * day) -  (unaCantidadDeKilometros / kilometer * day)! !

!Bill class methodsFor: '-- all --' stamp: 'hola 8/19/2024 20:01:13'!
comer: unaCantidadDeKilosDeCerebro

	^energia := energia + ((4 * day) * unaCantidadDeKilosDeCerebro / kilogram)
! !

!Bill class methodsFor: '-- all --' stamp: 'hola 8/16/2024 21:06:56'!
energia
	^energia! !

!Bill class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:05'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	energia := (SimpleMeasure basicNew instVarAt: 1 put: 47; instVarAt: 2 put: (BaseUnit basicNew instVarAt: 1 put: 'day'; instVarAt: 2 put: 'days'; instVarAt: 3 put: 'NO SIGN'; yourself); yourself).! !


!Bill class methodsFor: '--** private fileout/in **--' stamp: 'hola 9/16/2024 12:24:05'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	energia := (SimpleMeasure basicNew instVarAt: 1 put: 47; instVarAt: 2 put: (BaseUnit basicNew instVarAt: 1 put: 'day'; instVarAt: 2 put: 'days'; instVarAt: 3 put: 'NO SIGN'; yourself); yourself).! !

Bill initializeAfterFileIn!