// b ) Enunciar un algoritmo que use backtracking para resolver este problema que se base en la siguientes ideas:
// · La solución parcial tiene los valores de las primeras i − 1 filas establecidos, al igual que los valores
// de las primeras j columnas de la fila i.
// · Para establecer el valor de la posición (i, j +1) (o (i+1, 1) si j = n e i ≠ n) se consideran todos los
// valores que aún no se encuentran en el cuadrado. Para cada valor posible, se establece dicho valor en la 
// posición y se cuentan todos los cuadrados mágicos con esta nueva solución parcial.

// Mostrar los primeros dos niveles del árbol de backtracking para n = 3.


// Aclaraciones:
// restantes es un conjunto de enteros desde 1 hasta n².
// la recursión termina cuando se pasa de rosca, osea cuando i==n+1 (en el código lo llame lleneCuadrado)


// Fuerza Bruta
int cuantosCuadradosHay(cuadrado, restantes, i, j){
    if (lleneCuadrado && cuadradoEsCorrecto){
        cuadradosPosibles++;
    } else {
        for (int elem : restantes){
            coloco elem en i,j;
            quito elem de restantes;

            if (j==n){
                cuantosCuadradosHay(cuadrado, restantes, i+1, 1);
            } else {
                cuantosCuadradosHay(cuadrado, restantes, i, j+1);
            }

            quito elem de i,j;
            agrego elem en restantes;
        }
    }
}


// Backtracking
// > PODA 1: Al terminar una fila, columna o diagonal, chequea que está correcta.

int cuantosCuadradosHay(cuadrado, restantes, i, j){
    if (lleneCuadrado){
        cuadradosPosibles++;
    } else {
        for (int elem : restantes){
            coloco elem en i,j;
            quito elem de restantes;

            if (lleneFila && i!=1) me fijo si la fila está correcta
            if (lleneColumna)      me fijo si la columna está correcta
            if (lleneDiagonal)     me fijo si la diagonal está correcta

            if (estaTodoCorrecto){
                if (j==n){
                    cuantosCuadradosHay(cuadrado, restantes, i+1, 1);
                } else {
                    cuantosCuadradosHay(cuadrado, restantes, i, j+1);
                }
            }            

            quito elem de i,j;
            agrego elem en restantes;
        }
    }
}


// d) Considere la siguiente poda al árbol de backtracking : al momento de elegir el valor de una nueva
// posición, verificar que la suma parcial de la fila no supere el número mágico. Verifcar también
// que la suma parcial de los valores de las columnas no supere el número mágico. Introducir estas podas al
// algoritmo e implementarlo en la computadora. ¿Puede mejorar estas podas?

// > PODA 2: Al agregar un elem a una fila, chequeo que la suma parcial de la fila sea menor que el número
// mágico

int cuantosCuadradosHay(cuadrado, restantes, i, j, sumaParcial){
    if (lleneCuadrado){
        cuadradosPosibles++;
    } else {
        for (int elem : restantes){
            coloco elem en i,j;
            quito elem de restantes;

            sumaParcial += elem;

            if (lleneFila && i!=1) me fijo si la fila está correcta
            if (lleneColumna)      me fijo si la columna está correcta
            if (lleneDiagonal)     me fijo si la diagonal está correcta

            if (estaTodoCorrecto && sumaParcialEsCorrecta){
                if (j==n){
                    cuantosCuadradosHay(cuadrado, restantes, i+1, 1, 0);
                } else {
                    cuantosCuadradosHay(cuadrado, restantes, i, j+1, sumaParcial);
                }
            }

            sumaParcial -= elem;  

            quito elem de i,j;
            agrego elem en restantes;
        }
    }
}