-----------------------------------------------------
Recorridos en grafos

> Topological Sort
Es una manera de ordenar vértices en Directed Acyclic Graphs (DAGs).
Los vértices se ordenan de manera tal que si (u→v) ∈ E, entonces u aparece antes que v en el ordenamiento.



-----------------------------------------------------
Árbol Generador Mínimo (AGM)
Es un subgrafo asociado a un grafo G. Contiene los mismos vértices, es conexo y es acíclico.
* Tiene n vértices y n-1 aristas


> Kruskal
Arma un árbol generador de costo mínimo. Es greedy

Pseudocódigo: https://brilliant.org/wiki/kruskals-algorithm/

. Set an empty set A={} and F=E where E is the set of all edges.
. Choose an edge e in F of minimum weight, and check whether adding e to A creates a cycle.
	. If it does, remove e from F
	. If it doesn't, move e from F to A
. If F={} stop and output the minimal spanning tree (V,A).

- Complejidad: O(mlogn)

| -> Demo: 
| Recorrer todas las aristas es O(m) porque estamos representando el grafo en una lista de adyacencias.
| Primero ordenamos las aristas según su costo: O(mlogm)
| Luego por cada arista, consideramos agregarla o no al árbol: O(mlogn)
| 
| La complejidad sería O(m*(logn+logm)). Pero como
| 	m ≤ n²	⟺	logm ≤ 2logn	⟺	logm ∈ O(logn)
| Entonces la complejidad final es O(mlogn)

Para detectar si hay algún ciclo se usa *Union Find*
- Los vértices se van agregando a una estructura de elementos disjuntos (Disjoint-set data structure),
cada uno va a tener un representante, entonces basta saber a qué conjunto pertenece.

> Cormen:
It uses a disjoint-set data structure to
maintain several disjoint sets of elements. Each set contains the vertices in one tree
of the current forest. The operation F IND -S ET .u/ returns a representative element
from the set that contains u. Thus, we can determine whether two vertices u and 
belong to the same tree by testing whether F IND -S ET .u/ equals F IND -S ET ./. To
combine trees, Kruskal’s algorithm calls the U NION procedure.
for each edge (u,v) , whether the endpoints u and v belong to the same
tree. If they do, then the edge (u,v)  cannot be added to the forest without creating
a cycle, and the edge is discarded. Otherwise, the two vertices belong to different
trees. In this case, line 7 adds the edge (u,v) to A, and line 8 merges the vertices
in the two trees.



> Prim
Algoritmo para armar AGMs. Es greedy.

--- Vamos a mantener una cola con los vértices que no tienen conexión al árbol (vértices a los que los vértices del árbol no inciden). En cada iteración vamos a agregar al árbol un vértice de esa cola. El vértice que vamos a elegir va a ser aquel que tenga la arista con menor costo que conecta a algún vértice del árbol. ---

> Cormen:
Prim’s algorithm has the property that the edges in the set A always form a single tree. The tree starts from an arbitrary root vertex r and grows until the tree spans all the vertices in V. Each step adds to the tree A a light edge that connects A to an isolated vertex—one on which no edge of A is incident.
This strategy qualiﬁes as greedy since at each step it adds to the tree an edge that contributes the minimum amount possible to the tree’s weight.


> Pseudocódigo

.   for each u ∈ G.v
.   	u.key = inf
.   	u.π = NIL
.   r.key = 0
.   Q = G.V
.   while !Q.empty()
.   	u = EXTRACT-MIN(Q)
.   	for each v ∈ G.Adj[u]
.   		if v ∈ Q and w(u,v) < v.key
.   			v.π = u
.   			v.key = w(u,v)

Invariante: (antes de cada iteración del while)
* A = {(v, v.π) / v ∈ V - {r} - Q}
* Los vértices ya dentro del árbol son V-Q
* Todos los vértices v ∈ Q, si v.π ≠ NIL, entonces v.key < inf y v.key es el peso de la arista (v, v.π) conectando v a un vértice ya dentro del árbol.


Complejidad:
> Si implementamos Q como un binary min-heap

.   for each u ∈ G.v                        // O(n)
.   	u.key = inf
.   	u.π = NIL
.   r.key = 0
.   Q = G.V
.   while !Q.empty()                        // n veces
.   	u = EXTRACT-MIN(Q)                  // O(logn)
.   	for each v ∈ G.Adj[u]               // O(m) en total
.   		if v ∈ Q and w(u,v) < v.key     // Podemos hacerlo constante si queremos
.   			v.π = u
.   			v.key = w(u,v)              // Esto reordena Q (decrease-key), lo cual es O(logn)

-> Complejidad:              O(nlogn + O(mlogn)) = O(mlogn)
-> Complejidad con fib heap: O(m+nlogn)

We can improve the asymptotic running time of Prim’s algorithm by using Fibonacci heaps. If a Fibonacci heap holds n elements, an EXTRACT-MIN operation takes O(logn) amortized time and a DECREASE-KEY operation takes O(1) amortized time. Therefore, if we use a Fibonacci heap to implement the min-priority queue Q, the running time of Prim’s algorithm improves to O(m + nlogn)



-----------------------------------------------------
Camino mínimo

> Bellman Ford
Resuelve el problema de camino mínimo para grafos pesados (con aristas positivas o negativas). Devuelve un booleano indicando si hay o no un ciclo de peso negativo. Si no hay ciclo, devuelve los caminos mas cortos y su costo.

.   Relax(u,v,w)
.   if v.d > u.d + w(u,v)
.   	v.d = u.d + w(u,v)
.   	v.π = u

.   Bellman-Ford(G,w,s)
.   
.   // Inicializo estructuras          // O(n)
.   for each vertex v ∈ G.V
.   	v.d = inf
.   	v.π = nil
.   s.d = 0
.   
.   // Relajo n-1 veces
.   for i=1 to |G.V|-1                  // O(n)
.   	for each edge (u,v) ∈ G.E       // O(m)
.   		Relax(u,v,w)                // O(1)
.   
.   // Busco ciclos negativos		
.   for each edge (u,v) ∈ G.E
.   	if v.d > u.d + w(u,v)
.   		return FALSE	
.   return TRUE

-> Complejidad: O(mn)

| *Path-Relaxation Property:
| If p=(v0, v1, ..., vk) is a shortest path from s=v0 to vk, and we relax the edges of p in the order
| (v0,v1), (v1,v2), ..., (vk-1, vk), then vk.d = δ(s,vk). This property holds regardless of any other
| relaxation steps that occur, even if they are intermixed with relaxations of the edges of p.

A partir de esto es fácil demostrar que después de n-1 iteraciones, si no hay ciclos negativos, luego de correr bellman-ford se cumple que v.d = δ(s,v) ∀v∈V.



> Floyd
No funciona con ciclos negativos

.   Floyd(G)
.   entrada: G=(V,X) de n vértices
.   salida: D matriz de distancias de G
.   
.   D ← L
.   para k desde 1 hasta n hacer
.   	para i desde 1 hasta n hacer
.   		para j desde 1 hasta n hacer
.   			D[i][j] ← min(D[i][j], D[i][k] + D[k][j])
.   		fin para
.   	fin para
.   fin para
.   retornar D

-> Complejidad: O(n³)


Lema: Al finalizar la iteración k del algoritmo de Floyd, d[i][j] es la longitud de los caminos mínimos desde vi a vj cuyos vértices intermedios son elementos de {v1,...,vk}. 

| -> Demo: inducción en las iteraciones (k)
| 
| Caso base: Antes de entrar por primera vez al ciclo, k=0, vale D=L. Esto es correcto ya que son las 
| longitudes de los caminos mínimos sin vértices intermedios, es decir, caminos de solo un arco.
| 
| Paso inductivo: Consideremos una iteración k, k≥1
| 
| HI: Al terminar la iteración k' (k'<k), Dk' tiene las longitudes de los caminos mínimos entre todos los 
| pared de vértices cuyos vértices intermedios son elementos de {v1,...,vk}.
| 
| Dados dos vértices v1 y vj, el camino mínimo P de vi a vj cuyos vértices intermedios están en 
| {v1,...,vk}, tiene a vk o no lo tiene.
| Sea P1 un camino mínimo de vi a vj cuyos vértices intermedios estan en {v1,...,vk} y vk ∈ P1
| Sea P2 un camino mínimo de vi a vj cuyos vértices intermedios estan en {v1,...,vk-1}
| 
| - P1 = P1_(vivk) + P1_(vkvj). Por la subestructura óptima de un camino mínimo, tanto P1_(vivk) como 
| P1_(vkvj) son caminos mínimos de vi a vk y de vk a vj respectivamente con vértices intermedios en 
| {v1,...,vk-1}. Por HI, l(P1) = dk-1[i][k] + dk-1[k][j]
| - Por HI, l(P2) = dk-1[i][j]
| 
| P es el mínimo entre P1 y P2. Por lo tanto,
| l(P) = min{l(P1), l(P2)} = mín{dk-1[i][k] + dk-1[k][j],dk-1[i][j]}, que es el cálculo que realiza el
| algoritmo.


* Si durante el algoritmo encontramos que para algún i ∈ V, d[i][i] < 0 significa que hay al menos un ciclo de peso negativo que pasa por i. 