#ifndef EJERCICIO
#define EJERCICIO

#include <stdio.h>
#include <stdlib.h>

void SumarVectores(char *A, char *B, char *Resultado, int dimension);
void InicializarVector(short *A, short valorInicial, int dimension);
void DividirVectorPorPotenciaDeDos(int *A, int potencia, int dimension);
void FiltrarMayores(short *A, short umbral, int dimension);

#endif