section .data

M: dd 2,0,7,4,0,1,1,2,3,5,4,0,2,0,2,1

section .text

global Sumatoria

;void Sumatoria(int* M, int* Res, int dimension);
; M[rdi], Res[rsi], dimension[edx]

Sumatoria:
	push rbp
	mov rbp, rsp

    ; Descomentar para correr el ejemplo del enunciado
    ; mov rdi, M

    xor rax, rax
    mov ecx, 4

    ; Hago sumas horizontales
    .ciclo:
	    movdqu xmm0, [rdi+rax]

        ; packed horizontal add double
        movdqu xmm4, xmm0
        phaddd xmm4, xmm4
        phaddd xmm4, xmm4

        movdqu [rsi+rax], xmm4
        add rax, 16

    loop .ciclo

    ; [rsi+16*i] = sumatoria de la fila i de M

    movdqu xmm0, [rdi]        ; Fila 0
    movdqu xmm1, [rdi+16]     ; Fila 1
    movdqu xmm2, [rdi+16*2]   ; Fila 2
    movdqu xmm3, [rdi+16*3]   ; Fila 3



    ; Hago las sumas verticales
    movdqu xmm5, xmm0
    paddw  xmm5, xmm1
    paddw  xmm5, xmm2
    paddw  xmm5, xmm3

    ; xmm5[j] = sumatoria de la columna j de M

    ; Sumo sumas verticales con sumas horizontales
    xor rax, rax
    mov ecx, 4
    .ciclo2:
        movdqu xmm6, [rsi+rax]
        paddw  xmm6, xmm5
        movdqu [rsi+rax], xmm6
        add rax, 16
    loop .ciclo2


    ; En cada casilla ij de Res, sumé dos veces el valor de ij
    ; (Porque ij = sumatoria de la fila i + sumatoria de la columna j)

    xor rax, rax
    mov ecx, 4
    .ciclo3:
        movdqu xmm0, [rdi+rax]   ; xmm0   := M[i]
        movdqu xmm1, [rsi+rax]   ; xmm1   := Res[i]
        psubw  xmm1, xmm0        ; Res[i] := Res[i] - M[i]
        movdqu [rsi+rax], xmm1
        add rax, 16
    loop .ciclo3

	pop rbp
	ret


