section .data
vectorDeUnos:  times 16 dw 1
variableQ:              dq 0

section .text

global MultiplicarVectores
global ProductoInterno
extern malloc

; void MultiplicarVectores(short *A, short *B, int *Res, int dimension)
; A[rdi], B[rsi], Res[rdx], dimension[ecx]
MultiplicarVectores:
	push rbp
	mov rbp, rsp

    shr ecx, 2
    xor rax, rax
    .ciclo:
        movdqa xmm0, [rdi] ; A, xmm0 = | a_1 | ... | a_n |
        movdqa xmm1, [rsi] ; B, xmm1 = | b_1 | ... | b_n |
        movdqa xmm2, xmm0  ; B, xmm2 = | b_1 | ... | b_n |

        pmullw xmm1, xmm0 ; packed multiplication low word
        pmulhw xmm2, xmm0 ; packed multiplication high word
        movdqa xmm0, xmm1

        ; xmm0 = | lo(a_1*b_1) | ... | lo(a_n*b_n) |
        ; xmm1 = | lo(a_1*b_1) | ... | lo(a_n*b_n) |
        ; xmm2 = | hi(a_1*b_1) | ... | hi(a_n*b_n) |

        punpcklwd xmm0, xmm2 ; Packed Unpack low  double word
        punpckhwd xmm1, xmm2 ; Packed Unpack high double word

        ; xmm0 = | hi:lo(a_4*b_4) | ... | hi:lo(a_1*b_1) |
        ; xmm1 = | hi:lo(a_8*b_8) | ... | hi:lo(a_5*b_5) |

	   movdqa [rdx],    xmm0
	   movdqa [rdx+rax], xmm1

	   add rax, 16

	loop .ciclo

	pop rbp
	ret

; (a1, a2, a3) x (b1, b2, b3) = a1b1 + a2b2 + a3b3

; int ProductoInterno(short *A, short *B, int dimension);
; A[rdi], B[rsi], dimension[edx]
ProductoInterno:
    push rbp
    mov rbp, rsp

    ; Backupeo A, B, dimension y r12
    push r12
    push rdi
    push rsi
    push rdx

    xor r12, r12

    ; Malloqueo Res
    mov rdi, rdx ; rdi := dimension
    shl rdi, 5   ; rdi := dimension*sizeof(int)
    call malloc

    ; Rax := *Res

    pop rdx ; rdx := dimension
    pop rsi ; rsi := B
    pop rdi ; rdi := A

    mov r9, rdx  ; r9d := dimension
    mov rdx, rax ; rdx := Res

    mov ecx, r9d ; ecx := dimension
    shr ecx, 3   ; ecx := ecx/8
    .ciclo:

        push rcx     ; guardo iterador
        mov ecx, r9d ; sobreescribo iterador (ecx := dimension)

        ; A[rdi], B[rsi], Res[rdx], dimension[ecx]
        call MultiplicarVectores

        movdqa xmm0, [rdx]

        ; repito esto 3 veces (porque en xmm0 tengo 2³ words)
        phaddd xmm0, xmm0
        phaddd xmm0, xmm0
        phaddd xmm0, xmm0

        movsd [variableQ], xmm0
        add r12, [variableQ]

        add rdi, 16
        add rsi, 16

        pop rcx

    loop .ciclo

    mov rax, r12

    pop r12
    pop rbp
    ret
