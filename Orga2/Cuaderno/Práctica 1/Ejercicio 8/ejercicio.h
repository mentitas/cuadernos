#ifndef EJERCICIO
#define EJERCICIO

#include <stdio.h>
#include <stdlib.h>

#define NAME_LEN 21

typedef struct cliente_str {
    char nombre[NAME_LEN];
    char apellido[NAME_LEN];
    unsigned long int compra;
    unsigned int dni;
} cliente_t;

typedef struct __attribute__((__packed__)) packed_cliente_str {
    char nombre[NAME_LEN];
    char apellido[NAME_LEN];
    unsigned long int compra;
    unsigned int dni;
} __attribute__((packed)) packed_cliente_t;

cliente_t cliente_random_c(cliente_t* A);
cliente_t cliente_random_asm(cliente_t* A);

#endif