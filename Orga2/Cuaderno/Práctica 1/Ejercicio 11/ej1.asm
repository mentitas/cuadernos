extern malloc

global strArrayNew
global strArrayGetSize
global strArrayAddLast
global strArraySwap
global strArrayDelete

%define OFFSET_SIZE     0
%define OFFSET_CAPACITY 1
%define OFFSET_DATA     8

;########### SECCION DE DATOS
section .data


;########### SECCION DE TEXTO (PROGRAMA)
section .text

; str_array_t* strArrayNew(uint8_t capacity)
; capacity[rdi] = capacity[dil]
strArrayNew:
    push rbp
    mov rbp, rsp

    push rdi    ; guardo capacity
    sub rsp, 8  ; alineo stack

    mov rdi, 16    ; rdi := 16
    call malloc    ; rax := malloc(rdi bytes)

    add rsp, 8 ; alineo stack
    pop rdi    ; recupero capacity

    mov [rax+OFFSET_SIZE],     byte 0
    mov [rax+OFFSET_CAPACITY], byte dil

    push rax ; guardo ptr a res

    ; capacity[dil]
    sal dil, 3    ; dil := dil*(2^3)
    call malloc   ; rax := malloc(rdi bytes)

    mov r12, rax  ; ptr a data

    pop rax  ; recupero ptr a res
    mov [rax+OFFSET_DATA], r12

    pop rbp
    ret

; uint8_t  strArrayGetSize(str_array_t* a)
; a[rdi]
strArrayGetSize:
    push rbp
    mov rbp, rsp

    xor rax, rax
    mov rax, [rdi+OFFSET_SIZE]

    pop rbp
    ret

; void  strArrayAddLast(str_array_t* a, char* data)
; a[rdi], data[rsi]
strArrayAddLast:
    push rbp
    mov rbp, rsp

    mov al, byte [rdi+OFFSET_SIZE]
    mov bl, byte [rdi+OFFSET_CAPACITY]

    cmp al, bl  ; si size==capacity, entonces no puedo agregar nada
    je .fin

    push rdi ; guardo a
    push rsi ; guardo data

    call strArrayGetSize  ; rax := a->size

    pop rsi ; recupero data
    pop rdi ; recupero a


    mov r8, [rdi+OFFSET_DATA]  ; r8 := a->data

    ; [r8]            es el primer  char* del array
    ; [r8+8]          es el segundo char* del array
    ; [r8+8*(size-1)] es el último  char* del array

    push rax  ; guardo tamaño

    sal al, 3           ; al := al*(2^3)
    add r8b, al
    mov [r8], rsi       ; a->data[a->size] := rsi

    pop rax  ; recupero tamaño
    inc rax

    mov [rdi+OFFSET_SIZE], byte al

    .fin:
    pop rbp
    ret

; pre: voy a asumir que i y j son índices válidos
; void  strArraySwap(str_array_t* a, uint8_t i, uint8_t j)
; a[rdi], i[sil], j[dl]
strArraySwap:
    push rbp
    mov rbp, rsp

; probablemente este todo mal
    push rsi
    push rdx

    mov r8, [rdi+OFFSET_DATA]  ; r8 := a->data

    sal sil, 3 ; sil := sil*8
    sal dil, 3 ; dil := dil*8

    mov r9,  [rdi+rsi] ; r9  := a[i]
    mov r10, [rsi] ; r10 := a[j]

    pop rdx
    pop rsi

    pop rbp
    ret


; void  strArrayDelete(str_array_t* a)
strArrayDelete:


