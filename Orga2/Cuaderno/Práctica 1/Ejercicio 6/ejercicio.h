#ifndef EJERCICIO
#define EJERCICIO

#include <stdio.h>
#include <stdlib.h>
#include "string.h"

char* cesar_c(char* s, int x);
char* cesar_asm(char* s, int x);

#endif