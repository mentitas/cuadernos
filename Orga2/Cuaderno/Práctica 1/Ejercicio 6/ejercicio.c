#include "ejercicio.h"

// Funciona bien unicamente cuando s es un string en mayúscula
char* cesar_c(char* s, int x){

    int longitud = 0;
    for(int i=0; s[i] != 0; i++){
        longitud++;
    }

    // reservo espacio para el nuevo string
    char* res = malloc(longitud * sizeof(char));

    for(int i=0; i < longitud; i++){
        int num = s[i] + x;
        if (num > 90){
            res[i] = num-26;
        } else {
            res[i] = num;
        }
    }

    return res;
}