section .text

global sumaVectores

; int sumaVectores(int *v, int l);
; v[rdi], longitud[rsi]
sumaVectores:
	push rbp
	mov rbp, rsp

    xor rax, rax ; rax := 0
    mov rcx, rsi ; puedo reemplazar rsi := length
    mov r10, rdi ; puedo reemplazar rdi := vector

    .ciclo:
        add al, [r10]
        add r10, 4   ; si quiero usar vector, hay que incrementar r10 de a 1
    loop .ciclo

	pop rbp
	ret


section .data

vector db 1, 2, 3, 4, 5, 30
length equ $ - vector