#include "ejercicio.h"

int main()
{
    int* ptr = malloc(16);

    ptr[0] = 0;
    ptr[1] = 1;
    ptr[2] = 2;
    ptr[3] = 54;

    printf("%s", "Mi vector: ");
    printf("%i%s", ptr[0], ", ");
    printf("%i%s", ptr[1], ", ");
    printf("%i%s", ptr[2], ", ");
    printf("%i\n", ptr[3]);

    int res = sumaVectores(ptr, 4);

    printf("%s %i\n", "sumaVectores(v) = ", res);

    return 0;


}
