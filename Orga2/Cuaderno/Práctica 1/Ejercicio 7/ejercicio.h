#ifndef EJERCICIO
#define EJERCICIO

#include <stdio.h>
#include <stdlib.h>

int prefijo_de_c(char* s, char* t);
int prefijo_de_asm(char* s, char* t);

#endif