section .text
global prefijo_de_asm

; int prefijo_de_asm(char* s, char* t)
; s[rdi], t[rsi]
prefijo_de_asm:
	push rbp
	mov rbp, rsp

    ; s[r10]
	; t[r11]
	mov r10, rdi
	mov r11, rsi

    xor rax, rax

    ; longitud[r12]
	xor r12, r12
	.calculoLength:
	    mov cl, [rdi] ; cl es el bit actual que estoy chequeando en s
	    mov dl, [rsi] ; dl es el bit actual que estoy chequeando en t
	    cmp cl, 0     ; si dl vale 0 es porque ya recorrí todo s
	    je .exit
	    cmp dl, 0     ; si dl vale 0 es porque ya recorrí todo t
        je .exit

	    inc r12       ; sino incremento la longitud
	    inc rdi       ; e incremento los punteros
	    inc rsi

	    jmp .calculoLength

    .exit:

    xor rax, rax ; res := 0
    mov rdi, r10
    mov rsi, r11

    .calculoPrefijo:
    cmp rax, r12      ; if (res >= longitud){
    jge .exit2        ;     // salimos del ciclo

    mov cl, [rdi]     ; cl := s[i]
    mov dl, [rsi]     ; dl := t[i]
    cmp cl, dl        ; if (s[i] != t[i])
    jne .exit2        ;     // salimos del ciclo

    ; si llegamos acá es porque s[i] == t[i]
    inc rax
    inc rdi
    inc rsi
    jmp .calculoPrefijo

    .exit2:

	pop rbp
	ret

section .data