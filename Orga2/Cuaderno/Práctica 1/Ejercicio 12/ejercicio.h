#ifndef EJERCICIO
#define EJERCICIO

#include <stdio.h>
#include <stdlib.h>

typedef struct node_t {
    struct node_t *next; // 8 bytes
    struct node_t *prev; // 8 bytes
    char *string;        // 8 bytes
} node;

int stringTerminaEn(char* string, char c);
void borrarTerminadasEn(node** l, char c);


#endif