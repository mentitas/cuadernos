#include "ejercicio.h"

int main()
{
    /*
    char * stringA = malloc(sizeof(char)*4);
    char * stringB = malloc(sizeof(char)*5);

    stringA = "weiss";
    stringB = "adora";
    */

    node * nodoA = malloc(sizeof(node));
    node * nodoB = malloc(sizeof(node));

    nodoA->next   = nodoB;
    nodoA->prev   = NULL;
    nodoA->string = "weiss";

    nodoB->next   = NULL;
    nodoB->prev   = nodoA;
    nodoB->string = "adora";

    node **l = &nodoA;

    //printf("adora termina en a? %i", stringTerminaEn("adora", 'a'));

    borrarTerminadasEn(l, 'a');

    printf("\nsize of long %zu \n", sizeof(long));
    printf("size of node %zu \n", sizeof(node));

    return 0;

}
