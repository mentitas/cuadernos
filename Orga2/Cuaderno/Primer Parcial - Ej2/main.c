#include "ejercicio.h"

int main()
{
    int dimension = 32;
    short* X = malloc(sizeof(short)*dimension);
    short* Y = malloc(sizeof(short)*dimension-6);

    for(int i=0;  i<dimension; i++){
        if (i%2 == 0){
            X[i] = i; // Ri
        } else {
            X[i] = i-1; // Li
        }
    }

    for(int i=0; i<dimension-6; i++) Y[i] = 0;

    PromediarCanales(X, Y, dimension);

    printf("\nX: " );
    for(int i=0; i<dimension; i++) printf("%i ", X[i]);

    printf("\nY: " );
    for(int i=0; i<dimension-6; i++) printf("%i ", Y[i]);

    return 0;

}
