section .data
mascaraL: dw 0x0000, 0xFFFF, 0x0000, 0xFFFF, 0x0000, 0xFFFF, 0x0000, 0xFFFF
mascaraR: dw 0xFFFF, 0x0000, 0xFFFF, 0x0000, 0xFFFF, 0x0000, 0xFFFF, 0x0000

section .text

global PromediarCanales

;void PromediarCanales(short *X, short *Y, int dimension);
; X[rdi], Y[rsi], dimension[edx]

PromediarCanales:
	push rbp
	mov rbp, rsp

	xor rax, rax

	mov ecx, edx     ; Porque |Y| = |X|-6, y en total vamos a generar
	shr ecx, 1       ; 2 elementos de Y por iteración, es decir,
    sub ecx, 3       ; hacemos |Y|/2 iteraciones.


	.ciclo:

        movdqu xmm1, [rdi+rax]  ; xmm1 := | r0 | l0 | ... | rn | ln |
        movdqu xmm2, [rdi+rax]  ; xmm2 := | r0 | l0 | ... | rn | ln |
        movdqu xmm3, [mascaraR]   ; xmm3 := |  1 |  0 | ... |  1 |  0 |
        movdqu xmm4, [mascaraL]   ; xmm4 := |  0 |  1 | ... |  0 |  1 |

        ; aplico mascaras
        pand xmm1, xmm3           ; xmm1 := | r0 |  0 | ... | rn |  0 |
        pand xmm2, xmm4           ; xmm2 := |  0 | l0 | ... |  0 | ln |

        ; repito 3 veces porque hay 8 elementos en xmm1
        ; phaddw = packed horizontal add word
        phaddw xmm1, xmm1
        phaddw xmm1, xmm1
        phaddw xmm1, xmm1

        phaddw xmm2, xmm2
        phaddw xmm2, xmm2
        phaddw xmm2, xmm2

        ; shifteo 2 veces a la derecha para dividir por 4
        ; psrld = packed shift right logical data
        psrld xmm1, 2
        psrld xmm2, 2

        ; aplico mascaras
        pand xmm1, xmm3
        pand xmm2, xmm4

        ; por = packed or
        por xmm1, xmm2
        movdqu [rsi+rax], xmm1
        add rax, 4

    loop .ciclo

	pop rbp
	ret

