#include "ejercicio.h"

int main()
{
    int dimension = 4;
    int* A         = malloc(dimension*sizeof(int));
    int* B         = malloc(dimension*sizeof(int));
    int* Resultado = malloc(dimension*sizeof(int));

    A[0] = 0x3892;
    A[1] = 0xF145;
    A[2] = 0xDEDA;
    A[3] = 0xA163;

    B[0] = 0x532F;
    B[1] = 0x1768;
    B[2] = 0xE234;
    B[3] = 0x94BA;

    Sumar(A, B, Resultado, dimension);

    //printf("Mi vector Resultado: %zu\n",sizeof(char));

    printf("\nA (hex): " );
    for(int i=0; i<dimension; i++) printf("%04x ", A[i]);
    printf("\nA (dec): " );
    for(int i=0; i<dimension; i++) printf("%i ", A[i]);

    printf("\n\nB (hex): " );
    for(int i=0; i<dimension; i++) printf("%04x ", B[i]);
    printf("\nB (dec): " );
    for(int i=0; i<dimension; i++) printf("%i ", B[i]);

    printf("\n\nResultado (hex): " );
    for(int i=0; i<dimension; i++) printf("%04x ", Resultado[i]);
    printf("\nResultado (dec): " );
    for(int i=0; i<dimension; i++) printf("%i ", Resultado[i]);

    printf("\n");

    return 0;

}
