// Algoritmo y Estructura de Datos II
// Trabajo Práctico 1

// Integrantes:
// Halperín, Matías      LU: 1251/21, matias.halperin@gmail.com
// Hirsch, Micaela       LU: 186/21,  micahlopa@gmail.com
// Roccella, Cielo       LU: 122/21,  cielorocella@gmail.com
// Rosenzuaig, Florencia LU: 118/21,  f.rosenzuaig@gmail.com


// Decisiones:    intepretamos los 3 tipos de pisos como enteros, siendo 0 una planta baja, 1 una rampa
//                y 2 un techo

//                las rampas son como plataformas, no tienen una dirección predeterminada. Si una rampa 
//                conecta un piso con varios techos, o viceversa, todas las opciones son válidas.

//                consideramos el único movimiento inválido posible es 0 → 2 (de piso a techo), y luego
//                permitimos cualquier otro movimiento.

//                la grilla se genera como un terreno llano, todas las casillas son planta baja
//                y luego se agregan rampas y techos. Una vez generada la grilla, se chequea que
//                sea válida (es decir, que se cumpla que una rampa sea adyacente a al menos un techo
//                y a una rampa)



TAD coordenada es tupla(entero × entero)

TAD Grilla

    géneros: grilla

    exporta: grilla, observadores básicos, generadores

    usa: bool, nat, coordenada

    igualdad observacional:
        (∀g₁, g₂ : grilla)(g₁ =obs g₂ ⇔ (dimensiones(g₁) = dimensiones(g₂)) ∧L
            (∀ c : coordenada)(coordenadaValida(g₁, c) ⇒ₗ tipoDeCasilla(g₁, c) = tipoDeCasilla(g₂, c)))

    generadores:
        nuevaGrilla : nat n × nat m → grilla                    {n≠0 ∧ m≠0}
        nuevaRampa  : grilla g × coordenada c → grilla          {coordenadaValida(g, c)}
        nuevoTecho  : grilla g × coordenada c → grilla          {coordenadaValida(g, c)}

    observadores básicos:
        dimensiones   : grilla → tupla(entero × entero)
        tipoDeCasilla : grilla g × coordenada c → casilla       {coordenadaValida(g, c)}

    otras operaciones:
        coordDerecha   : coordenada → coordenada
        coordIzquierda : coordenada → coordenada
        coordArriba    : coordenada → coordenada
        coordAbajo     : coordenada → coordenada
        coordenadaValida       : grilla × coordenada → bool
        saltoDeLineaHaciaAtrás : grilla × coordenada → coordenada
        anteriorCoordenada     : grilla × coordenada → coordenada
        casillaVálida          : grilla × coordenada → bool
        grillaVálidaHasta      : grilla × coordenada → bool
        grillaVálida           : grilla → bool


    axiomas: ∀ g : grilla, ∀ c, d :coordenada, ∀ n, m : nat, 
        dimensiones(nuevaGrilla(n, m)) ≡ tupla(n, m)
        dimensiones(nuevaRampa(g, c))  ≡ dimensiones(g)
        dimensiones(nuevoTecho(g, c))  ≡ dimensiones(g)
        tipoDeCasilla(nuevaGrilla(n, m), d) ≡ if coordenadaValida(g, d) then 0 else -1 fi
        tipoDeCasilla(nuevaRampa(g, c), d)  ≡ if coordenadaValida(g, d) then (if c=d then 1 else tipoDeCasilla(g, d) fi) else -1 fi
        tipoDeCasilla(nuevoTecho(g, c), d)  ≡ if coordenadaValida(g, d) then (if c=d then 2 else tipoDeCasilla(g, d) fi) else -1 fi
        
        coordDerecha(c)   ≡ tupla(π₁(c)+1, π₂(c))
        coordIzquierda(c) ≡ tupla(π₁(c)-1, π₂(c))
        coordArriba(c)    ≡ tupla(π₁(c),   π₂(c)+1)
        coordDerecha(c)   ≡ tupla(π₁(c),   π₂(c)-1)

        coordenadaValida(g, c) ≡ 0 ≤ π₁(c) < π₁(dimensiones(g)) ∧ 0 ≤ π₂(c) < π₂(dimensiones(g))

        anteriorCoordenada(g, c) ≡  if π₁(c) > 0 then coordIzquierda(c) else saltoDeLineaHaciaAtrás(g, c) fi

        saltoDeLineaHaciaAtrás(g, c) ≡ tupla(π₁(dimensiones(g))-1, π₂(c)-1)

        casillaVálida(g, c) ≡ if tipoDeCasilla(g, c)≠1 then true else
                                1 ∈ conjuntoDeAdyacentes(g, c) ∧ 2 ∈ conjuntoDeAdyacentes(g, c)
                            fi

        conjuntoDeAdyacentes(g, c) ≡ {tipoDeCasilla(g, coordIzquierda(c)), tipoDeCasilla(g, coordDerecha(c)),
                                      tipoDeCasilla(g, coordArriba(c)),    tipoDeCasilla(g, coordAbajo(c))}

        grillaVálidaHasta(g, c) ≡ if c=tupla(0,0)
                                then CasillaValida(g, tupla(0,0))
                                else CasillaValida(g, c) ∧ grillaVálidaHasta(g, AnteriorCoordenada(c)) fi

        grillaVálida(g) ≡ grillaVálidaHasta(g, tupla(π₁(dimensiones(g)-1, π₂(dimensiones(g)-1))))




        
Fin TAD





TAD Algo2landia
    géneros: algo2landia

    exporta: algo2landia, observadores básicos, generadores

    usa: bool, nat, grilla, coordenada

    igualdad observacional:
        (∀a₁, a₂ : algo2landia)(a₁ =obs a₂ ⇔ (posiciónActual(a₁) =obs posiciónActual(a₂)
                                            ∧ #movimientos(a₁) = #movimientos(a₂)
                                            ∧ grilla(a₁) =obs grilla(a₂)))

    generadores:
        nuevaPartida : grilla g × coordenada c → algo2landia  {grillaVálida(g) ∧ coordenadaValida(g, c)}
        derecha      : algo2landia a → algo2landia            {coordenadaVálida(coordDerecha(posiciónActual(a))  )}
        izquierda    : algo2landia a → algo2landia            {coordenadaVálida(coordIzquierda(posiciónActual(a)))}
        arriba       : algo2landia a → algo2landia            {coordenadaVálida(coordArriba(posiciónActual(a))   )}
        abajo        : algo2landia a → algo2landia            {coordenadaVálida(coordAbajo(posiciónActual(a))    )}
    
    observadores básicos:
        posiciónActual : algo2landia → coordenada
        #movimientos   : algo2landia → nat
        grilla         : algo2landia → grilla

    otras operaciones:
        esMovimientoValido : algo2landia a × coordenada c₁ × coordenada c₂ → bool
                            {coordenadaValida(grilla(a), c₁) ∧ coordenadaValida(grilla(a), c₂)}

    axiomas: ∀ a : algo2landia, ∀ g : grilla, ∀ c : coordenada
        posiciónActual(nuevaPartida(g, c)) ≡ c

        posiciónActual(derecha(a))         ≡ if esMovimientoValido(a, posiciónActual(a), coordDerecha(posiciónActual(a)))
                                                then coordDerecha(posiciónActual(a))
                                                else posiciónActual(a)
                                             fi

        posiciónActual(izquierda(a))       ≡ if esMovimientoValido(a, posiciónActual(a), coordIzquierda(posiciónActual(a)))
                                                then coordIzquierda(posiciónActual(a))
                                                else posiciónActual(a)
                                             fi

        posiciónActual(arriba(a))          ≡ if esMovimientoValido(a, posiciónActual(a), coordArriba(posiciónActual(a)))
                                                then coordArriba(posiciónActual(a))
                                                else posiciónActual(a)
                                             fi

        posiciónActual(abajo(a))           ≡ if esMovimientoValido(a, posiciónActual(a), coordAbajo(posiciónActual(a)))
                                                then coordAbajo(posiciónActual(a))
                                                else posiciónActual(a)
                                             fi

        #movimientos(nuevaPartida(g, c)) ≡ 0
        #movimientos(derecha(a))         ≡ 1 + #movimientos(a)
        #movimientos(izquierda(a))       ≡ 1 + #movimientos(a)
        #movimientos(arriba(a))          ≡ 1 + #movimientos(a)
        #movimientos(abajo(a))           ≡ 1 + #movimientos(a)
        grilla(nuevaPartida(g, c)) ≡ g
        grilla(derecha(a))         ≡ grilla(a)
        grilla(izquierda(a))       ≡ grilla(a)
        grilla(arriba(a))          ≡ grilla(a)
        grilla(abajo(a))           ≡ grilla(a)

        esMovimientoValido(a, c₁, c₂) ≡ ¬(tipoDeCasilla(grilla(a), c₁)=0 ∧ tipoDeCasilla(grilla(a), c₂)=2) 

Fin TAD 