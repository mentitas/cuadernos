#include "Objetivo.h"

Objetivo::Objetivo() : _objeto(""), _receptaculo("") {};

Objetivo::Objetivo(Color objeto, Color receptaculo) : _objeto(std::move(objeto)),
                                                      _receptaculo(std::move(receptaculo)) {}

const Color &Objetivo::objeto() const {
    return _objeto;
}

const Color &Objetivo::receptaculo() const {
    return _receptaculo;
}