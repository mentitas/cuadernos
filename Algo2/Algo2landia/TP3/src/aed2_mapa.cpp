#include "aed2_mapa.h"

aed2_Mapa::aed2_Mapa() {}

aed2_Mapa::aed2_Mapa(Nat ancho, Nat alto, const set<Coordenada> &elevaciones,
                     const map<Color, Coordenada> &receptaculos) : _mapa(ancho, alto, receptaculos) {
    for (Coordenada c: elevaciones)
        _mapa.agregarElevacion(c);
}

void aed2_Mapa::agregarRampa(Coordenada c) {
    _mapa.agregarRampa(c);
}

Nat aed2_Mapa::ancho() const {
    return _mapa.ancho();
}

Nat aed2_Mapa::alto() const {
    return _mapa.alto();
}

TipoCasillero aed2_Mapa::tipoCasillero(Coordenada c) const {
    TipoCasillero tc = PISO;
    if (_mapa.esRampa(c))
        tc = RAMPA;
    else if (_mapa.esElevacion(c))
        tc = ELEVACION;
    return tc;
}

const map<Color, Coordenada> &aed2_Mapa::receptaculos() const {
    return _mapa.receptaculos();
}

Coordenada aed2_Mapa::receptaculo(Color c) {
    return _mapa.receptaculo(c);
}

aed2_Mapa::aed2_Mapa(Mapa m) : _mapa(std::move(m)) {}

Mapa aed2_Mapa::mapa() const {
    return _mapa;
}