#ifndef TP3_ALGO2LANDIA_SIMULACION_H
#define TP3_ALGO2LANDIA_SIMULACION_H

#include "Tipos.h"
#include "Mapa.h"
#include "Objetivo.h"

using namespace std;

class Simulacion {
public:
    Simulacion(Mapa m, Coordenada c, const map<Color, Coordenada> &objetos);

    bool mover(Direccion d);

    void agObjetivo(const Objetivo &o);

    Coordenada posJugador() const;

    Nat cantMovimientos() const;

    const list<Objetivo> &objetivosDisponibles() const;

    Nat cantObjetivosRealizados() const;

    Coordenada posObjeto(const Color &c);

    bool seCumpleObjetivo(const Objetivo &o);

    // Funciones para la traducción

    Mapa mapa() const;

    const map<Color, Coordenada> &objetos() const;

private:
    Coordenada _agente;
    Nat _cantMov;
    Nat _cantObjetivosRealizados;
    list<Objetivo> _objetivosDisponibles;
    Trie<Trie<list<Objetivo>::iterator>> _diccObjetivos; // dicc(colorOb, dicc(colorCa, it))
    Mapa _mapa;
    vector<vector<Color>> _posObj; // los colores de los objetos
    Trie<Coordenada> _objPos;      // dicc(colorOb, coordenada)

    static Coordenada _siguientePosicion(Coordenada c, Direccion d);

    // Traduccion
    map<Color, Coordenada> _objetos;
};


#endif //TP3_ALGO2LANDIA_SIMULACION_H