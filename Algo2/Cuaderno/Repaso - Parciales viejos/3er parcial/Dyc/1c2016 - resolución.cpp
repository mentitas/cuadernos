// Ejercicio 3. Dividir y conquistar

// Dado un arreglo de n>2 intervalos cerrados de números naturales I₁,...,Iₙ (cada uno representado como un par
// <Iinf, Isup>), se desea encontrar dos de ellos que maximicen su intersección, es decir, un par de índices i
// y j con 1 ≤ i < j ≤ n tal que la cantidad de valores dentro de la intersección de los intervalos Iᵢ ∩ Iⱼ sea
// máxima entre todos los intervalos de entrada. Sugerencia: ordenar los intervalos por su extremo izquierdo.

// a. Dar un algoritmo que use la técnica de Divide and Conquer y resuelva el problema en tiempo O(n*log(n)) en 
// el peor caso.n

// b. Marcar claramente qué partes del algoritmo se corresponden a dividir, conquistar y unir subproblemas.

// c. Justificar detalladamente que el algoritmo cumple con la complejidad pedida.



res = tupla⟨i,j,max⟩

a.

// Idea:
// Separar el arreglo en subarreglos sin huequitos. O(n)
// En cada subarreglo hago dyc:
// - Caso base: devuelvo rta trivial
// - Caso recursivo: devuelvo el maximo entre la recuI, recuD y el medio

// Medio ≡ el minimo de la derecha × el maximo de la izquierda (con reglas)


int intersección(pair<int,int> a, pair<int,int> b){
	if(b.first >= a.second || a.first >= b.second){
		// No se intersecan
		// Gráficamente:
		// caso 1. (a₀         aₙ) (b₀         bₙ)
		// caso 2. (b₀         bₙ) (a₀         aₙ)
		return 0;
	} else {
		// Se intersecan
		// Gráficamente:
		// caso 1. (a₀        (b₀   aₙ)          bₙ)
		// caso 2. (a₀        (b₀   bₙ)          aₙ) ← (b ⊆ a)
		return min(a.second, b.second) - max(a.first, b.first);
	}
}