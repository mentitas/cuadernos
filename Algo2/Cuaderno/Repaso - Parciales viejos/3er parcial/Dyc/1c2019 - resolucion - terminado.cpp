// Idea 1:

// > Descompongo todos los elementos en su forma binómica. O(n)
// > Ordeno el nuevo arreglo según el primer elemento del binomio. O(n*log(n))
// > Busco la cadena más larga. O(n)

// Pro:    O(n*log(n))
// Contra: no uso dyc



// Idea 2: procuro usar dyc

// > Descompongo todos los elementos en su forma binómica. O(n)
// > Ordeno el nuevo arreglo y en simultáneo armo la forma trinómica. O(n*log(n))
// > Busco la cadena más larga. O(n)

// Pro:    O(n*log(n))
// Contra: Es lo mismo de arriba pero chamuyando un dyc

// Voy a usar los renombres:
// dupla  = pair<int,int>
// tripla = pair<int,int,int>



*** Explicación algoritmo ***
Yo quiero encontrar la longitud de la cadena más larga del arreglo A. 
Para ello, primero descompongo todos los elementos del arreglo en su forma trinomial con la operación
descomponerEnTrinomios.

En descomponerEnTrinomios es donde ocurre el dyc.

El caso base de descomponerEnTrinomios es cuando se está evaluando un sólo elemento, y es ahí cuando
devuelve una respuesta trivial, ya que sólo debo convertir un elemento de su forma entera a trinomial.

> Dado un número entero n, su forma binomial (b,p) la consigo en O(1) con la operación Descomponer
> Dado un binomio de la forma (b,p), su trinomio es (b,p,p)

En el caso recursivo de descomponerEnTrinomios, llamo recursivamente a la operación en las dos mitades
del arreglo en 2T(n/2). Ahora, tengo dos arreglos con trinomios ordenados de manera creciente y voy a mergearlos.
Para mergearlos uso la operación superMergeRECRAZY, que es O(n)

superMergeRECRAZY (perdón el nombre) recibe dos arrays de triplas (ambos ordenados de manera creciente) y
los mergea. Si es posible unir trinomios, los une, y sino los deja como está. El costo de la operación es O(n).

Luego de aplicar descomponerEnTrinomios en mi arreglo, ahora tengo un arreglo de trinomios ordenados de manera
creciente según la primer componente de la tripla.
Ahora, sólo me queda encontrar la tripla con mayor longitud, lo cuál es O(n).




*** Algoritmos ***

int longitudCadenaMásLarga(array<int> A){
	A_trinomial = descomponerEnTrinomios(A, 0, A.size());             O(n*log(n))
	return longitudMayorTrinomio(A_trinomial);                        O(n)
}

array<tripla> descomponerEnTrinomios_aux(array<int> A, int l, int r){ // Rango [l,r)
	
	int n = r-l;                                                          θ(1)

	if (n==1) {                                                           θ(1)
		dupla d = descomponer(A[l]);                                      θ(1)
		return make_pair(d.first, d.second, d.second);                    θ(1)
	} else {

		int m = (l+r)/2;                                                  θ(1)
		
		array<tripla> recursionIzq = descomponerEnTrinomios_aux(A,l,m);   T(n/2)
	    array<tripla> recursionDer = descomponerEnTrinomios_aux(A,m,r);   T(n/2)

	    return superMergeRECRAZY(recursionIzq,recursionDer);              θ(n)
	}
}


// Pre: A y B están ordenadas de manera creciente según el primer elemento de cada tripla
array<tripla> superMergeRECRAZY(array<tripla> A, array<tripla> B){

	list<tripla> resLista;                                     θ(1)

	int a=0;                                                   θ(1)
	int b=0;                                                   θ(1)


	while(a<A.size() && b<B.size()){                           θ(n)

		bA = A[a].first;                                       θ(1)
		bB = B[b].first;                                       θ(1)
		pA = A[a].second;                                      θ(1)
		pB = B[b].second;                                      θ(1)
		qA = A[a].third;                                       θ(1)
		qB = B[b].third;                                       θ(1)

		if(bA == bB && qA+1==pB) {                             θ(1)
			// Entonces uno las triplas
			resLista.push_back(make_pair(bA,pA,qB));           θ(1)
			a++;                                               θ(1)
			b++;                                               θ(1)
		} else {
			// No uno las triplas, las agrego en orden
			if (bA > bB || (bA==bB && pA > pB)) {              θ(1)
				resLista.push_back(B[b]);                      θ(1)
				b++;                                           θ(1)
			} else {
				resLista.push_back(A[a]);                      θ(1)
				a++;                                           θ(1)
			}
		}
	}

	// θ(n):
	while(a<A.size()){
		resLista.push_back(A[a]);                              θ(1)            
		a++;                                                   θ(1)   
	}

	while(b<B.size()){
		resLista.push_back(B[b]);                              θ(1)            
		b++;                                                   θ(1) 
	}

	return list_to_array(resLista);                            θ(n)
}

int longitudMayorTrinomio(array<tripla> A){
	int max = 0;                              θ(1)

	for(tripla t : A){                        θ(n)
		if (t.third-t.second > max){          θ(1)
			max = t.third-t.second;           θ(1)
		}
	}

	return max;                               θ(1)
}







*** Demostración complejidad de descomponerEnTrinomios_aux ***

T(n) = 2T(n/2) + θ(n)

Observación: tiene la misma complejidad que el MergeSort
⟹ La complejidad es O(n*log(n))













// Pre: A está ordenado de menor a mayor de manera estricta
int longitudCadenaMásLarga(array<int> A){
	A_trinomial = descomponerEnTrinomios(A); // dyc!!
	return longitudMayorTrinomio(A_trinomial);
}


4    =  (1,2)        = (1,2,2)
5    =  (5,0)        = (5,0,0)
10   =  (5,1)        = (5,1,1)
5,10 = [(5,0),(5,1)] = (5,1,2)


array<trinomio> descomponerEnTrinomios(array<int> A, int l, int r){ // Rango [l,r)
	int n = r-l;
	if (n == 1){
		// descomponemos el unico elemento, es trivial

	} else {

		int m = (l+r)/2;

		array<trinomio> recursionIzq = descomponerEnTrinomios(A,l,m);
		array<trinomio> recursionDer = descomponerEnTrinomios(A,m,r);

		return merge(recursionDer, recursionIzq);
	}
}

