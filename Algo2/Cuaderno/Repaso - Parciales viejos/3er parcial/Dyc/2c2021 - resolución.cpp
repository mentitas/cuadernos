// Ej. 2. Divide and Conquer
// Como parte de un proyecto de generación automática de contenido audiovisual para Internet, se desea que, a partir
// de una lista dada, combinar aquellos videos consecutivos que traten sobre un mismo tema, para obtener pelı́culas
// de la mayor duración posible.

// Al momento se cuenta con dos operaciones ya realizadas, las cuales que no se pueden modificar:

// > MismoTema(in v : video, in v' : video) → res : bool, que nos dice si dos videos v y v' tratan sobre el mismo
// tema. Complejidad temporal: θ(1).
// > Combinar(in v : video, in v' : video) → res : video, que combina dos videos v y v'. Complejidad temporal:
// Θ(d(v) + d(v')), siendo d(x) la duración del video x.

// Dar un algoritmo que utilice la técnica Divide and Conquer para calcular la función

// Peliculas(in V : arreglo(video)) → res : arreglo(video)

// Por ejemplo, siendo los videos v1 , v3 y v4 del mismo tema, Peliculas([v1,v2,v3,v4]) → [v1,v2,v3−4]

// Se pide que el algoritmo tenga complejidad temporal O(n + (D log n)), donde D = ∑^n_i=1 d(V[i]) (es decir, es
// la suma de las duraciones de todos los videos). Justificar detalladamente que el algoritmo efectivamente resuelve
// el problema y demostrar formalmente que cumple con la complejidad solicitada. Para esto último, se deberá utilizar
// alguno de los métodos vistos de acuerdo a las caracterı́sticas del problema que se está resolviendo.



void Peliculas (array<video> &V, int l, int r){ // Rango: [l,r)

	int n = r-l;

	if (n==1){
		// No hago nada, ya terminé                            θ(1)
	}

	int m = (l+r)/2;

	recursiónIzq = Peliculas(V, l, m);
	recursiónDer = Peliculas(V, m, r);

	if (mismoTema?(V[m], V[m+1])){
		// V[m] = Combinar(V[m], V[m+1])
		// Quito V[m+1]
	} else {
		// No hago nada
	}
}