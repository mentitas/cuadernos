// Ej. 1. Sorting
// Ante la terrible crisis sanitaria que estamos viviendo, los hospitales están pidiendo ayuda. Tienen una lista
// con los pacientes, donde se incluye el apellido, nombre y un número real que representa la gravedad de su
// estado, en ese orden. La gravedad va del 1.0 al 10.0, donde 10.0 representa el caso más grave posible. La
// lista está ordenada de menor a mayor por apellido, pero necesitan ordenarla según la gravedad del caso, de
// forma de ir atendiendo a aquellas personas con mayor riesgo de vida.

// a. Como la lista es muy larga y la necesitan con urgencia, nos piden ordenarla en O(n), considerando que
// como máximo n/log(n) pacientes tienen valores de gravedad con más de dos decimales. Nos piden,
// además, que el ordenamiento sea estable. Describir en detalle el algoritmo a utilizar y las estructuras
// adicionales necesarias, si las hubiese. Justificar la complejidad y la estabilidad de la solución, y mostrar
// que efectivamente resuelve el problema.



Donde paciente es tupla(apellido : string,
	                    nombre   : string,
	                    urgencia : float)

void ordenarSegúnUrgencia(list<int> &L){

	int n = L.size();

	// Armo un Bucket con los elementos de menos de 2 decimales y una lista para los elementos de más de
	// 2 decimales. Entre el 1.00 y el 10.00 hay 900 elementos, entonces el Bucket va a tener 900 posiciones
	
	vector<list<paciente>> buckets(900, {});
	list<paciente> masDeDosDecimales;

	for(paciente p : L){ // θ(1)
		if(TieneDosDecimales(p.urgencia())){ // O(1)
			masDeDosDecimales.push_back(p);  // θ(1)
		} else {
			buckets[1000 - p.urgencia()*100].push_back(p); // θ(1)
		}
	}

	// Reconstruyo

	list<paciente> L_aux;

	for(list<paciente> l : buckets){ // θ(n)
		for(paciente p : l){ 
			L_aux.push_back(p);      // θ(1);
		}
	}

	// Ordeno masDeDosDecimales (es estable, los apellidos siguen en orden creciente)
	MergeSort_decreciente(masDeDosDecimales) // O(m*log(m)) = O(n/log(n) * log(n/log(n))) = O(n/log(n) * log(n)) = O(n)

	// Mergeo las dos listas 
	L = Merge_decreciente(L_aux, masDeDosDecimales); // O(n)
}


// O(|A|+|B|)
lista<paciente> Merge(lista<paciente> A, lista<paciente> B){
	list<paciente> res;       // θ(1)
	list<paciente> A_aux = A; // O(A.size())
	list<paciente> B_aux = B; // O(B.size())

	while(A_aux.size()>0 && B_aux.size()>0){ 
		if(A_aux[0].urgencia() > B_aux[0].urgencia()){ // θ(1)             
			res.push_back(A_aux[0]); // θ(1)
			A_aux.pop_front();       // θ(1)            
		} else {
			res.push_back(B_aux[0]); // θ(1)
			B_aux.pop_front();       // θ(1)         
		}
	}

	while(A_aux.size()>0){               
		res.push_back(A_aux[0]); // θ(1)
		A_aux.pop_front();       // θ(1)        
	}

	while(B_aux.size()>0){              
		res.push_back(B_aux[0]); // θ(1)
		B_aux.pop_front();       // θ(1)        
	}

	return res;
}


// b. ¿Qué pasa si nos piden que para dos personas con la misma gravedad aparezca primero en la lista
// ordenada la de mayor apellido, invirtiendo el orden original? ¿Qué y cómo cambiarı́a el algoritmo
// propuesto? Justificar.

Mi algoritmo es estable, por lo que ahora mismo los pacientes están ordenados según la urgencia de manera
decreciente y luego según su apellido de manera creciente.
Si quisiera invertir el orden de los apellidos, modificaría el algoritmo en la línea 35 y 37 para que en
vez de hacer push_back haga push_front.


// c. Teniendo en cuenta que luego de haber ordenado la lista según la gravedad de cada paciente la lista
// mantiene su orden relativo según el apellido del paciente, ¿cómo podrı́amos volver al orden anterior
// según el apellido en O(n)? Justificar.





// Algunas observaciones:
// > Puede suponerse que se cuenta con una función TieneDosDecimales(num) que indica en O(1) si un
// número tiene dos cifras decimales significativas o menos.
// > Puede suponerse que, sólo para este problema, comparar dos reales tiene costo O(1)

