// Ej. 2. Ordenamiento

// Un coleccionista de figuras de acción de pelı́culas quiere ordenar su vitrina. Los objetos los clasifica
// según nombre del personaje, grupo de pelı́culas y número de pelı́cula. Puede tener varias figuras de acción
// repetidas. Los nombres de los personajes y de las pelı́culas tienen una longitud no mayor a 100, y pueden
// contener cualquier tipo de caracteres (podemos suponer que conocemos la relación de orden de estos
// caracteres). Los números de pelı́culas son enteros entre 1 y p inclusive. Se sabe que no existen más de
// 50 personajes por pelı́cula.
// El coleccionista desea ordenar primero por grupo, luego por número de pelı́cula y finalmente por personaje.
// Si tiene figuras repetidas, desea conservar solo una de ellas.

// Por ejemplo, si G = [“Star wars”, “El señor de los anillos’, “Volver al futuro”], p = 7 y
// C =[
//      (“Leia Organa/Skywalker” ,“Star wars”,               7)
//      (“Frodo Baggins”         ,“El señor de los anillos”, 2)
//      (“Samwise Gamgee (Sam)”  ,“El señor de los anillos”, 1)
//      (“Luke Skywalker”        ,“Star wars”,               4)
//      (“Emmett ((Doc)) Brown”  ,“Volver al futuro”,        1)
//      (“Leia Organa/Skywalker” ,“Star wars”,               4)
//      (“Samwise Gamgee (Sam)”  ,“El señor de los anillos”, 1)
//      (“Emmett ((Doc)) Brown”  ,“Volver al futuro”,        1)
// ]

// la salida debe ser

// C ord = [
//      (“Samwise Gamgee (Sam)”  ,“El señor de los anillos”, 1)
//      (“Frodo Baggins”         ,“El señor de los anillos”, 2)
//      (“Leia Organa/Skywalker” ,“Star wars”,               4)
//      (“Luke Skywalker”        ,“Star wars”,               4)
//      (“Leia Organa/Skywalker” ,“Star wars”,               7)
//      (“Emmett ((Doc)) Brown”  ,“Volver al futuro”,        1)
// ]

// Dar un algoritmo de tiempo O(g*log(g) + gp + n*log(g)) que ordene la colección y elimine los repetidos,
// donde n es la cantidad de elementos de C y g es la cantidad de elementos de G. Explicar por qué el
// algoritmo propuesto cumple con todo lo pedido.
// Aclaración: se puede asumir que la comparación entre dos caracteres es constante.


// g = cantidad de películas
// n = cantidad de figuras

// Donde Figura es pair<nombre, pelicula, numero>

void OrdenarVitrina(vector<pelicula> &G, int p, vector<Figura> &C){

	int n = C.size();
	int g = G.size();

	// Ordeno G alfabéticamente. 
	MergeSort(G); // O(g*log(g))

	// Separo los elementos en buckets según la película. Respeto el orden de las películas en G.
 	vector<list<pair<personaje,int>> Buckets_list(g, list::empty()); // O(g)

 	for(Figura f : C){             // O(n*log(g))
 		int i = buscar(G, f.pelicula());  // Uso búsqueda binaria, es O(log(g))
 		Buckets_list[i].push_back(f);     // θ(1)
 	}

 	vector<

}


// g = cantidad de películas
// n = cantidad de figuras

// Donde Figura es pair<nombre, pelicula, numero>

void OrdenarVitrina(vector<pelicula> &G, int p, vector<Figura> &C){

	int n = C.size();
	int g = G.size();

	// voy a hacer un radix loco

	// Ordeno según el nombre del personaje

	// No se cuántos caracteres voy a precisar → no puedo usar radix
	// Se que la longitud es menor que 100
	// Se que hay menos de 50 personajes por película
	// Hacer esto es O(nlog(n))

	// Ordeno según el número de la película
	// Ordeno según el nombre de la película

}


// g = cantidad de películas
// n = cantidad de figuras

// Donde Figura es pair<nombre, pelicula, numero>

void OrdenarVitrina(vector<pelicula> &G, int p, vector<Figura> &C){

	int n = C.size();
	int g = G.size();


	// Ordeno G alfabéticamente: O(g*log(g))
	
	// Separo los personajes en buckets según el grupo de la película: O(n*log(g))

	// Los vuelvo a separar en p buckets según el número de la película: O(n) + O(p)

	// El tamaño de cada bucket es menor o igual a 50

	// Ordeno según el nombre del personaje: Selection Sort, O(1)

	// Reconstruyo C: Recorro g buckets_pelicula con p bucket_numero cada uno, es g*p
}




// g = cantidad de películas
// n = cantidad de figuras

// Donde Figura es pair<nombre, pelicula, numero>

void OrdenarVitrina(vector<pelicula> &G, int p, vector<Figura> &C){

	int n = C.size();
	int g = G.size();


	// Ordeno G alfabéticamente: O(g*log(g))
	MergeSort(G);
	
	// Armo los buckets: O(g*p)
	vector<vector<Nombre>> Bucket(p, make_pair(0, vector<Nombre>(50,""))); // O(p)
	vector<vector<vector<Nombre>>> MasterBucket(g, Bucket_num);            // O(g*p)


	// MasterBucket       : vector<vector<vector<Nombre>>>
	// MasterBucket[g]    : vector<vector<Nombre>>
	// MasterBucket[g][p] : pair<int,vector<Nombre>>

	// Master bucket: g×p×50

	// Separo los personajes en buckets según el grupo de la película y su número: O(n*log(g))
	
	for(Figura f : C){                // O(n*log(g))
		Pelicula peli = f.pelicula(); // θ(1)
		Nombre nombre = f.nombre();   // θ(1)
		int numero    = f.numero();   // θ(1)

		int i = Buscar(G,peli)        // O(log(g))
		int j = BuscarPrimerVacio()

		// MasterBucket[i][numero] // Es el pair <int, vector<Nombre>>

		int posEnVector = MasterBucket[i][numero].first;        // θ(1)          
		(MasterBucket[i][numero].second)[posEnVector] = nombre; // θ(1)     
		MasterBucket[i][numero].first++;                        // θ(1)    
	}

	// El tamaño de cada bucket es menor o igual a 50

	// Los buckets con los nombres son más grandes de los necesario, los voy a achicar

	int tamaño=0;
	for(int i=0; i<g; i++){      // O(g*p)
		for(int j=0; j<p; j++){  // O(p)
			tamaño = MasterBucket[i][j].first           // θ(1)
			vector<Nombre> nuevoVector(tamaño, "");     // O(tamaño) = O(50) = O(1)
			for(int k=0; k<tamaño; k++){                // O(tamaño) = O(50) = O(1) 
				nuevoVector[k] = MasterBucket[i][j][k]; // θ(1)
			}
			MasterBucket[i][j] = nuevoVector;		    // O(tamaño) = O(50) = O(1)
		}
	}


	// Ordeno según el nombre del personaje: Selection Sort, O(1)

	// Reconstruyo C: Recorro g buckets_pelicula con p bucket_numero cada uno, es g*p
}




// g = cantidad de películas
// n = cantidad de figuras

// Donde Figura es pair<nombre, pelicula, numero>

void OrdenarVitrina(vector<pelicula> &G, int p, vector<Figura> &C){

	int n = C.size();
	int g = G.size();

	// Ordeno G alfabéticamente: O(g*log(g))
	MergeSort(G);
	
	// Armo los buckets: O(g*p)
	vector<vector<Nombre>> MiniBucket(p, vector<Nombre>(50,""));    // O(p)
	vector<vector<vector<Nombre>>> MasterBucket(g, MiniBucket);     // O(g*p)

	// MasterBucket       : vector<vector<vector<Nombre>>>
	// MasterBucket[g]    : vector<vector<Nombre>>
	// MasterBucket[g][p] : vector<Nombre>

	// Master bucket: g×p×50

	// Separo los personajes en buckets según el grupo de la película y su número: O(n*log(g))
	
	for(Figura f : C){                // O(n*log(g))
		Pelicula peli = f.pelicula(); // θ(1)
		Nombre nombre = f.nombre();   // θ(1)
		int numero    = f.numero();   // θ(1)

		int posPeli = Buscar(G,peli)  // O(log(g)), uso búsqueda binaria porque G está ordenado

		MasterBucket[posPeli][numero].push_back(nombre) // θ(long(vector)) = θ(50) = θ(1)
	}

	// El tamaño de cada bucket es menor o igual a 50 B^)

	// Ordeno según el nombre del personaje: Selection Sort, O(g*p)

	for(int i=0; i<g; i++){     // O(g*p)
		for(int j=0; j<p; j++){ // O(p)
			SelectionSort(MasterBucket[i][j]); // O(|MasterBucket[i][j]|²) = O(50²) = O(1)
		}
	}

	// Quito personajes repetidos: O(g*p)

	for(int i=0; i<g; i++){     // O(g*p)
		for(int j=0; j<p; j++){ // O(p)
			QuitarRepetidos(MasterBucket[i][j]); // O(|MasterBucket[i][j]|) = O(50) = O(1)
		}
	}

	// Reconstruyo C: Recorro g buckets_pelicula con p bucket_numero cada uno, es g*p

	int c=0; // Recorre c
	int d=0; // Recorre los grupos de peliculas
	int e=0; // Recorre ĺas películas
	int f=0; // Recorre los nombres de los personajes


	// Pelicula > Numero > Listado de Nombres


	while(c<n){
		Pelicula peliActual = G[d];

		


	}


}