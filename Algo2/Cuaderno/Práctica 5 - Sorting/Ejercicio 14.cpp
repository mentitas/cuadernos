// Ejercicio 14

// Se tiene un arreglo A de n números naturales y un entero k. Se desea obtener un arreglo B
// ordenado de n × k naturales que contenga los elementos de A multiplicados por cada entero
// entre 1 y k, es decir, para cada 1 ≤ i ≤ n y 1 ≤ j ≤ k se debe incluir en la salida el
// elemento j × A[i]. Notar que podrı́a haber repeticiones en la entrada y en la salida.

// a) Implementar la función
// ordenarMúltiplos(in A: arreglo(nat), in k: nat) → arreglo(nat)
// que resuelve el problema planteado. La función debe ser de tiempo O(nk log n), dónde n = tam(A).

// b) Calcular y justificar la complejidad del algoritmo propuesto.

// Idea 1: primero multiplico y después ordeno
vector<int> ordenarMúltiplos(vector<int> A, int k){
	int n = A.size();       // O(1)
	vector<int> res(n*k,0); // O(n*k)

	int i=0; // Recorre A
	int j=0; // Recorre res
	int l=0; // Multiplicador

	while(i<n){                // O(n*k)
		l=0;                   // O(1)
		while(l<k){            // O(k)
			res[j] = A[i]*l    // O(1)
			j++;               // O(1)
			l++;               // O(1) 
		}                             
		i++;                   // O(1) 
	}

	MergeSort(res);            // O(n*k*log(n*k))

	return res; // O(n*k)

}



// Idea 2:
// Hago buckets según qué K está multiplicando al A[i];
// Ordeno los buckets O(k*n*log(n))
// Reconstruyo

vector<int> ordenarMúltiplos(vector<int> A, int k){
	int n = A.size();               // O(1)
	vector<list<int>> bucket(k,{}); // O(k)

	int i=0; // Recorre A
	int j=0; // Recorre bucket

	while(i<n){                              // O(n*k)
		j=0;                                 // O(1)
		while(j<k){                          // O(k)
			bucket[j].push_back(A[i]*(j+1))  // O(1)
			j++;                             // O(1) 
		}                             
		i++;                                 // O(1) 
	}

	for(list<int> l : bucket){  // O(k*n*log(n))
		MergeSort(l);           // O(n*log(n))
	}

	vector<int> res = {};

	for(list<int> l : bucket){  // O(n*k)
		res = Merge(res, l);    // O(n)
	}

	return res; // O(n*k)

}

// La complejidad total del algoritmo es O(k*n*log(n))