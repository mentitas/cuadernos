// Ejercicio 3
//
// Escribir un algoritmo que encuentre los k elementos más chicos de un arreglo de dimensión n,
// donde k ≤ n. ¿Cuál es su complejidad temporal?¿A partir de qué valor de k es ventajoso ordenar
// el arreglo primero?


// v.size() = n
vector<int> losKMasPeques(vector<int> &v const){ // v es pasado por referencia no modificable
	vector<int> res = {}; // O(1)

	int minimo = 0;          // O(1)
	int siguienteMinimo = 0; // O(1)

	minimo = buscoMinimo(v) // O(n)
	res.push_back(minimo);  // O(1)

	while(res.size() < k){                                // O(k*n)                         
		siguienteMinimo = buscoMinimoMayorQue(v, minimo); // O(n)
		res.push_back(siguienteMinimo);                   // O(1)
		minimo = siguienteMinimo;                         // O(1)

	}

	return res; // O(k)
}

// La complejidad total del algoritmo es O(k*n)



vector<int> losKMasPeques_Sorting(vector<int> v){ // v es pasado por referencia no modificable
	vector<int> res = {};       // O(1)
	vector<int> v_ordenado = v; // O(n)

	mergeSort(v_ordenado);      // O(n log(n))

	for(int i=0; i<k; i++){           // O(k)
		res.push_back(v_ordenado[i]); // O(1)
	}

	return res; // O(k)
}

// La complejidad total del algoritmo es O(n log(n) + k)
// Como k ≤ n, entonces:

// O(n log(n) + k) ⊆ O(n log(n) + n) ≤ O((n+1) log(n)) ⊆ O(n log(n))

// Finalmente, la complejidad del algoritmo es O(n log(n))


// RTA: si losKMasPeques: O(k*n) y losKMasPeques_sorting: O(n*log(n)),
// entonces conviene usar losKMasPeques para cuando k < log(n).