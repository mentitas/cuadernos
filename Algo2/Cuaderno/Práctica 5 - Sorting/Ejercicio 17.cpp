// Ejercicio 17 ★
// Se tiene un arreglo de enteros no repetidos A[1..n], tal que se sabe que para todo i hay a lo sumo i
// elementos mas chicos que A[i] en todo el arreglo. Dar un algoritmo que ordene el arreglo en O(n).

// A=[5,2,4,3,0,1]
// A[1] = 5, a lo sumo hay 1 elemento menor que 5 
// A[2] = 2, a lo sumo hay 2 elementos menores que 2 (0 y 1)


// Observación: en A[1] está el más chico o el segundo más chico

// A=[1,0,5,3,4,2]
// A[1]=1, a lo sumo hay 1 elemento menor que 1    ← ¡se cumple!
// A[2]=0, a lo sumo hay 2 elementos menores que 0 ← ¡se cumple!


// Observación: el tercer elemento no puede estar en A[1]

// A=[2,1,0,5,3,4]
// A[1]=2, a lo sumo hay 1 elemento menor que 2 ← ¡no se cumple!

// Observación: el cuarto elemento no puede estar en A[2]

// A=[1,3,0,5,2,4]
// A[1]=1, a lo sumo hay 1 elemento menor que 1    ← ¡se cumple!
// A[2]=3, a lo sumo hay 2 elementos menores que 3 ← ¡no se cumple!


//    1 2 3 4 5 6
// A=[1,0,3,2,5,4]
// A[1]=1, a lo sumo hay 1 elemento menor que 1    ← ¡se cumple!
// A[2]=0, a lo sumo hay 2 elementos menores que 0 ← ¡se cumple!
// A[3]=3, a lo sumo hay 3 elementos menores que 3 ← ¡se cumple!
// A[4]=2, a lo sumo hay 4 elementos menores que 2 ← ¡se cumple!
// A[5]=5, a lo sumo hay 5 elementos menores que 5 ← ¡se cumple!
// A[6]=4, a lo sumo hay 6 elementos menores que 4 ← ¡se cumple!


// el tercer elemento no puede estar en el primer  lugar
// el cuarto elemento no puede estar en el segundo lugar
// el quinto elemento no puede estar en el tercer  lugar
// etc

// Observación: el máximo está en el último o anteúltimo lugar

// Observación: 
// Si A está ordenado y e = A[k] (k ∈ ℕ, 1 ≤ k ≤ n), entonces en el arreglo desordenado
// e se encuentra en una posición mayor o igual a k-1.


// Formalmente:
// |A| = |A'| = n
// ∧ A' = ordenar(A)
// ∧ (∀k:ℕ)(1 ≤ k ≤ n ⟹L (∃k':ℕ)(k-1 ≤ k' ≤ n ∧L A[k] = A'[k']))



//    1 2 3 4 5 6
// A=[1,0,3,4,5,2]
// A[1]=1, a lo sumo hay 1 elemento menor que 1    ← ¡se cumple!
// A[2]=0, a lo sumo hay 2 elementos menores que 0 ← ¡se cumple!
// A[3]=3, a lo sumo hay 3 elementos menores que 3 ← ¡se cumple!
// A[4]=4, a lo sumo hay 4 elementos menores que 4 ← ¡se cumple!
// A[5]=5, a lo sumo hay 5 elementos menores que 5 ← ¡se cumple!
// A[6]=2, a lo sumo hay 6 elementos menores que 2 ← ¡se cumple!


A=[0,1,2,3,4,5]
// A[1] ∈ {0,1}
// A[2] ∈ {0,1,2}
// A[3] ∈ {0,1,2,3}
// A[4] ∈ {0,1,2,3,4}
// A[5] ∈ {0,1,2,3,4,5}
// A[6] ∈ {0,1,2,3,4,5}


void ordenarArregloLoco(vector<int> &A){
	int n=A.size();         // θ(1)
	for(int i=n; i>0; i--){ // θ(n)		
		if(v[j]>v[j-1]){    // θ(1)
			// V[j] es el máximo del subarreglo A[0..j], ya está bien ubicado
		} else {
			// V[j-1] es el máximo del subarreglo A[0..j], lo ubico donde corresponde
			swap(A, j, j-1); // θ(1)
		}
	}
}

// La complejidad total es θ(n)