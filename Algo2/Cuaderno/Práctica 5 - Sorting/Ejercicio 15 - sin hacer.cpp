// Ejercicio 15

// Dado un conjunto de naturales, diremos que un agujero es un natural x tal que el conjunto no contiene a x
// y sı́ contiene algún elemento menor que x y algún elemento mayor que x.
// Diseñar un algoritmo que, dado un arreglo de n naturales, diga si existe algún agujero en el conjunto de los
// naturales que aparecen en el arreglo. Notar que el arreglo de entrada podrı́a contener elementos repetidos, pero
// en la vista de conjunto, no es relevante la cantidad de repeticiones.

// a) Implementar la función
// tieneAgujero?(in A: arreglo(nat)) → bool
// que resuelve el problema planteado. La función debe ser de tiempo lineal en la cantidad de elementos de
// la entrada, es decir, O(n), dónde n = tam(A).

// Ideas:

// > Si busco el mínimo y el máximo, y hay menos elementos que maximo-minimo, entonces hay una agujero
// > Counting no.
// > No me alcanza para ordenarlo???

// Tiene agujero:    [1,2, ,4,5,6],  [6,2,1,4,5]
// No tiene agujero: [1,2,3,4,5,6],  [6,2,1,3,4,5]

// Un array sin agujeros tiene al menos maximo-minimo elementos



// Son naturales, puedo hacer radixSort????


bool hayAgujero(vector<int> A){
	int n=A.size();  // θ(1)
	bool res = true; // θ(1)

	vector<int> A_aux = A; // O(n)
	
	RadixSort(A_aux); // O(d*n), siendo d la cantidad de dígitos del elemento más largo


	for(int i=1; i<n; i++){                                     // O(n)
		if(A_aux[i-1] != A_aux[i] && A_aux[i-1] != A_aux[i]+1){ // θ(1)
			res = false;                                        // θ(1)
		}
	}

	return res; // O(n)
}



// b) Calcular y justificar la complejidad del algoritmo propuesto.