// Ejercicio 16

// Se tiene un arreglo A de n números naturales. Sea m := máx{A[i] : 1 ≤ i ≤ n} el máximo del arreglo. Se
// desea dar un algoritmo que ordene el arreglo en O(n*log(m)), utilizando únicamente arreglos y variables
// ordinarias (i.e., sin utilizar listas enlazadas, árboles u otras estructuras con punteros).

// a) Implementar la función
// raroSort(in A: arreglo(nat)) → bool
// que resuelve el problema planteado. La función debe ser de tiempo O(n*log(m)), dónde n = tam(A) y
// m = máx{A[i] : 1 ≤ i ≤ n}.










// b) Calcular y justificar la complejidad del algoritmo propuesto.

// c) Hay al menos 3 formas de resolver este ejercicio, pensar las que se puedan, discutir luego con los compañeros
// las que encontraron ellos y si hace falta, consultar.