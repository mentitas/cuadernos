// O(n log(n))
vector<int> MergeSort(vector<int> &v, int desde, int hasta){ // desde y hasta incluyen
	int n = v.size();
	if (n<2){
		// ya está ordenado
	} else {
		int medio = (desde+hasta) div 2;
		vector<int> A = MergeSort(v, desde,   medio);
		vector<int> B = MergeSort(v, medio+1, hasta);
		v = Merge(A, B);
	}
	return v;
}

template<class T>
vector<T> Merge(vector<T> A, vector<T> B){

	vector<T> res(A.size()+B.size()); // O(|A| + |B|)

	int a=0;   // O(1)
	int b=0;   // O(1)
	while(a<A.size() && b<B.size()){  // O(|A| + |B|)
		if(A[a] < B[b]){              // O(1)
			res.push_back(A[a]);      // O(1)
			a++;                      // O(1)
		} else {
			res.push_back(B[b]);      // O(1)
			b++;
		}
	}

	while(a<A.size()){         // O(|A|)
		res.push_back(A[a]);   // O(1)
		a++;                   // O(1)
	}

	while(b<B.size()){         // O(|B|)
		res.push_back(B[b]);   // O(1)
		b++;                   // O(1)
	}

	return res;                // O(1)
}