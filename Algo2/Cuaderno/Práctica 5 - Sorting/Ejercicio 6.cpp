// Ejercicio 6 ★
// Sea A[i...n] un arreglo que contiene n números naturales. Diremos que un arreglo de posiciones
// [i...j], con 1 ≤ i ≤ j ≤ n, contiene una escalera en A si valen las siguientes dos propiedades

// 1. (∀k:nat)(i ≤ k < j ⟹L A[k+1] = A[k]+1) (esto es, los elementos no sólo están ordenados en
//    forma creciente, sino que además el siguiente vale exactamente uno más que el anterior)

// 2. Si 1 < i entonces A[i] ≠ A[i-1]+1 y si j<n entonces A[j+1] ≠ A[j]+1 (la propiedad es 
//    maximal, es decir que el rango no puede extenderse sin que deje de ser una escalera
//    según el punto anterior)

// Se puede verificar fácilmente que cualquier arreglo puede ser descompuesto de manera única
// como una secuencia de escaleras. Se pide escribir un algoritmo para reposicionar las escaleras
// del arreglo original, de modo que las mismas se presenten en orden decreciente de longitud y,
// para las de la misma longitud, se presenten ordenadas en forma creciente por el primer valor
// de la escalera.

// El resultado debe ser del mismo tipo de datos que el arreglo original. Calculo complejidad 
// temporal de la solución propuesta, y justifique dicho cálculo.


Ejemplos de arreglo:

  1.i=1 ∧ j=n           2. 1<i ∧ j=n         3. i=1 ∧ j<n
   [1,2,3,4,5,6,7,8]    [0,1,3,4,5,6,7,8]    [1,2,3,4,5,6,8,10]                                               
    i             j          i         j      i         j    


/* Con arreglos: complejidad O(n²)
vector<vector<int>> agruparEscaleras(vector<int> v){
	
	int n = v.size();                    // O(1)     
	vector<int> escalera = {};           // O(1)       
	vector<vector<int>> escaleras = {};  // O(1)         

	for(int i=0; i<n-1; i++){              // O(n)
		if(v[i+1]=v[i]+1){
			// sigue escalera
			escalera.push_back(v[i]);      // O(|escalera|)  ⊆ O(n)
		} else {
			// se corta escalera
			escaleras.push_back(escalera); // O(|escaleras|) ⊆ O(n)
			escalera = {v[i]};             // O(1)
		}
	}

	if (v[n-1] == v[n-2]+1){
		escalera.push_back(v[n-1]);
	} else {
		escaleras.push_back(escalera);
		escalera = v[n-1];
	}
	escaleras.push_back(escalera);	
}
*/

list<list<int>> agruparEscaleras(vector<int> v){
	
	int n = v.size();                // O(1)     
	list<int> escalera = {};         // O(1)       
	list<list<int>> escaleras = {};  // O(1)         

	for(int i=0; i<n-1; i++){              // O(n-1) ⊆ O(n)
		if(v[i+1]=v[i]+1){
			// sigue escalera
			escalera.push_back(v[i]);      // θ(1)
		} else {
			// se corta escalera
			escaleras.push_back(escalera); // θ(1)
			escalera = {v[i]};             // θ(1)
		}
	}

	if (v[n-1] == v[n-2]+1){
		escalera.push_back(v[n-1]);    // θ(1)
	} else {
		escaleras.push_back(escalera); // θ(1)
		escalera = {v[n-1]};           // θ(1)
	}

	escaleras.push_back(escalera);	   // θ(1)

	return escaleras;                  // O(|escaleras| * |escalera|) ⊆ O(n)

}

vector<int> desagruparEscaleras(list<list<int>> ll, int n){
	vector<int> res(n);           // O(n)
	for(list<int> l in ll){       // O(|ll|*|l|) = O(n)
		for(int elem in l){         // O(|l|)
			res.push_back(elem);      // O(1)
		}
	}
	return res; // O(res) = O(n)
}

void EscaleraSort(vector<int> v){
	int n = v.size();
	list<list<int>> escalerasAgrupadas = agruparEscaleras(v); // O(n)
	HeapSort_ordenarSegúnLongitud(escalerasAgrupadas);        // O(n log(n))
	v = desagruparEscaleras(escalerasAgrupadas, n);           // O(n) 
}