Ejercicio 8 (Oficina estatal)

Considerar el siguiente TAD que modela el comportamiento de una oficina del Estado que procesa trámites.
Cada trámite está identificado por un ID y se le asigna una categorı́a al momento de ingresar. Las categorı́as de
la oficina no son fijas, y pueden agregarse nuevas categorı́as en cualquier momento. En cualquier momento se
puede dar prioridad a una categorı́a. Todos los trámites pendientes que pertenecen a una categorı́a prioritaria se
consideran prioritarios (Notar que en esta oficina, como buena dependencia estatal, un trámite nunca concluye):

--------------------------------------
TAD Oficina

géneros oficina

observadores básicos
	categorias   : oficina          → conj(categoria)   
	pendientes   : oficina          → secu(id)          
	prioritarias : oficina          → conj(categoria)   
	catTram      : id i × oficina o → categoria               {(i ∈ pendientes(o))}

generadores
	nuevo     :                                → oficina      
	nuevaCat  : categoria c × oficina o        → oficina     {(c ∉ categorias(o))}
	nuevoTram : id i × categoria c × oficina o → oficina     {(i ∉ pendientes(o) ∧ c ∈ categorias(o))}
	priorizar : categoria c × oficina o        → oficina     {(c ∈ categorias(o))}

otras operaciones
	pendPrioritarios     : oficina → secu(id)
	filtrarPorCategorias : secu(id) s × conj(categoria) × oficina o → secu(id)
		{((∀i : nat)(0 ≤ i < long(s) ⇒ L s[i] ∈ pendientes(o)))}

Fin TAD
--------------------------------------


Se decidió utilizar la siguiente estructura como representación:

--------------------------------------
oficina se representa con estr, donde
estr es tupla

 ⟨catPrioritarias  : conj(categoria),
  tramites         : dicc(id, categoria),
  tramites×Cat     : dicc(categoria, conj(id)),
  pendPrioritarios : secu(id),
  pendientes       : secu(id)⟩
 --------------------------------------

Informalmente:
· catPrioritarias representa el conjunto de todas las categorı́as a las que se ha dado prioridad
· tramites le asocia a cada trámite su categorı́a
· tramites×Cat describe todos los trámites asociados a cada categorı́a.
· pendPrioritarios contiene la secuencia de trámites pendientes que tienen una categorı́a prioritaria
· pendientes contiene todos los trámites pendientes (incluso a los prioritarios).

a) Escribir en castellano y formalmente el invariante de representación.
b) Escribir formalmente la función de abstracción.


***



a. Invariante en castellano:

1. en pendientes no hay elementos repetidos
2. si armo un conjunto con todos los elementos de pendientes, sería igual a las claves de trámites
3. todos los significados de tramites×Cat están contenidos en el conjunto de claves de tramites
4. cada trámite de trámites está en una única categoria de tramites×Cat
5. cada trámite de trámites está en tramites×Cat en la categoría que le corresponde según lo asignado en tramites
6. catPrioritarias es subconjunto de las claves de tramites×Cat
7. pendPrioritarios es igual a quitarle los trámites no prioritarios a pendientes



* Invariante formalmente:

Rep(e) : estr → booleano

∀ (e : estr)
Rep(e) ≡ 1 ∧ 2 ∧ 3 ∧ 4 ∧ 5

1 ≡ (∀ i,j : nat)(0 ≤ i < j < long(e.pendientes) ⇒L e.pendientes[i] ≠ e.pendientes[j])
2 ≡ claves(tramites) = secu_a_conj(pendientes)
3 ≡ (∀ c : categoria)(c ∈ claves(tramites×Cat) ⇒L obtener(c, tramites×Cat) ⊆ claves(tramites))
4 ≡ (∀ i : id)(i ∈ claves(tramites) ⇒L (∃! c : categoria)(c ∈ claves(tramites×Cat) ∧L i ∈ obtener(c, tramites×Cat))
5 ≡ (∀ i : id)(i ∈ claves(tramites) ⇒L (∃  c : categoria)(c ∈ claves(tramites×Cat) ∧L tramites[i] = c)))
6 ≡ catPrioritarias ⊆ claves(tramites×Cat)
7 ≡ pendPrioritarios = filtrarNoPrioritarios(pendientes)


Funciones auxiliares:

	//extensión de secuencia
	• [ • ] : secu(α) s × nat n → α                    {0 ≤ n < long(s)}
	s[n] ≡ if n = 0 then prim(s) else fin(s)[n-1] fi

	secu_a_conj : secu(α) → conj(α)
	secu_a_conj(s) ≡ if vacía?(s) then ø else {prim(s)} ∪ secu_a_conj(fin(s)) fi

	filtrarNoPrioritarios : dicc(id, categoria) × conj(categoria) × secu(id) → secu(id)
	filtrarNoPrioritarios(d, c, s) ≡ if vacía?(s)
	                                    then ⟨⟩
	                                    else
	                                        if esPrioritario?(d,c,pri(s)) then ⟨pri(s)⟩ else ⟨⟩ fi
	                                        & filtrarNoPrioritarios(d, c, fin(s))
	                                    fi

	esPrioritario?(dicc, catPri, i) ≡ dicc[i] ∈ catPri



b. Función de abstracción:

Abs(e) : estr → oficina estatal {Rep(e)}

∀ (e : estr)
Abs(e) =obs a |

   categorias(a) = claves(e.tramites×Cat)
    ∧ pendientes(a) = e.pendientes
    ∧ prioritarias(a) = e.pendPrioritarios
    ∧ (∀ i : id)(está?(i, pendientes(a)) ⇒L catTram(i, a) = tramites[i])