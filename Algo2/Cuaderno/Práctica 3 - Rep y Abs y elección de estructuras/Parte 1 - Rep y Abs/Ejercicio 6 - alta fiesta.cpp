// c) Escribir una versión imperativa de la función llegaGrupo marcando claramente los puntos de su
// programa en que alguna parte del invariante de representación se rompe, indicando a qué parte
// se refiere, y también aquellos puntos donde éste se reestablece.

// grupo = string
// persona = string
// regalo = string

// Voy a suponer que tengo éstas variables privadas
// _invitados : set<persona>
// _clavesDeGrupos : set<grupo>
// _grupos : map<grupo, set<persona>>
// _regalos : map<grupo, regalo>
// _presentes : set<persona>
// _grupoMasNumeroso : grupo

void llegaGrupo(fiesta & f, grupo g, set<persona> c, regalo r){

	 if (!pertenece(g, _claveDeGrupos && r != "" && pertenece(c, _invitados))){

	 	_claveDeGrupos.insert(g);

	 	//se rompe el invariante: _claveDeGrupos ya no corresponde a las claves de _grupos

	 	_grupos[g] = c;

	 	//se arregla el invariante: _claveDeGrupos corresponde a las claves de _grupos
	 	//se rompe el invariante: las claves de _grupos no son las mismas que las claves de _regalos
	 	//se rompe el invariante: _presentes todavía no contiene a las nuevas personas
	 	//se rompe el invariante: no tengo certeza de que _grupoMasNumeroso sea el más numeroso

	 	_regalos[g] = r;

	 	//se arregla el invariante: las claves de _grupos son las misas que las claves de _regalos

	 	agregar(_presentes, c)

	 	//se arregla el invariante: _presentes ahora contiene a las nuevas personas

	 	_grupoMasNumeroso = buscoMasNumeroso(_grupos)

	 	//se arregla el invariante: _grupoMasNumeroso es el grupo más numeroso
	 }
}