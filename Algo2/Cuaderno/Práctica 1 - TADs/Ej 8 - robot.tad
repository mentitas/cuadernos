// Ejercicio 8 (Robot)
// Especifique tipos para un robot que realiza un camino a través de un plano de coordenadas cartesianas
// (enteras), es decir, tiene operaciones para ubicarse en un coordenada, avanzar hacia arriba, hacia abajo, hacia
// la derecha y hacia la izquierda, preguntar por la posición actual, saber cuántas veces pasó por una coordenada
// dada y saber cuál es la coordenada más a la derecha por dónde pasó.
// Completar la especificación a partir de la siguiente signatura:

TAD coordenada es tupla(entero × entero)

TAD Robot

    géneros: robot

    exporta: robot, observadores básicos, generadores

    usa: bool, nat, secuencia, coordenada

    igualdad observacional:
        (∀r₁, r₂ : robot)(r₁ =obs r₂ ⇔ (trayectoria(r₁) =obs trayectoria(r₂)))

    generadores:
        Ubicar     : coordenada → robot //un robot se crea con ésto. Todo robot tiene al menos una coordenada en su trayectoria (dónde nació)
        Arriba     : robot → robot
        Abajo      : robot → robot
        Derecha    : robot → robot  
        Izquierda  : robot → robot 

    observadores básicos:
        trayectoria : robot → secuencia(coordenada)

    otras operaciones:
        PosiciónActual   : robot → coordenada
        CuantasVecesPasó : coordenada × robot → nat
        MásALaDerecha    : robot → coordenada}
        maxDer           : coordenada × coordenada → coordenada

    axiomas: ∀ r,s : robot, ∀ c,d : coordenada
        trayectoria(Ubicar(r, c)) ≡ c • ⟨⟩
        trayectoria(Arriba(r))    ≡ ⟨π₁(c), π₂(c)+1⟩ • trayectoria(r)
        trayectoria(Abajo(r))     ≡ ⟨π₁(c), π₂(c)-1⟩ • trayectoria(r)
        trayectoria(Derecha(r))   ≡ ⟨π₁(c)+1, π₂(c)⟩ • trayectoria(r)
        trayectoria(Izquierda(r)) ≡ ⟨π₁(c)-1, π₂(c)⟩ • trayectoria(r)

        PosiciónActual(Ubicar(r, c)) ≡ c
        PosiciónActual(Arriba(r))    ≡ ⟨π₁(PosiciónActual(r)),   π₂(PosiciónActual(r))+1⟩
        PosiciónActual(Abajo(r))     ≡ ⟨π₁(PosiciónActual(r)),   π₂(PosiciónActual(r))-1⟩
        PosiciónActual(Derecha(r))   ≡ ⟨π₁(PosiciónActual(r))+1, π₂(PosiciónActual(r))⟩
        PosiciónActual(Izquierda(r)) ≡ ⟨π₁(PosiciónActual(r))-1, π₂(PosiciónActual(r))⟩

        CuantasVecesPasó(c, Ubicar(r,d))  ≡ if c = d then 1 else 0 fi
        CuantasVecesPasó(c, Arriba(r))    ≡ (if c = PosiciónActual(Arriba(r))    then 1 else 0 fi) + CuantasVecesPasó(c, r)
        CuantasVecesPasó(c, Abajo(r))     ≡ (if c = PosiciónActual(Abajo(r))     then 1 else 0 fi) + CuantasVecesPasó(c, r)
        CuantasVecesPasó(c, Derecha(r))   ≡ (if c = PosiciónActual(Derecha(r))   then 1 else 0 fi) + CuantasVecesPasó(c, r)
        CuantasVecesPasó(c, Izquierda(r)) ≡ (if c = PosiciónActual(Izquierda(r)) then 1 else 0 fi) + CuantasVecesPasó(c, r)

        MásALaDerecha(Ubicar(r, c)) ≡ c
        MásALaDerecha(Derecha(r))   ≡ maxDer(PosiciónActual(r), MásALaDerecha(r))
        MásALaDerecha(Izquierda(r)) ≡ MásALaDerecha(r)
        MásALaDerecha(Arriba(r))    ≡ MásALaDerecha(r)
        MásALaDerecha(Abajo(r))     ≡ MásALaDerecha(r)

        maxDer(c, d) ≡ if π₁(c) ≥ π₁(d) then c else d fi 
 
Fin TAD

// versiones chotas:

// ***Extiendo TAD secuencia***
// apariciones : secu(α) × α → nat
// apariciones(s, a) ≡ if vacio?(s) then 0 else (if prim(s)=a then 1 else 0 fi) + apariciones(fin(s), a) fi

// PosiciónActual(r)         ≡ prim(trayectoria(r))
// CuantasVecesPasó(c, r)    ≡ apariciones(trayectoria(r), c)