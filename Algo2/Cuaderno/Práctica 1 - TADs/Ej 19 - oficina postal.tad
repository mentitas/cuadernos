// Ejercicio 19 (La oficina postal)

// Se quiere especificar un sistema para controlar una oficina postal que se ocupa de almacenar, administrar
// y entregar la correspondencia en una ciudad. La ciudad en cuestión se encuentra dividida en zonas, cada una
// de las cuales se identifica con un código postal. La oficina recibe periódicamente un cargamento de paquetes
// (cartas, encomiendas, documentos, etc.). Cada paquete tiene escrito el código postal de su destinatario y un
// peso en gramos.
// La oficina cuenta con un grupo de empleados destinados al reparto de paquetes (los carteros). A cada cartero
// le corresponde entregar los paquetes dentro de una única zona. Es decir, cada cartero tiene asociado un único
// código postal.
// Cuando llega un camión con un cargamento, los empleados de la oficina organizan los paquetes recibidos.
// Los paquetes cuyo código postal no se corresponde con el de algún cartero se devuelven inmediatamente al
// camión. Los paquetes restantes ingresan al depósito del correo.
// Un cartero puede iniciar su recorrido en cualquier momento, llevándose del depósito una bolsa de paquetes
// para repartir. Los paquetes a repartir se asignan de la manera detallada a continuación, teniendo en cuenta que
// el método utilizado para seleccionarlos se postergará hasta el momento de la implementación:
// · A cada cartero le corresponden únicamente paquetes asociados a su código postal.
// · El peso total de la bolsa no debe exceder los 25Kg.
// Cuando el cartero finaliza su recorrido, reporta cuáles paquetes no pudieron ser entregados. Los paquetes
// rebotados se reingresan al depósito. Este conjunto debe ser (obviamente) un subconjunto de los paquetes que le
// correspondı́a entregar al momento de iniciar su recorrido, en caso de que sea el mismo conjunto, el cartero es
// despedido de la oficina postal.

// Especificar el sistema utilizando TADs. Se desea saber en todo momento:
// · Cuántos paquetes hay en el depósito, y qué caracterı́sticas tienen.
// · Cuántos paquetes fueron rebotados.

> La ciudad está dividida en zonas, identificadas con un código postal
> La oficina recibe cargamentos con paquetes
> Cada paquete tiene código postal y peso en gramos
> A cada cartero le corresponde un único código postal
> Cuando llega un camión con cargamento, los paquetes se organizan y se devuelven aquellos que no son de ningún cartero
> Los carteros pueden salir en cualquier momento con una bolsa de paquetes que sacan del deṕosito. 
> La bolsa de paquetes solo tiene paquetes de su código postal
> La bolsa pesa menos que 25kg
> El cartero puede volver con paquetes rebotados, éstos reingresan al depósito
> Si vuelve con los mismos paquetes con los que salió, es despedido

> Qué paquetes hay en el depósito
> Cantidad de paquetes rebotados

//Voy a identificar a los carteros según su código

TAD código es nat
TAD paquete es tupla(código, nat)

TAD OficinaPostal

    géneros: oficina

    exporta: oficina, observadores básicos, generadores

    usa: bool, nat, tupla, conjunto, código, paquete

    igualdad observacional:
    	(∀o₁, o₂ : oficina)(o₁ =obs o₂ ⇔ (ciudad(o₁) =obs ciudad(o₂)
    		                              ∧ depósito(o₁) =obs depósito(o₂)
    		                              ∧ carteros(o₁) =obs carteros(o₂)
    		                              ∧ #rebotados(o₁) =obs #rebotados(o₂)))

    observadores básicos:
        ciudad     : oficina → conj(código)
        depósito   : oficina → conj(paquete)
        carteros   : oficina → conj(código)
        #rebotados : oficina → nat

    generadores:
        abroOficina      : conj(código) × conj(código) → oficina
        reciboCargamento : oficina × conj(paquete) → oficina
        saleCartero      : oficina o × código c × conj(paquete) cp → oficina 
                                                    {c ∈ carteros(o) ∧ todosDeLaZona(cp, c) ∧ pesoTotal(cp)<25000}

        vuelveCartero    : oficina o × código c × conj(paquete) cp × conj(paquete) cr → oficina
                                                    {c ∈ carteros(o) ∧ todosDeLaZona(cp, c) ∧ cr ⊆ cp}
                                                    // fijarse que cp sea el paquete actual del cartero

    otras operaciones:
        todosDeLaZona : conj(paquete) × código → bool
        pesoTotal     : conj(paquete) → nat
        devolverPaquetesInválidos : conj(código) × conj(paquete) → conj(paquete)

    axiomas: ∀ o:oficina, ∀ ciu, car:conj(código), ∀ cp,cr:conj(paquete), ∀ c:código, ∀ p:paquete, 
    	ciudad(abroOficina(ciu, car))    ≡ ciu
    	ciudad(reciboCargamento(o,cp))   ≡ ciudad(o)
    	ciudad(saleCartero(o,c,cp))      ≡ ciudad(o)
    	ciudad(vuelveCartero(o,c,cp,cr)) ≡ ciudad(o)

    	depósito(abroOficina(ciu,car))     ≡ ø
    	depósito(reciboCargamento(o,cp))   ≡ devolverPaquetesInválidos(carteros(o), cp)
    	depósito(saleCartero(o,c,cp))      ≡ depósito(o) - cp 
    	depósito(vuelveCartero(o,c,cp,cr)) ≡ depósito(o) ∪ cr 

    	devolverPaquetesInválidos(car, cp) ≡ if vacío?(cp)
    	                                        then ø
    	                                        else 
    	                                            (if π₁(dameUno(cp)) ∈ car
    	                                               then {dameUno(cp)}
    	                                               else ø
    	                                            fi) ∪ devolverPaquetesInválidos(car, sinUno(cp))
    	                                     fi 

    	carteros(abroOficina(ciu, car))    ≡ car
    	carteros(reciboCargamento(o,cp))   ≡ carteros(o)
    	carteros(saleCartero(o,c,cp))      ≡ carteros(o)
    	carteros(vuelveCartero(o,c,cp,cr)) ≡ if #cp = #cr then carteros(o)-c else carteros(o) fi

    	#rebotados(abroOficina(ciu, car))    ≡ 0
    	#rebotados(reciboCargamento(o,cp))   ≡ rebotados(o)
    	#rebotados(saleCartero(o,c,cp))      ≡ rebotados(o)
    	#rebotados(vuelveCartero(o,c,cp,cr)) ≡ rebotados(o) + #cr

    	todosDeLaZona(cp, c) ≡ if vacío?(cp)
    	                          then true
    	                          else π₁(dameUno(cp))=c ∧ todosDeLaZona(sinUno(cp),c)
    	                       fi

    	pesoTotal(cp) ≡ if vacío?(cp) then 0 else (π₂(dameUno(cp)) + pesoTotal(sinUno(cp))) fi

Fin TAD