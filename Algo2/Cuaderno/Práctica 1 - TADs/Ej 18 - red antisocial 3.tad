TAD persona es string

TAD secta

    géneros: secta

    exporta: secta, observadores básicos, generadores

    usa: bool, nat, conjunto

    igualdad observacional:
    	(∀s₁, s₂ : secta)(s₁ =obs s₂ ⇔ (líder(s₁) =obs líder(s₂)
                          ∧ expulsados(s₁) =obs expulsados(s₂)
    		              ∧ (∀ p : persona)(esMiembro?(s₁, p) =obs esMiembro?(s₂, p)
    		              ∧L (esMiembro?(s₁, p) ∧ p ≠ líder(s₁)) →L reclutador(s₁, p) =obs reclutador(s₂, p))))

    observadores básicos:
        líder      : secta → persona
        esMiembro? : secta × persona → bool      
        reclutador : secta s × persona p → persona                     {esMiembro?(s, p) ∧ p ≠ líder(s)}
        expulsados : secta → conj(personas)

    generadores: //absolver no es necesario que sea generador, pero simplifica muchas cosas
        nuevaSecta : persona → secta
        reclutar   : secta s × persona p × persona q → secta           {esMiembro?(s, p) ∧ ¬esMiembro?(s, q) ∧ ¬(q ∈ expulsados(s))}
        enjuiciar  : secta s × persona p × conj(persona) → secta       {esMiembro?(s, p)}
        absolver   : secta s × persona p → secta                       {p ∈ expulsados(s)}

    otras operaciones:
        dameMiembro  : secta → persona  ***???
        sinMiembro   : secta → secta    ***???
        sonMiembros? : secta × conj(persona) → bool
        esReclutaDe? : secta s × persona p ×  persona q → bool          {esMiembro?(s, p) ∧ esMiembro?(s, q)}
        #reclutasDe  : secta s × persona p → nat                        {esMiembro?(s, p)}
        ganaJuicio?  : secta s × persona p × conj(persona) t → bool     {esMiembro?(s, p) ∧ sonMiembros?(s, t)}
        todesLesReclutasDe : secta s × persona p → conj(persona)        {esMiembro?(s, p)} ***TERMINAR


    axiomas: ∀ s:secta, ∀ t:conj(persona), ∀ p,q,r:persona, 
        líder(nuevaSecta(p))   ≡ p
        líder(reclutar(s,p,q)) ≡ líder(s)
        líder(enjuiciar(s,p))  ≡ líder(s)
        líder(absolver(s,p))   ≡ líder(s)

        esMiembro?(nuevaSecta(p), q)    ≡ p=q
        esMiembro?(reclutar(s,p,r), q)  ≡ q=r ∨L esMiembro?(s, q)
        esMiembro?(enjuiciar(s,p,t), q) ≡ if p=q then ganaJuicio?(s,p,t) else esMiembro?(s, q) fi
        esMiembro?(absolver(s,p), q)    ≡ esMiembro?(s, q)

        reclutador(reclutar(s,p,q), r)  ≡ if q=r then p else reclutador(s,r) fi
        reclutador(enjuiciar(s,p,t), r) ≡ reclutador(s, r)

        expulsados(nuevaSecta(p))    ≡ ø
        expulsados(reclutar(s,p,q))  ≡ expulsados(s)
        expulsados(enjuiciar(s,p,t)) ≡ (if ganaJuicio?(s,p,t) then ø else todesLesReclutasDe(s,p) fi) ∪ expulsados(s)
        expulsados(absolver(s,p))    ≡ expulsados(s) - {p}

        //¿r es recluta de p?
        esReclutaDe(s,p,r) ≡ if r = líder(s)
                                then false
                                else reclutador(s, r) = p ∨L esReclutaDe(s,p,reclutador(s, r))
                             fi

        sonMiembros?(s,t) ≡ vacío?(t) ∨L (esMiembro?(s, dameUno(t)) ∧ sonMiembros?(s,sinUno(t)))

        #reclutasDe(s,p,t) ≡ if vacío?(t)
                                 then 0
                                 else β(esReclutaDe(s,p,dameUno(t))) + #reclutasDe(s,p,sinUno(t))
                             fi

        ganaJuicio(s,p,t) ≡ #reclutasDe(s,p,t)≥2

        esMiembro?(s, dameMiembro(s))             ≡ true    //???
        esMiembro?(sinMiembro(s), dameMiembro(s)) ≡ false   //??? 

        todesLesReclutasDe(s,p) ≡ if ***fin recursión*** 
                                    then {p}
                                    else 
                                        (if esReclutaDe(s,p,dameMiembro(s))
                                            then {dameMiembro}
                                            else ø
                                         fi) ∪ todesLesReclutasDe(sinMiembro(s), p)
                                  fi
                                  
Fin TAD