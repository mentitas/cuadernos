// Ejercicio 7 (polinomios)

// Se quieren especificar los polinomios con coeficientes naturales, sabiendo que:

// * Un número natural es un polinomio.
// * La indeterminada X es un polinomio.
// * Si p1 y p2 son dos polinomios, entonces la suma p1 + p2 y el producto p1 · p2 son polinomios.

// Se desea tener en el tipo una operación de evaluación, que dado un polinomio y un número natural lo evalúe de
// la manera habitual y otra que indique si un número natural es raı́z de un polinomio. Completar la especificación
// a partir de la signatura dada.

TAD Polinomio

    géneros: polinomio

    exporta: polinomio, observadores básicos, generadores

    usa: bool, nat

    igualdad observacional:
    	(∀p₁, p₂ : polinomio)(p₁ =obs p₂ ⇔ ((∀n : nat)(Evaluar(p₁, n) =obs Evaluar(p₂, n))))

    generadores:
        Cte   : nat → polinomio
        X     :     → polinomio
        • + • : polinomio + polinomio → polinomio
        • . • : polinomio . polinomio → polinomio

    observadores básicos:
        Evaluar : polinomio × nat → nat

    otras operaciones:
        esRaiz?      : polinomio × nat → bool
        esConstante? : polinomio       → bool

    axiomas: ∀ p,q : polinomio, ∀ n, m : nat
        Evaluar(X, n)       ≡ n
        Evaluar(Cte(m), n)  ≡ m
        Evaluar(p + q, n)   ≡ Evaluar(p, n) + Evaluar(q, n)
        Evaluar(p . q, n)   ≡ Evaluar(p, n) . Evaluar(q, n)
        esConstante(X)      ≡ false
        esConstante(Cte(n)) ≡ true
        esConstante(p + q)  ≡ esConstante(p) ∧ esConstante(q)
        esConstante(p . q)  ≡ esConstante(p) ∧ esConstante(q)
        esRaiz?(p, n)       ≡ ¬esConstante?(p) ∧ (Evaluar(p, n) = 0)

Fin TAD

//cómo se si un pol es constante?
// (∀ n:nat)(Evaluar(p, n) = p)
// esConstante(p)     ≡ p ∈ nat //¿está bien?