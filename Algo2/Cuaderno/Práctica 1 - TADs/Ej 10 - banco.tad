// Ejercicio 10 (Para hacer en la fila del banco)
// Con el objetivo de mejorar su servicio, un banco decidió analizar el comportamiento de sus largas filas de
// clientes. Se encargó la tarea al señor Felipe N. Sativo, empleado de la casa central del banco en cuestión, quien
// inmediatamente reconoció la necesidad de pedir nuestro asesoramiento.
// Proponer una solución para cada ı́tem del ejercicio, discutirlo con sus compañeros y luego consultarlo con
// un docente.

// a) El señor Sativo nos presentó un informe en el que detallaba el comportamiento esperado de una fila de clientes.
// Después de leerlo, releerlo y extraer de allı́ la información importante, comprendimos que las acciones que
// esperaba registrar eran la apertura de la ventanilla, la llegada de un nuevo cliente a la fila, y la atención
// del cliente que estuviera en primer lugar (con su consecuente egreso de la fila). También quedó claro que
// el banco deseaba poder verificar si la fila estaba vacı́a, conocer la longitud (cantidad de clientes) de la fila,
// si un cliente determinado estaba o no esperando su atención en dicha fila y, en caso de estarlo, cuál era su
// posición en ella (siendo la posición 1 la del próximo cliente que serı́a atendido, es decir, el que haya llegado
// primero entre los clientes presentes).
// Especificar (distinguiendo generadores, observadores básicos y otras operaciones, y escribiendo los axiomas)
// el tipo FILA, cuyas funciones a exportar son las siguientes:

TAD persona es string
TAD Fila es cola(persona)

TAD Fila

    géneros: fila

    exporta: algo, observadores básicos, generadores

    usa: bool, nat

    igualdad observacional:
        (∀f₁, f₂ : fila)(f₁ =obs f₂ ⇔ (Vacía(f₁) ⟺ Vacía(f₂))
                                ∧L ¬Vacía(f₁) ⇒L (∀ p : persona)((Esperando(p, f₁) ⟺ Esperando(p, f₂)) 
                                ∧ (Esperando(p, f₁) ⇒L Posición(p, f₁) = Posición(p, f₂))))

    generadores:
        AbrirVentanilla : → fila
        Llegar          : persona p × fila f → fila     {¬Esperando(p, f)}

    observadores básicos:
        Vacía     : fila → bool
        Esperando : persona × fila → bool
        Posición  : persona p × fila f → nat            {Esperando(p, f)}

    otras operaciones:
        Atender   : fila f → fila                       {¬Vacía(f)}
        Longitud  : fila → nat

    axiomas:
        Vacía(AbrirVentanilla) ≡ true
        Vacía(Llegar(p, f))    ≡ false

        Esperando(p, AbrirVentanilla) ≡ false
        Esperando(p, Llegar(q, f))    ≡ if p=q then true else Esperando(p, f) fi

        Posición(p, Llegar(q, f)) ≡ if p=q then Longitud(f) else Posición(p, f) fi

        Longitud(f) ≡ if Vacía(f) then 0 else 1 + Longitud(Atender(f)) fi

        Atender(f) ≡ desencolar(f)

Fin TAD


// b) Durante el primer dı́a de observación, Felipe N. Sativo descubrió que la realidad de las filas del banco era
// más compleja de lo que él habı́a previsto. Notó que algunos clientes, desalentados por la longitud y lentitud
// de la fila, se retiraban sin haber sido atendidos. También detectó algunos clientes que no respetaban el orden
// de la fila, introduciéndose en ella delante de otros clientes que habı́an llegado antes (en términos más simples:
// colándose). Felipe decidió que era importante registrar estos sucesos, ası́ como también conocer la identidad
// de los irrespetuosos alteradores del orden.
// Modificar la especificación anterior para incluir las siguientes funciones:

TAD persona es string
TAD fila es cola(persona)

TAD Fila

    géneros: fila

    exporta: fila, observadores básicos, generadores

    usa: bool, nat, persona, cola

    igualdad observacional:
        (∀f₁, f₂ : fila)(f₁ =obs f₂ ⇔ (Vacía(f₁) ⟺ Vacía(f₂))
                                ∧L ¬Vacía(f₁) ⇒L (∀ p : persona)((Esperando(p, f₁) ⟺ Esperando(p, f₂)) 
                                ∧ (Esperando(p, f₁) ⇒L (Posición(p, f₁) = Posición(p, f₂) ∧ (SeColó?(p, f₁) ⟺ SeColó?(p, f₂))))))

    generadores:
        AbrirVentanilla   : → fila
        Llegar            : persona p × fila f → fila                {¬Esperando(p, f)}
        ColarseAdelanteDe : persona p × persona q × fila f → fila    {¬Esperando(p, f) ∧ Esperando(q, f)}
        Retirarse         : persona p × fila f → fila                {Esperando(p, f)}

    observadores básicos:
        Vacía     : fila → bool
        Esperando : persona × fila → bool
        Posición  : persona p × fila f → nat                         {Esperando(p, f)}
        SeColó?   : persona p × fila f → bool                        {Esperando(p, f)}

    otras operaciones:
        Atender   : fila f → fila                                    {¬Vacía(f)}
        Longitud  : fila → nat

    axiomas:
        Vacía(AbrirVentanilla) ≡ true
        Vacía(Llegar(p, f))    ≡ false
        Vacía(Retirarse(p, f)) ≡ Vacía(Atender(f))

        Esperando(p, AbrirVentanilla)            ≡ false
        Esperando(p, Llegar(q, f))               ≡ if p=q then true else  Esperando(p, f) fi
        Esperando(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then true else  Esperando(p, f) fi
        Esperando(p, Retirarse(q, f))            ≡ if p=q then false else Esperando(p, f) fi

        Posición(p, Llegar(q, f))               ≡ if p=q then Longitud(f) else Posición(p, f) fi
        Posición(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then Posición(r) else (Posición(p, f) + β(Posición(p, f)>Posición(r, f))) fi //if Posición(p, f)<Posición(r, f) then Posición(p, f) else Posición(p, f) + 1 fi
        Posición(p, Retirarse(q, f))            ≡ Posición(p) + β(Posición(q)<Posición(p)) //p≠q

        SeColó?(p, Llegar(q, f))               ≡ if p=q then false else SeColó?(q, f) fi
        SeColó?(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then true  else SeColó?(p, f) fi
        SeColó?(p, Retirarse(q, f))            ≡ SeColó?(p, f)

        Longitud(f) ≡ if Vacía(f) then 0 else 1 + Longitud(Atender(f)) fi

        Atender(f) ≡ desencolar(f)

Fin TAD

//Si para axiomatizar Retirarse armo una función que aplica Llegar un montón de veces, 
//luego de usarla, sigue siendo la misma fila? se van a seguir teniendo en cuenta las personas que se colaron?
// no. Tengo que agregarla como generador



// c) El banco felicitó al Sr. Sativo por su gran desempeño, y le entregó un último encargo, necesario para com-
// probar la verdadera eficiencia del banco con respecto a la atención de sus clientes. El encargo consistı́a en
// averiguar si un cliente dado ingresó alguna vez a la fila (ya se encuentre esperando en ella, haya sido atendido
// o se haya retirado), y si una persona determinada habı́a sido atendida durante el dı́a (nótese que el tipo
// FILA sólo registra la información de un dı́a, desde el momento en que se abre la ventanilla).
// Modificar nuevamente la especificación para agregar las funciones faltantes:
//  Entro? : persona × fila → bool
//  FueAtendido? : persona × fila → bool
// Notar que la nueva especificación puede no cumplir los puntos anteriores.

TAD persona es string
TAD fila es cola(persona)

TAD Fila

    géneros: fila

    exporta: fila, observadores básicos, generadores

    usa: bool, nat, persona, cola

    igualdad observacional:
        (∀f₁, f₂ : fila)(f₁ =obs f₂ ⇔ (Vacía(f₁) ⟺ Vacía(f₂))
                                ∧L ¬Vacía(f₁) ⇒L (∀ p : persona)((Esperando(p, f₁) ⟺ Esperando(p, f₂))
                                ∧ (Entró?(p, f₁) ⟺ Entró?(p, f₂))
                                ∧ (SeRetiró?(p, f₁) ⟺ SeRetiró?(p, f₂)) 
                                ∧ (Esperando(p, f₁) ⇒L (Posición(p, f₁) = Posición(p, f₂) ∧ (SeColó?(p, f₁) ⟺ SeColó?(p, f₂))))))

    generadores:
        AbrirVentanilla   : → fila
        Llegar            : persona p × fila f → fila                   {¬Esperando(p, f)}
        ColarseAdelanteDe : persona p × persona q × fila f → fila       {¬Esperando(p, f) ∧ Esperando(q, f)}
        Retirarse         : persona p × fila f → fila                   {Esperando(p, f)}

    observadores básicos:
        Vacía        : fila → bool
        Esperando    : persona × fila → bool
        Posición     : persona p × fila f → nat                         {Esperando(p, f)}
        SeColó?      : persona p × fila f → bool                        {Esperando(p, f)}
        Entro?       : persona × fila → bool
        SeRetiró?    : persona × fila → bool

    otras operaciones:
        Atender   : fila f → fila                                       {¬Vacía(f)}
        Longitud  : fila → nat
        FueAtendido? : persona × fila → bool

    axiomas:
        Vacía(AbrirVentanilla) ≡ true
        Vacía(Llegar(p, f))    ≡ false
        Vacía(Retirarse(p, f)) ≡ Vacía(Atender(f))

        Esperando(p, AbrirVentanilla)            ≡ false
        Esperando(p, Llegar(q, f))               ≡ if p=q then true else  Esperando(p, f) fi
        Esperando(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then true else  Esperando(p, f) fi
        Esperando(p, Retirarse(q, f))            ≡ if p=q then false else Esperando(p, f) fi

        Posición(p, Llegar(q, f))               ≡ if p=q then Longitud(f) else Posición(p, f) fi
        Posición(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then Posición(r) else (Posición(p, f) + β(Posición(p, f)>Posición(r, f))) fi //if Posición(p, f)<Posición(r, f) then Posición(p, f) else Posición(p, f) + 1 fi
        Posición(p, Retirarse(q, f))            ≡ Posición(p) + β(Posición(q)<Posición(p)) //p≠q

        SeColó?(p, Llegar(q, f))               ≡ if p=q then false else SeColó?(q, f) fi
        SeColó?(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then true  else SeColó?(p, f) fi
        SeColó?(p, Retirarse(q, f))            ≡ SeColó?(p, f)

        Entró?(p, AbrirVentanilla)            ≡ false
        Entró?(p, Llegar(q, f))               ≡ if p=q then true else Entró?(p, f) fi
        Entró?(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then true else Entró?(p, f) fi 
        Entró?(p, Retirarse(q, f))            ≡ Entró(p, f)

        SeRetiró?(p, AbrirVentanilla)            ≡ false
        SeRetiró?(p, Llegar(q, f))               ≡ if p=q then false else SeRetiró?(p, f) fi
        SeRetiró?(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then false else SeRetiró?(p, f) fi
        SeRetiró?(p, Retirarse(q, f))            ≡ if p=q then true  else SeRetiró?(p, f) fi

        FueAtendido?(p, f) ≡ Entró?(p, f) ∧ ¬Esperando(p, f) ∧ ¬SeRetiró?(p, f)

        Longitud(f) ≡ if Vacía(f) then 0 else 1 + Longitud(Atender(f)) fi

        Atender(f) ≡ desencolar(f)

Fin TAD
