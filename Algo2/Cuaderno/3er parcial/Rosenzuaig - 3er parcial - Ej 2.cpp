// Ej. 2. Divide and Conquer
// Como parte de un proyecto de generación automática de contenido audiovisual para Internet, se desea que, a partir
// de una lista dada, combinar aquellos videos consecutivos que traten sobre un mismo tema, para obtener pelı́culas
// de la mayor duración posible.

// Al momento se cuenta con dos operaciones ya realizadas, las cuales que no se pueden modificar:

// > MismoTema(in v : video, in v' : video) → res : bool, que nos dice si dos videos v y v' tratan sobre el mismo
// tema. Complejidad temporal: θ(1).
// > Combinar(in v : video, in v' : video) → res : video, que combina dos videos v y v'. Complejidad temporal:
// Θ(d(v) + d(v')), siendo d(x) la duración del video x.

// Dar un algoritmo que utilice la técnica Divide and Conquer para calcular la función

// Peliculas(in V : arreglo(video)) → res : arreglo(video)

// Por ejemplo, siendo los videos v1 , v3 y v4 del mismo tema, Peliculas([v1,v2,v3,v4]) → [v1,v2,v3−4]

// Se pide que el algoritmo tenga complejidad temporal O(n + (D log n)), donde D = ∑^n_i=1 d(V[i]) (es decir, es
// la suma de las duraciones de todos los videos). Justificar detalladamente que el algoritmo efectivamente resuelve
// el problema y demostrar formalmente que cumple con la complejidad solicitada. Para esto último, se deberá utilizar
// alguno de los métodos vistos de acuerdo a las caracterı́sticas del problema que se está resolviendo.


// Voy a resolverlo en c++. Voy a representar los arreglos como vectores

vector<video> Peliculas(vector<video> V){
	return Peliculas_aux(V, 0, V.size());
}


vector<video> Peliculas_aux(vector<video> V, int l, int r){ // Rango [l,r)
	int n = r-l;

	if (n==1){
		return V;
	}

	int m = (l+r)/2;

	vector<video> recursiónDer = Peliculas_aux(V,l,m); // T(n/2)
	vector<video> recursiónIzq = Peliculas_aux(V,m,r); // T(n/2)

	return ConcatenarConCombinación(recursiónDer, recursiónIzq); //O(n + d(V[m]) + d(V[m+1]))

}

// Complejidad total: O(|A| + |B| + d(A[a-1]) + d[B[0]])
vector<video> ConcatenarConCombinación(vector<video> A, vector<video> B){
	int a = A.size();
	int b = B.size();

	

	if (MismoTema(A[a-1], B[0])){                       // θ(1)

		Video c = Combinar(A[a-1], B[0]);               // Θ(d(A[a]) + d(B[0]))
		
		vector<video> res(a+b-1, videoDefault);

		for(int i=0; i<a-1; i++) res[i] = A[i];
		
		res[a-1] = c;
		
		for(int j=1; j<b; j++) res[a+j] = B[j];

	} else {

		vector<video> res(a+b, videoDefault);

		for(int i=0; i<a; i++) res[i]   = A[i];
		for(int j=0; j<b; j++) res[a+j] = B[j];

	}

	return res
}




> Cálculo de complejidad:

Voy a usar un árbol de recursión:

// T(n) = T(n/2) + O(n + d(m) + d(m+1))



// Complejidad total del algoritmo: O(n*log(n) + D*log(n))

// Demostración: