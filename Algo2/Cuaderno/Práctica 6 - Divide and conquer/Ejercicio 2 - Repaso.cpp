// Ejercicio 2 ★
// Tenemos un arreglo a = [a1 , a2 , . . . , an] de n enteros distintos (positivos y negativos)
// en orden estrictamente creciente. Queremos determinar si existe una posición i tal que aᵢ = i.
// Por ejemplo, dado el arreglo a = [−4, −1, 2, 4, 7], i = 4 es esa posición.
// Diseñar un algoritmo dividir y conquistar eficiente (de complejidad de orden estrictamente menor
// que lineal) que resuelva el problema. Calcule y justifique la complejidad del algoritmo dado.


bool existeIndice(array<int> A){
	return existeIndice_aux(A,0,A.size());
}

bool existeIndice_aux(array<int> A, int l, int r){
	int n = r-l;                              θ(1)
 
	if (n==1){                                θ(1)
		return l+1 == A[l];                   θ(1) 
	}

	int m = (l+r)/2;                          θ(1)

	if (A[m]>m+1){                            θ(1)
		return existeIndice_aux(A,l,m);       T(n/2)
	} else {
		return existeIndice_aux(A,m,r);       T(n/2)
	}
}

// A[m]=m → existeIndice_aux(A,m,r)
// A[m]<m → existeIndice_aux(A,m,r)
// A[m]>m → existeIndice_aux(A,l,m)







*** Cálculo de la complejidad ***

T(n) = T(n/2) + θ(1)

Observación: es la misma complejidad que la búsqueda binaria.

⟹ La complejidad del algoritmo es O(log(n))