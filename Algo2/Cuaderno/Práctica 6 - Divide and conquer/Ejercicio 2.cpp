// Ejercicio 2 ★
// Tenemos un arreglo a = [a 1 , a 2 , . . . , a n ] de n enteros distintos (positivos y negativos)
// en orden estrictamente creciente. Queremos determinar si existe una posición i tal que aᵢ = i.
// Por ejemplo, dado el arreglo a = [−4, −1, 2, 4, 7], i = 4 es esa posición.
// Diseñar un algoritmo dividir y conquistar eficiente (de complejidad de orden estrictamente menor
// que lineal) que resuelva el problema. Calcule y justifique la complejidad del algoritmo dado.

bool existeSubindice(vector<int> v, int l, int r){  // Rango [l,r)
    int n = r-l; // Tamaño actual

    if (n==1){            // θ(1)
        return v[l]==l+1; // θ(1)
    }

    int i = (r+l) / 2; // θ(1)

    if(v[i] > i+1){ // θ(1)
        return existeSubindice(v,l,i); // T(n/2)
    } else {       
        return existeSubindice(v,i,r); // T(n/2)
    }

}

// T(n) = T(n/2) + θ(1)
// Obs: la complejidad es igual a la de la búsqueda binaria

// ⟹ RTA: la complejidad es O(log(n))