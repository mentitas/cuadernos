// Ejercicio 6 ★
// Dado un árbol binario cualquiera, diseñar un algoritmo de dividir y conquistar que devuelva la máxima
// distancia entre dos nodos (es decir, máxima cantidad de ejes a atravesar). El algoritmo no debe hacer
// recorridos innecesarios sobre el árbol.


int distanciaMaxima(arbolBinario ab){
	return distanciaMaxima_aux(ab).first;
}

// Devuelve par (distancia maxima, altura maxima)
pair<int,int> distanciaMaxima_aux(arbolBinario ab){
	if(nil?(ab)){
		return make_pair(0,0);                                     
	} else {
		pair<int,int> recursiónDer = distanciaMaxima_aux(der(ab)); T(n/2)
		pair<int,int> recursiónIzq = distanciaMaxima_aux(izq(ab)); T(n/2)

		int alturaDer = 1 + recursiónDer.second;                   
		int alturaIzq = 1 + recursiónIzq.second;                   
	
		pair<int,int> medio = make_pair(alturaDer + alturaIzq + 1, max(alturaDer, alturaIzq) + 1);   

		return max(recursiónDer, recursiónIzq, medio);            
	}
}

// max devuelve la maxima tupla respecto al primer elemento de la tupla

T(n) = 2T(n/2) + O(1)


// Teorema maestro




// Caso 1: Si ∃ ε>0 tal que f(n) ∈ O(n^(log_c(a)-ε))

// f(n) ∈ O(n^(log_c(a)-ε))
// f(n) ∈ O(n^(log_2(2)-ε))
// f(n) ∈ O(n^1-ε)

//*tomando ε = 1*

// f(n) ∈ O(1)

// Entonces como me encuentro en el caso 1, T(n) = θ(n)


