// Ejercicio 6 ★
// Dado un árbol binario cualquiera, diseñar un algoritmo de dividir y conquistar que devuelva la máxima
// distancia entre dos nodos (es decir, máxima cantidad de ejes a atravesar). El algoritmo no debe hacer
// recorridos innecesarios sobre el árbol.

// Si yo tengo un árbol, el recorrido máximo puede estar en:
// > el subárbol izq
// > el subárbol der
// > en el medio


int distanciaMáxima(árbolBinario ab){
	
	if (nil?(ab)){                                                        θ(1)
		return 0;                                                         θ(1)
	}

	int recursiónDer = distanciaMáxima(der(ab));                          T(n/2)
	int recursiónIzq = distanciaMáxima(izq(ab));                          T(n/2)
	
	int distanciaMedio = altura(der(ab)) + altura(izq(ab)) + 1;           O(n)
           
	return max(recursiónIzq, recursiónDer, distanciaMedio);               θ(1)          

}

// O(n), siendo n la cantidad de nodos
int altura(arbolBinario ab){
	if(nil?(ab)){
		return 0;
	} else {
		return 1 + max(altura(der(ab)), altura(izq(ab)));
	}
}







*** Cálculo de la complejidad ***

T(n) = 2T(n/2) + O(n)      (siendo n la cantidad de nodos)

Observación: tiene la misma complejidad que el merge sort

⟹ La complejidad de T(n) es O(n*log(n))