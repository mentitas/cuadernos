// Ejercicio 5
// 
// Suponga que se tiene un método potencia que, dada un matriz cuadrada A de orden 4 × 4 y un número n,
// computa la matriz Aⁿ. Dada una matriz cuadrada A de orden 4 × 4 y un número natural n que es potencia
// de 2 (i.e., n = 2ᵏ para algun k ≥ 1), desarrollar, utilizando la técnica de dividir y conquistar y el
// método potencia, un algoritmo que permita calcular
// 	
// 	A¹ + A² + ... + Aⁿ .
// 
// Calcule el número de veces que el algoritmo propuesto aplica el método potencia. Si no es
// estrictamente menor que O(n), resuelva el ejercicio nuevamente.



// Idea:  

//     A¹ + A² + A³ + ... + A^n/2  +  ... + Aⁿ
// =  (A¹ + A² + A³ + ... + A^n/2) + A^n/2 * (A¹ + A² + A³ + ... + A^n/2)



array<array<int>> sumaDePotencias(array<array<int>> m, int n){

	if (n==1){                                                          θ(1)
		return m;                                                       θ(1)                       
	}

	array<array<int>> sumaHastaMitad = sumaDePotencias(m, n/2);         T(n/2)

	return sumaHastaMitad + potencia(m, n/2) * sumaHastaMitad;          θ(1) //(es suma y producto de matrices 4x4)
}







*** Cálculo de la complejidad ***

T(n) = T(n/2) + θ(1)

> Por cada recursión, uso la operación "potencia" una vez.
  Como hago log₂(n) pasos recursivos, uso la operación "potencia" log₂(n) veces

> La complejidad total del algoritmo es la misma que la de la búsqueda binaria.
  ⟹ La complejidad del algoritmo es θ(log(n)).