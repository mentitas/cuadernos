// Ejercicio 6 ★
// Dado un árbol binario cualquiera, diseñar un algoritmo de dividir y conquistar que devuelva la máxima
// distancia entre dos nodos (es decir, máxima cantidad de ejes a atravesar). El algoritmo no debe hacer
// recorridos innecesarios sobre el árbol.

// Se parece al ejercicio de la práctica


// 3 casos:
// La distancia maxima esta a la derecha
//                          a la izquierda
//                          en el medio




int distanciaMaxima(arbolBinario a){

	if(nil?(a)){
		return 0;
	}

	int recursionDer = distanciaMaxima(der(a));
	int recursionIzq = distanciaMaxima(izq(a));
	int sumaMedio = 1 + altura(der(a)) + altura(izq(a));

	return max(recursionDer, recursionIzq, sumaMedio);
}

int altura(arbolBinario a){
	if(nil?(a)){
		return 0;
	} else {
		return 1 + max(altura(der(a)), altura(izq(a)));
	}
}