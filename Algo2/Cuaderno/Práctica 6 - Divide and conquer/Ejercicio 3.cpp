// Ejercicio 3 ★
// Encuentre un algoritmo para calcular aᵇ en tiempo logarı́tmico en b. Piense cómo reutilizar los
// resultados ya calculados. Justifique la complejidad del algoritmo dado.


// Complejidad que quiero: O(log(b))

// Estoy calculando aᵇ
// Versión 1
int potenciaRara(int a, int b){

    if (b==0){    // θ(1)
        return 1; // θ(1)
    }
	if (b==1){    // θ(1)
		return a; // θ(1)
	}

	int recursion = potenciaRara(a, b/2); // T(b/2)

	if (b%2==0){
		return recursion*recursion;       // θ(1)
	} else {
		return a*recursion*recursion;     // θ(1)
	}
}



// Estoy calculando aᵇ
// Versión 2
int potenciaRara(int a, int b){
    if (b==0){    // θ(1)  
        return 1; // θ(1)
    }
    if (b==1){    // θ(1)
        return a; // θ(1)  
    }

    if (b%2==0){ // θ(1)
        return potenciaRara(a*a, b/2);   // T(b/2)
    } else {
        return potenciaRara(a*a, b/2)*a; // T(b/2)
    }
}


// Para las dos versiones:
// T(b) = T(b/2) + θ(1)

// Obs: es igual que la búsqueda binaria

//	⟹ RTA: O(log(b))
