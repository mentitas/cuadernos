// Ejercicio 9

// Dados dos arreglos de naturales, ambos ordenados de manera creciente, se desea buscar, dada una posición i,
// el i-ésimo elemento de la unión de ambos. Dicho de otra forma, el i-ésimo del resultado de hacer merge ordenado
// entre ambos arreglos. Notar que no es necesario hacer el merge completo. Se puede asumir que cada natural
// aparece a lo sumo en uno de los arreglos, y a lo sumo una vez.

// a) Implementar la función
// iésimoMerge(in A: arreglo(nat), in B: arreglo(nat), in i: nat) → nat
// que resuelve el problema planteado. La función debe ser de tiempo O(log²n), dónde n = tam(A) = tam(B).


// O(log(n)) ⊆ O(log²(n)) ⊆ O(n)

// Algoritmo O(n)
// Pre: n = |A| = |B|
int iesimoMerge(vector<int> A, vector<int> B, int i){
	int n = A.size();

	int j=0;
	int a=0;
	int b=0;
	int res=0;

	while(j<=i && a<n && b<n){  // O(2n) = O(n)
		if(A[a]<B[b]){
			a++;
			j++;
			res = A[a];
		} else {
			b++;
			j++;
			res = B[b];
		}
	}

	while(j<=i && a<n){
		a++;
		j++;
		res = A[a];
	}

	while(j<=i && b<n){
		b++;
		j++;
		res = B[b];
	}

	return res;}

// Algoritmo θ(log(n))
int iesimoMerge(vector<int> A, vector<int> B, int i){
	return iesimoMerge_aux(A,B,i,0,A.size());
}

int iesimoMerge_aux(vector<int> A, vector<int> B, int i, int l, int r){ // Rango [l,r)
	int n=r-l; // θ(1)

	if(n==1){        // θ(1)
		return A[l]; // θ(1)
	}

	int ma = (l+r)/2; // θ(1)
	int mb = posInmediatamenteMenor(B,A[ma]); // O(log(n))

	if (ma+mb > i){ // θ(1)
		return iesimoMerge_aux(A,B,l,ma); // T(n/2)
	} else {
		return iesimoMerge_aux(A,B,ma,r); // T(n/2)
	}
}

int posInmediatamenteMenor_aux(vector<int> A, int e, int l, int r){ // Rango [l,r)
	int n = r-l;

	if (n==1){
		return l;
	}

	int m = (l+r)/2

	if(A[m]>e){
		return posInmediatamenteMenor_aux(A,l,m);
	} else {
		return posInmediatamenteMenor_aux(A,m,r);
	}
}

// b) Calcular y justificar la complejidad del algoritmo propuesto


// T(n) = T(n/2) + O(log(n))

// Caso 1: Si ∃ ε>0 tal que f(n) ∈ O(n^(log_c(a)-ε))

// f(n) ∈ O(n^(log_c(a)-ε))
// f(n) ∈ O(n^(log_2(1)-ε))
// f(n) ∈ O(n^(0-ε))

// ⟹ no es caso 1


// Caso 2: f(n) ∈ θ(n^log_c(a))

// f(n) ∈ θ(n^log_c(a))
// f(n) ∈ θ(n^log_2(1))
// f(n) ∈ θ(n^0)
// f(n) ∈ θ(1)

// ⟹ no es caso 2


// Caso 3: ∃ ε>0 tal que f(n) ∈ Ω(n^(log_c(a) + ε)) y ∃ δ<1, ∃ n₀>0 tal que ∀ n≥n₀ se cumple a*f(n/c) ≤ δ*f(n)

// f(n) ∈ Ω(n^(log_c(a) + ε))
// f(n) ∈ Ω(n^(log_2(1) + ε))
// f(n) ∈ Ω(n^(0 + ε))
// f(n) ∈ Ω(n^ε)

// *tomo ε=0.001*

// Se cumple que f(n) ∈ Ω(n^0.001) (chequeado en geogebra)

//      a*f(n/c) ≤ δ*f(n)
//        f(n/2) ≤ δ*f(n)
//      log(n/2) ≤ δ*log(n)
//             0 ≤ δ*log(n)-log(n/2)
//             0 ≤ log(n^δ)-log(n/2)
//             0 ≤ log(n^δ / (n/2))
//             0 ≤ log(2n^(δ-1)) → y ésto se cumple ya que log(x)≥0 ∀ x ∈ ℝ

// Entonces como me encuentro en el caso 3, T(n) = θ(f(n)) = θ(log(n))



// c) Intente resolver el mismo problema en tiempo O(log n) (este ı́tem es bastante mas difı́cil, se incluye como
// desafı́o adicional).

// ???