// Ejercicio 11

// Se tiene un arreglo de números naturales A. Además se cuenta con estructuras adicionales sobre el arreglo
// que proveen la función
  "aparece?(in A: arreglo(nat), in i:nat, in j:nat, in e: nat) → bool"
// que dado el arreglo A, ı́ndices i, j y un natural e, devuelve true si y sólo si e = A[k] para algún k tal que
// i ≤ k ≤ j. Además se sabe que aparece? toma tiempo O(√j−i+1), es decir, la raiz cuadrada del tamaño del
// intervalo de búsqueda.

// Se desea encontrar un algoritmo sublineal que encuentra el ı́ndice de un elemento e en el arreglo A, asumiendo
// que tal elemento existe en el arreglo. El resultado de la función es justamente el ı́ndice i tal que A[i] = e.

// a) Implementar la función 
  "ubicar?(in A: arreglo(nat), in e: nat) → nat"
// que resuelve el problema planteado. La función debe ser de tiempo estrictamente menor a O(n), dónde n = tam(A)
// = tam(B) (formalmente, la complejidad del algoritmo no debe pertenecer a Ω(n))


// Pre: e ∈ A
int ubicar(vector<int> A, int e){

}

int ubicar_aux(vector<int> A, int e, int l, int r){ // Rango [l, r)
	
	int n = r-l;  // θ(1)

	if (n==1){    // θ(1)
		return l; // θ(1)
	}

	int m = (l+r)/2; // θ(1)

	if(aparece(A,l,m,e)){             // O(√(m-l+1)) = O(√(n/2))
		return ubicar_aux(A,l,m,e);   // T(n/2)
	} else {
		return ubicar_aux(A,m+1,r,e); // T(n/2)
	}

}

// b) Calcular y justificar la complejidad del algoritmo propuesto.

// T(n) = T(n/2) + O(√(n/2))

// log_c(a) = log_2(1) = 0

-------------------------------------------------
// Caso 1: ∃ ε>0 tal que f(n) ∈ O(n^log_c(a)-ε)

// f(n) ∈ O(n^log_c(a)-ε)
// f(n) ∈ O(n^(0-ε))

// No existe ε
// No es el caso 1

-------------------------------------------------
// Caso 2: f(n) ∈ θ(n^log_c(a))

// f(n) ∈ θ(n^log_c(a))
// f(n) ∈ θ(n^0)
// f(n) ∈ θ(1)

// No es el caso 2

-------------------------------------------------
// Caso 3: ∃ ε>0 tal que f(n) ∈ Ω(n^(log_c(a)+ε)) y ∃ δ<1, ∃ n₀>0 tal que ∀ n≥n₀ se cumple a*f(n/c) ≤ δ*f(n)

// f(n) ∈ Ω(n^(log_c(a)+ε))
// f(n) ∈ Ω(n^ε)

// *Voy a asumir que ésto se cumple, porque no hay manera de que f(n) ∉ Ω(1),
// entonces debe existir un ε lo suficientemente chico (ε≠0) tal que f(n) ∈ Ω(n^ε)*

// a*f(n/c) ≤ δ*f(n)
//   f(n/2) ≤ δ*f(n)
//   √(n/2) ≤ δ*√(n)

// *tomo δ = 1/4*

//   √(n/2) ≤ 1/4*√(n)
//   √(n/2) ≤ √(n/2)   → ¡y ésto se cumple!

// Entonces como me encuentro en el caso 3, T(n) = θ(f(n)) = θ(√n)

// Finalmente, como θ(√n) ⊆ O(n) ∧ θ(√n) ⊈ Ω(n), se cumple la complejidad requerida.