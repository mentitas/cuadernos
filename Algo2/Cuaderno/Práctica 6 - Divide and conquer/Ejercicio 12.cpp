Ejercicio 12 (Difı́cil)
Se tiene un tablero rectangular de n × n posiciones, con n potencia de 2, donde una de las posiciones se
encuentra inicialmente ocupada. Diseñar un algoritmo con la técnica de dividir y conquistar para rellenar
todas las posiciones del tablero con figuras que ocupan 3 posiciones y tienen forma de L. Formalmente, podemos
definir el problema de la siguiente forma:

dado un valor n y un par de valores i₀ , j₀ (1 ≤ i₀ , j₀ ≤ n), se quiere
encontrar una matriz B de tamaño n × n tal que:
	> B[i₀ , j₀ ] = 0,
	> Todos los valores entre 1 y (n² − 1)/3 aparecen exactamente tres veces en B, y
	> Para todo 1 ≤ i, j ≤ n tal que (i, j) ≠ (i₀, j₀), ocurre que el conjunto
	{B[x, y] | 1 ≤ x, y ≤ n e i − 1 ≤ x ≤ i + 1 y j − 1 ≤ y ≤ j + 1}
	contiene exactamente tres elementos con el valor B[i, j] (uno de los cuales es B[i, j]).

Ningun entero aparece más de dos veces en la misma fila o columna.
Por ejemplo, si n = 4, entonces la matriz B podrı́a ser

· con i₀=1 y j₀=1            · con i₀=3 y j₀=2            · con i₀=4 y j₀=2     
  0 1 2 2                      1 1 2 2                      1 1 2 2            
  1 1 4 2                      1 5 5 2                      1 4 4 2            
  3 4 4 5                      3 0 5 4                      3 3 4 5           
  3 3 5 5                      3 3 4 4                      3 0 5 5            



Observación:
Si no se cumple que el 0 se encuentra cerca de una esquina (formalmente, 
(1 ≤ i₀ ≤ 2  ∨  n-1 ≤ i₀ ≤ n) ∧ (1 ≤ j₀ ≤ 2  ∨  n-1 ≤ j₀ ≤ n) )
entonces las esquinas son Ls




// Idea
// Divido en cuadrados

// Coloco 4 elementos iguales en el 2x2

· con i₀=1 y j₀=1            · con i₀=3 y j₀=2            · con i₀=4 y j₀=2     
  1 1 2 2                      1 1 2 2                      1 1 2 2            
  1 1 2 2                      1 1 2 2                      1 1 2 2            
  3 3 5 5                      3 3 4 4                      3 3 5 5           
  3 3 5 5                      3 3 4 4                      3 3 5 5           

// coloco el 0

· con i₀=1 y j₀=1            · con i₀=3 y j₀=2            · con i₀=4 y j₀=2     
  0 1 2 2                      1 1 2 2                      1 1 2 2            
  1 1 2 2                      1 1 2 2                      1 1 2 2            
  3 3 5 5                      3 0 4 4                      3 3 5 5           
  3 3 5 5                      3 3 4 4                      3 0 5 5           

// Emprolijo alrededor del cuadrado recién formado

· con i₀=1 y j₀=1            · con i₀=3 y j₀=2            · con i₀=4 y j₀=2     
  0 1 2 2                      1 1 2 2                      1 1 2 2            
  1 1 _ 2                      1 _ _ 2                      1 _ _ 2            
  3 _ _ 5                      3 0 _ 4                      3 3 _ 5           
  3 3 5 5                      3 3 4 4                      3 0 5 5           

· con i₀=1 y j₀=1            · con i₀=3 y j₀=2            · con i₀=4 y j₀=2     
  0 1 2 2                      1 1 2 2                      1 1 2 2            
  1 1 4 2                      1 5 5 2                      1 4 4 2            
  3 4 4 5                      3 0 5 4                      3 3 4 5           
  3 3 5 5                      3 3 4 4                      3 0 5 5           







void tableroConL(vector<vector<int>> &m, int i, int j){
	tableroConL_aux(m, i₀, j₀, make_pair(1,m.size(),1,m.size())); 
}

// Esto sirve para 4x4
void tableroConL_aux(vector<vector<int>> &m, int i, int j, pair<int,int,int,int> cuadrante){ 
	int i₀ = cuadrante.first
	int iₙ = cuadrante.second;
	int j₀ = cuadrante.third;
	int jₙ = cuadrante.forth;

	// Rango [i₀,iₙ] 

	if (iₙ == i₀+1){
		m[i₀][j₀] = // Algo
		m[i₀][jₙ] = // Algo
		m[iₙ][j₀] = // Algo
		m[iₙ][jₙ] = // Algo
	}


	int iₘ = (i₀ + iₙ - 1) / 2
	int jₘ = (j₀ + jₙ - 1) / 2

	pair<int,int,int,int> cuadranteA = make_pair(i₀, iₘ, j₀, jₘ);
	pair<int,int,int,int> cuadranteB = make_pair(i₀, iₘ, jₘ, jₙ);
	pair<int,int,int,int> cuadranteC = make_pair(iₘ, iₙ, j₀, jₘ);
	pair<int,int,int,int> cuadranteD = make_pair(iₘ, iₙ, jₘ, jₙ);

	tableroConL_aux(m, i, j, cuadranteA);
	tableroConL_aux(m, i, j, cuadranteB);
	tableroConL_aux(m, i, j, cuadranteC);
	tableroConL_aux(m, i, j, cuadranteD);

	m[i][j] = 0;

	if((i,j) ∉ cuadranteA){
		// Corrijo A
	} else if ((i,j) ∉ cuadranteB){
		// Corrijo B
	} else if ((i,j) ∉ cuadranteC){
		// Corrijo C
	} else {
		// Corrijo D
	}

}







// Ejemplo 8x8

0 1 2 2  6 6 7 7
1 1 4 2  6 8 8 7
3 4 4 5  9 9 8 a
3 3 5 5  9 b a a

d d e e  g b b c
d f f e  g g c c
h i i j  j k l l
h h i j  k k l X