// Ejercicio 1 ★
// 
// Escriba un algoritmo con dividir y conquistar que determine si un arreglo de tamaño potencia
// de 2 es más a la izquierda, donde “más a la izquierda” significa que:
// 
// > La suma de los elementos de la mitad izquierda superan los de la mitad derecha.
// > Cada una de las mitades es a su vez “más a la izquierda”.
// 
// Por ejemplo, el arreglo [8, 6, 7, 4, 5, 1, 3, 2] es “más a la izquierda”, pero 
// [8, 4, 7, 6, 5, 1, 3, 2] no lo es.
// Intente que su solución aproveche la técnica de modo que complejidad del algoritmo sea
// estrictamente menor a O(n²).


int suma(std::vector<int> v, int l, int r){ // Rango [l,r)
    int res = 0;            // θ(1)
    for(int i=l; i<r; i++){ // θ(r-l)
        res+=v[i];          // θ(1)
    }
    return res;             // θ(1)
}


bool masALaIzquierda(std::vector<int> v, int l, int r){ // Rango [l,r)

    int n = r-l; // Tamaño actual

    if (n==1){         // θ(1)
        return true;   // θ(1)
    }

    int m = (l+r) / 2; // θ(1)

    bool izqEsMayor = suma(v,l,m) >= suma(v,m,r);                       // 2θ(n/2) = θ(n)
    bool recursion  = masALaIzquierda(v,l,m) && masALaIzquierda(v,m,r); // 2T(n/2)

    return izqEsMayor && recursion; // θ(1)
}


// La complejidad es T(n) = 2T(n/2) + θ(n)
// Obs: la complejidad es igual a la del Merge Sort

//	⟹ RTA: La complejidad es O(n*log(n))