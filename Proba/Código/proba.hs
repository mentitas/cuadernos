-- acá voy a hacer algunas funciones que calculen mi función de probabilidad para modelos ya conocidos


factorial :: Integer -> Integer
factorial 1 = 1
factorial n = factorial (n-1) * n

combinatorio :: Integer -> Integer -> Integer
combinatorio n 0 = 1
combinatorio n k | n == k    = 1 
                 | otherwise = (combinatorio (n-1) k) + (combinatorio (n-1) (k-1))

combinatorio2 :: Integer -> Integer -> Integer
combinatorio2 n 0 = 1
combinatorio2 n k | n == k    = 1
                  | otherwise = div (factorial n) ((factorial k)*(factorial (n-k)))

potencia :: Float -> Integer -> Float
potencia a 0 = 1
potencia a p = a * (potencia a (p-1))

--------------------------------------- Hipergeométrica ---------------------------------------
-- ñ = N, d = D

hg :: Integer -> Integer -> Integer -> Integer -> Float
hg ñ d n k = fromIntegral((combinatorio2 d k) * (combinatorio2 (ñ-d) (n-k))) / fromIntegral(combinatorio2 ñ n)  

hg_esperanza :: Integer -> Integer -> Integer -> [Integer] -> Float
hg_esperanza ñ d n []     = 0
hg_esperanza ñ d n (x:xs) = fromIntegral(x) * (hg ñ d n x) + (hg_esperanza ñ d n xs)

hg_esperanza_2 :: Integer -> Integer -> Integer -> [Integer] -> Float
hg_esperanza_2 ñ d n []     = 0
hg_esperanza_2 ñ d n (x:xs) = fromIntegral(x*x) * (hg ñ d n x) + (hg_esperanza_2 ñ d n xs)

hg_varianza :: Integer -> Integer -> Integer -> [Integer] -> Float
hg_varianza ñ d n x = (hg_esperanza_2 ñ d n x) - (potencia (hg_esperanza ñ d n x) 2) 


--------------------------------------- Binomial ---------------------------------------

bi :: Integer -> Float -> Integer -> Float
bi n p k = abs ( fromIntegral(combinatorio2 n k) * (potencia p k) * (potencia (p-1) (n-k)))

-- Rango: [i,j]
bi_rango :: Integer -> Float -> Integer -> Integer -> Float
bi_rango n p i j | i > j     = 0
                 | otherwise = (bi n p i) + (bi_rango n p (i+1) j)

bi_esperanza :: Integer -> Float -> [Integer] -> Float
bi_esperanza n p []     = 0
bi_esperanza n p (x:xs) = fromIntegral(x) * (bi n p x) + (bi_esperanza n p xs)

bi_esperanza_2 :: Integer -> Float -> [Integer] -> Float
bi_esperanza_2 n p []     = 0
bi_esperanza_2 n p (x:xs) = fromIntegral(x*x) * (bi n p x) + (bi_esperanza_2 n p xs)

bi_varianza :: Integer -> Float -> [Integer] -> Float
bi_varianza n p x = (bi_esperanza_2 n p x) - (potencia (bi_esperanza n p x) 2)