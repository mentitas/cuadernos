f x y = x * x + y * y

g x y z = x + y + z * z

doble x = 2*x

suma x y = x + y

normavectorial x y = sqrt (x^2 + y^2)

funcionConstanteOcho x = 8

f1 n | n == 0 = 1
     | n /= 0 = 0

f2 n | n == 0 = 1
     | otherwise = 0

signo1 n | n > 0 = 1
        | n < 0 = -1
        | n == 0 = 0

signo2 n | n > 0 = 1
         | n == 0 = 0
         | otherwise = -1


-- usando pattern matching
signo3 0 = 0
signo3 n | n > 0 = 1
         | otherwise = -1

maximo x y | x >= y = x
           | otherwise = y


-- si es mayor o igual a 3, devuelve 5; sino exception
f3 n | n >= 3 = 5

-- si es mayor o igual a 3, devuelve 5; si es mejor o igual a 1, devuelve 8; sino exception
f4 n | n >= 3 = 5
     | n <= 1 = 8

-- si es mayor o igual a 3, devuelve 5; si es 2, devuelve undefined; sino devuelve 8
f5 n | n >= 3 = 5
     | n == 2 = undefined
     | otherwise = 8


-- si el n cumple las dos condiciones, va a devolver la primera

g1 n | n >= 3 = 5
     | n <= 9 = 7
-- si n=4, devuelve 5

g2 n | n <= 9 = 7
     | n >= 3 =5
--si n=4, devuelve 7


escero n | n == 0 = 1
         | n /= 0 = 0

-- usando pattern matching (es lo mismo que escero)
esceroPM 0 = 1
esceroPM n = 0

cantidadDeSoluciones1 b c | b^2 - 4*c > 0 = 2
                          | b^2 - 4*c == 0 = 1
                          | otherwise = 0

cantidadDeSoluciones2 b c | d > 0 = 2
                          | d == 0 = 1
                          | otherwise = 0
                          where d = b^2 - 4*c



{- | Documentación

- Nombre: bicho - fórmula resolvente
- Declaración: Float -> Float -> Float -> (Float, Float)
- Descripción: Recibe 3 valores a, b y c, que corresponden a la fórmula
 ax^2 + bx + c = 0 y devuelve uno o dos resultados en forma de Double 
- Parámetros: Floats: a, b, c
- Retorna: (Float, Float): Las raíces de la función -}

bicho :: Float -> Float -> Float -> (Float, Float)
bicho a b c | d < 0 = (0, 0)
            | d > 0 = ((-b+d)/(2*a), (-b-d)/(2*a))
            | d == 0 = ((-b/(2*a), 0))
            where d = sqrt (b^2 - 4*a*c) 


{- | Documentación

- Nombre: maximoint
- Declaración: Int -> Int -> Int
- Descripción: Recibe dos valores enteros y devuelve el más grande.
- Parámetros: Int: x y
- Retorna: Int: el número más grande
- Función auxiliar: máximo -}


maximoint :: Int -> Int -> Int
maximoint x y = (maximo x y)



{- | Documentación

- Nombre: maximorac
- Declaración:  Float -> Float -> Float
- Descripción: Recibe dos valores float y devuelve el más grande.
- Parámetros: Float: x y
- Retorna: Float: el número más grande
- Función auxiliar: máximo -}

maximorac :: Float -> Float -> Float
maximorac x y = (maximo x y)



{- | Documentación

- Nombre: mayoranueve
- Declaración:  Int -> Bool
- Descripción: Recibe un n entero y decide si es o no mayor a nueve
- Parámetros: Int: el número
- Retorna: Bool: True si es mayor, False si es menor -}

mayoranueve :: Int -> Bool
mayoranueve n | n > 9 = True
              | otherwise = False


{- | Documentación

- Nombre: espar
- Declaración:  Int -> Bool
- Descripción: Recibe un n entero y decide si es o no es par.
- Parámetros: Int: el número
- Retorna: Bool: True si es par, False si es impar -}

espar :: Int -> Bool
espar n | mod n 2 == 0 = True
        | otherwise = False

-- funciona igual que espar, pero es raro
espar2 :: Int -> Bool
espar2 n = mod n 2 == 0

esimpar :: Int -> Bool
esimpar n = not (espar n)


--z es bool. Si alguno de los dos es true (o los dos son true) (z o si (x>=y)), devuelve true
funcionrara :: Float -> Float -> Bool -> Bool
funcionrara x y z = (x >= y) || z

--PM es por Pattern Matching: si z es true, devuelve true. Si z es false, se fija si (x>=y) es true
funcionraraPM1 :: Float -> Float -> Bool -> Bool
funcionraraPM1 x y True = True
funcionraraPM1 x y False = x >= y

--Lo mismo de arriba, pero si z es true ni se fija que pasa con x e y
funcionraraPM2 :: Float -> Float -> Bool -> Bool
funcionraraPM2 _ _ True = True
funcionraraPM2 x y False = x >= y 


--ejercicios de tarea


--absoluto: calcula el valor absoluto de un número entero

absoluto :: Int -> Int
absoluto x | x < 0 = (-1)*x
           | otherwise = x


--maximoabsoluto: devuelve el máximo entre el valor absoluto de dos números enteros

maximoabsoluto :: Int -> Int -> Int
maximoabsoluto x y = maximo (absoluto x) (absoluto y)


--maximo3: devuelve el m´aximo entre tres n´umeros enteros
maximo3 :: Int -> Int -> Int -> Int 
maximo3 x y z = maximo x (maximo y z)


--algunoEs0: dados dos números racionales, decide si alguno de los dos es igual a 0 (hacerlo
--dos veces, una sin usar y otra usando pattern matching).

algunoescero :: Float -> Float -> Bool
algunoescero x y | x == 0 || y == 0 = True
                 | otherwise = False

algunoesceroPM :: Float -> Float -> Bool
algunoesceroPM _ 0 = True
algunoesceroPM 0 _ = True
algunoesceroPM _ _ = False


--ambosSon0: dados dos n´umeros racionales, decide si ambos son iguales a 0 (hacerlo dos
--veces, una sin usar y otra usando pattern matching).

ambossoncero :: Float -> Float -> Bool
ambossoncero x y | x==0 && y==0 = True
                 | otherwise = False

ambossonceroPM :: Float -> Float -> Bool
ambossonceroPM 0 0 = True
ambossonceroPM _ _ = False


--esMultiploDe: dados dos n´umeros naturales, decidir si el primero es m´ultiplo del segundo.

esmultiplode :: Int -> Int -> Bool
esmultiplode x y | mod x y == 0 = True
                 | otherwise = False


--digitoUnidades: dado un n´umero natural, extrae su d´ıgito de las unidad

digitounidades :: Int -> Int
digitounidades x = mod x 10


--digitoDecenas: dado un n´umero natural, extrae su d´ıgito de las decenas.
digitodecenas :: Int -> Int
digitodecenas x = digitounidades (div x 10)