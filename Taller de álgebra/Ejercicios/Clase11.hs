type Complejo = (Float, Float)

-- Ejercicio 1: re :: Complejo -> Float

re :: Complejo -> Float
re (a, b) = a


-- Ejercicio 2: im :: Complejo -> Float

im :: Complejo -> Float
im (a, b) = b


-- Ejercicio 3: conjugado :: Complejo -> Complejo

conjugado :: Complejo -> Complejo
conjugado (a, b) = (a, -b)


-- Ejercicio 4: suma :: Complejo -> Complejo -> Complejo

suma :: Complejo -> Complejo -> Complejo
suma (a, b) (c, d) = (a+c, b+d)


-- Ejercicio 5: producto :: Complejo -> Complejo -> Complejo

producto :: Complejo -> Complejo -> Complejo 
producto (a, b) (c, d) = (a*c - b*d, a*d + b*c)


-- Ejercicio 6: inverso :: Complejo -> Complejo

inverso :: Complejo -> Complejo 
inverso (a, b) =  (a/c, -b/c)
 where c = (a**2 + b**2)

--para hacer potencias de floats hay que usar ** en vez de ^


-- Ejercicio 7: cociente :: Complejo -> Complejo -> Complejo

cociente :: Complejo -> Complejo -> Complejo
cociente a b = a `producto` (inverso b)


-- Ejercicio 8: potencia :: Complejo -> Int -> Complejo

potencia :: Complejo -> Int -> Complejo
potencia a 0 = (1, 0)
potencia a p = a `producto` (potencia a (p-1))


{- Ejericio 9 : solucionesCuadratica :: Float -> Float -> Float -> (Complejo, Complejo)
Dada una función cuadrática ax2 + bx + c con a, b, c ∈ R,
a /= 0, definir la función que tome los coeficientes a, b y c y
devuelve las dos raíces. En caso de haber una sola, devolverla dos veces. -}

solucionesCuadratica :: Float -> Float -> Float -> (Complejo, Complejo)
solucionesCuadratica a b c
 | det == 0  = ((-b/(2*a), 0), (-b/(2*a), 0))
 | det > 0   = (((-b+det')/(2*a), 0), ((-b-det')/(2*a), 0))
 | otherwise = ((-b/(2*a), det'/(2*a)), (-b/(2*a), -det'/(2*a)))
 where det  = (b**2 - 4*a*c)
       det' = sqrt (det)


-- Ejercicio 10: modulo :: Complejo -> Float 

modulo :: Complejo -> Float
modulo (a, b) = sqrt (a**2 + b**2)

-- Ejercicio 11: argumento :: Complejo -> Float

argumento :: Complejo -> Float
argumento (a, b)
 | (a<0)     = 1 + arg
 | (b<0)     = 2 + arg
 | otherwise = arg
 where arg = (atan (b/a))

argumentar :: Float -> Float
argumentar a
 | (a > 2) = argumentar (a-2)
 | (a < 0) = argumentar (a+2)
 | otherwise = a


{- Ejercicio 12: pasarACartesianas :: Float -> Float -> Complejo
que toma r ≥ 0 y θ ∈ [0, 2π) y devuelve el complejo z que tiene módulo r
y argumento θ -}

pasarACartesianas :: Float -> Float -> Complejo
pasarACartesianas r arg = (r*cos(arg), r*sin(arg))

-- el argumento está en radianes
pasarACartesianasRad :: Float -> Float -> Complejo
pasarACartesianasRad r arg = (r*cos(a), r*sin(a))
 where a = arg*pi

-- el argumento está en radianes y además redondea los números muy peques
pasarACartesianasRadRed :: Float -> Float -> Complejo
pasarACartesianasRadRed r arg = (redondearACero a, redondearACero b)
 where (a, b) = pasarACartesianasRad r arg



{- Ejercicio 13: raizCuadrada :: Complejo -> (Complejo,Complejo)
Dado z ∈ C, devuelve los dos complejos w que satisfacen w2 = z -}
-- (mod w)^2 = mod z
-- (2*arg w) = arg z       

raizCuadrada :: Complejo -> (Complejo, Complejo)
raizCuadrada (a, b) = (pasarACartesianas r arg1, pasarACartesianas r arg2)
 where r = sqrt (modulo (a, b))
       arg1 = (argumento (a, b))/2
       arg2 = (argumento (a, b))/2 + pi

-- ¿ESTÁ BIEN HECHO?




{- Ejercicio 14: raicesNEsimas :: Int -> [Complejo]
que, dado n natural, devuelve la lista con las raíces n-ésimas de la
unidad. -}

-- argumentos son: 2kpi/n


argumentosNesimosHasta :: Float -> Float -> [Float]
argumentosNesimosHasta n 0 = []
argumentosNesimosHasta n k = (2*k*pi/n) : argumentosNesimosHasta n (k-1)

argumentosNesimos :: Float -> [Float]
argumentosNesimos n = argumentosNesimosHasta n n

raicesNesimasConArgs :: Float -> [Float] -> [Complejo]
raicesNesimasConArgs n []     = []
raicesNesimasConArgs n (a:as) = redondearComplejoACero (pasarACartesianas 1 a) : (raicesNesimasConArgs n as)

raicesNesimas :: Float -> [Complejo]
raicesNesimas n = raicesNesimasConArgs n (args)
 where args = argumentosNesimos n


---- lo mismo pero con radianes ----

argumentosNesimosHastaRad :: Float -> Float -> [Float]
argumentosNesimosHastaRad n 0 = []
argumentosNesimosHastaRad n k = (2*k/n) : argumentosNesimosHastaRad n (k-1)

argumentosNesimosRad :: Float -> [Float]
argumentosNesimosRad n = argumentosNesimosHastaRad n n

raicesNesimasConArgsRad :: Float -> [Float] -> [Complejo]
raicesNesimasConArgsRad n []     = []
raicesNesimasConArgsRad n (a:as) = (pasarACartesianasRad 1 a) : (raicesNesimasConArgsRad n as)

raicesNesimasRad :: Float -> [Complejo]
raicesNesimasRad n = raicesNesimasConArgsRad n (args)
 where args = argumentosNesimosRad n




{- Ejercicio 15: potenciasRaizNesima :: Int -> Int -> [Complejo]
que, dados k y n enteros con 0 ≤ k < n, devuelve la lista con las
potencias 0, 1, ... , n − 1 de la raíz n-ésima asociada a k
siguiendo la fórmula de arriba. -}

potenciasRaizNesimaHasta :: Int -> Int -> Int -> [Complejo]
potenciasRaizNesimaHasta n k 0 = [1]
potenciasRaizNesimaHasta n k m = (potencia raiz m) : potenciasRaizNesimaHasta n k (m-1)
 where raiz = pasarACartesianas 1 (2*k*pi)/n)

potenciasRaizNesima :: Int -> Int -> [Complejo]
potenciasRaizNesima n k = potenciasRaizNesimaHasta n k (n-1)



----------------------(Funciones Propias)----------------------

grados_Rad :: Float -> Float
grados_Rad g = g/pi

rad_Grados :: Float -> Float
rad_Grados g = g*pi

bin_trig :: Complejo -> (Float, Float)
bin_trig a = (modulo a, argumento a) 

bin_trigRad :: Complejo -> (Float, Float)
bin_trigRad a = (modulo a, grados_Rad (argumento a))

trig_bin :: (Float, Float) -> Complejo
trig_bin (r, arg) = (r*cos(arg), r*sin(arg))

trig_binRad :: (Float, Float) -> Complejo
trig_binRad (r, arg) = (r*cos(rad_Grados arg), r*sin(rad_Grados arg))

redondearACero :: Float -> Float
redondearACero a 
 | abs a < (1e-6) = 0
 | otherwise      = a

redondearComplejoACero :: Complejo -> Complejo
redondearComplejoACero (a,b) = (redondearACero a, redondearACero b)