module FuncionesClase01
where
    {- | Documentación

    Las funciones que hay en éste módulo son:

     - normavectorial
     - cantidaddesoluciones
     - bicho
     - espar
     - esimpar
     - esmultiplode
     - digitounidades
     - digitodecenas
     -}
                   
    indice1 x = x
    -- esta función no hace nada, pero su doc devuelve el índice


    {- |Documentación

    - Nombre: cantidaddesoluciones
    - Declaración: (Num t, Ord t) => t -> t -> Int
    - Descripción: Recibe dos valores b y c, y decide cuántas soluciones tiene
    la fórmula resolvente. b y c corresponden a ax^2 + bx + c = 0
    - Parámetros: (Num t, Ord t): b y c. 
      .Son Num porque se les aplica multiplicación y potencia
      .Son Ord porque d se compara con igual y con mayor
    - Retorna: Int: la cantidad de soluciones
    -}


    cantidaddesoluciones :: (Num t, Ord t) => t -> t -> Int
    cantidaddesoluciones b c | d > 0 = 2
                             | d == 0 = 1
                             | otherwise = 0
                             where d = b^2 - 4*c

    {- |Documentación

    - Nombre: normavectorial
    - Descripción: Recibe dos valores y devuelve la norma vectorial
    - Parámetros: x y
    - Retorna: la norma vectorial
    -}

    normavectorial x y = sqrt (x^2 + y^2)

    {- |Documentación

    - Nombre: bicho - fórmula resolvente
    - Declaración: Float -> Float -> Float -> (Float, Float)
    - Descripción: Recibe 3 valores a, b y c, que corresponden a la fórmula
       ax^2 + bx + c = 0 y devuelve uno o dos resultados en forma de Double 
    - Parámetros: Floats: a, b, c
    - Retorna: (Float, Float): Las raíces de la función 
    -}

    bicho :: Float -> Float -> Float -> (Float, Float)
    bicho a b c | d < 0 = (0, 0)
                | d > 0 = ((-b+d)/(2*a), (-b-d)/(2*a))
                | d == 0 = ((-b/(2*a), 0))
                where d = sqrt (b^2 - 4*a*c)

    {- | Documentación

    - Nombre: espar
    - Declaración:  Int -> Bool
    - Descripción: Recibe un n entero y decide si es o no es par.
    - Parámetros: Int: el número
    - Retorna: Bool: True si es par, False si es impar 
    -}

    espar :: Int -> Bool
    espar n | mod n 2 == 0 = True
            | otherwise = False


    {- | Documentación

    - Nombre: esimpar
    - Declaración:  Int -> Bool
    - Descripción: Recibe un n entero y decide si es o no es impar.
    - Parámetros: Int: el número
    - Retorna: Bool: True si es impar, False si es par
    - Necesita: espar 
    -}

    esimpar :: Int -> Bool
    esimpar n = not (espar n)



    {- | Documentación

    - Nombre: esmultiplode
    - Declaración:  Int -> Int -> Bool
    - Descripción: Recibe dos números enteros x y y decide si el primero es
    múltiplo del segundo; queremos ver si x es múltiplo de y
    - Parámetros: Int: x y 
    - Retorna: Bool: True si es múltiplo, False si no es múltiplo
    -}

    esmultiplode :: Int -> Int -> Bool
    esmultiplode x y | mod x y == 0 = True
                     | otherwise = False


    {- | Documentación

    - Nombre: digitounidades
    - Declaración:  Int -> Int
    - Descripción: Dado un número natural, extrae el dígito de las unidades
    - Parámetros: Int: el número entero
    - Retorna: Int: el dígito de las unidades
    -}

    digitounidades :: Int -> Int
    digitounidades x = mod x 10


    {- | Documentación

    - Nombre: digitodecenas
    - Declaración:  Int -> Int
    - Descripción: Dado un número natural, extrae el dígito de las decenas
    - Parámetros: Int: el número entero
    - Retorna: Int: el dígito de las decenas
    - Necesita: digitounidades
    -}

    digitodecenas :: Int -> Int
    digitodecenas x = digitounidades (div x 10)
