module FuncionesClase02
where
    {- | Documentación

    Las funciones que hay en éste módulo son:

     - mismotipo
     - maximo
     - sumavectores
     - restavectores
     - sumanormal
     - prodint (producto interno)
     - distanciapuntos
     -}
                   
    indice2 x = x
    -- esta función no hace nada, pero su doc devuelve el índice


    {- |Documentación

    - Nombre: mismotipo
    - Declaración: t -> t -> Bool
    - Descripción: Recibe dos valores y si son del mismo tipo, devuelve
    True, sino devuelve error
    - Parámetros: t: dos valores
    - Retorna: Bool: Si son o no del mismo tipo
    -}

    mismotipo :: t -> t -> Bool
    mismotipo x y = True


    {- |Documentación

    - Nombre: maximo
    - Declaración: (Ord a) => a -> a -> a
    - Descripción: Recibe dos valores y devuelve el más grande
    - Parámetros: (Ord a) x y. Son Ord porque tienen que tener un orden
    para compararlos con mayor, menor o igual
    - Retorna: a: el valor más grande
    -}

    maximo :: (Ord a) => a -> a -> a
    maximo x y | x >= y = x
               | otherwise = y


    {- |Documentación

    - Nombre: sumavectores
    - Declaración: (Ord a) => a -> a -> a
    - Descripción: Recibe dos valores y devuelve el más grande
    - Parámetros: (Ord a) x y. Son Ord porque tienen que tener un orden
    para compararlos con mayor, menor o igual
    - Retorna: a: el valor más grande
    -}