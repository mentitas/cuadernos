import FuncionesClase01



{- Ejercicio 1: sumatoria :: [Int] -> Int
que indica la suma de los elementos de una lista-}

{- |Documentación:

    - Nombre: sumatoria
    - Declaración: [Int] -> Int
    - Descripción: Recibe una lista de enteros y devuelve 
    la suma de todos los elementos. La función funciona tomando
    la cabeza y achicando la lista, y sumando la cabeza con el
    primer elemento de la lista achicada, así sucesivamente
    hasta que la lista no tenga elementos.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: Int: la suma de todos los elementos de la lista.
    -}


sumatoria :: [Int] -> Int
sumatoria a | a == [] = 0
            | otherwise = head a + sumatoria (tail a)





{- |Documentación:

    - Nombre: sumatoriapm
    - Declaración: [Int] -> Int
    - Descripción: Recibe una lista de enteros y devuelve 
    la suma de todos los elementos. La función funciona tomando
    la cabeza y achicando la lista, es decir sumando la cabeza 
    con el primer elemento de la lista achicada, así 
    sucesivamente hasta que la lista no tenga elementos.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: Int: la suma de todos los elementos de la lista.
    -}


sumatoriapm :: [Int] -> Int
sumatoriapm []     = 0
sumatoriapm (h:t)  = h + sumatoriapm (t)






{- Ejercicio 2: longitud :: [Int] -> Int
que indica cuántos elementos tiene una lista -}


{- |Documentación:

    - Nombre: longitud
    - Declaración: [Int] -> Int
    - Descripción: Recibe una lista de enteros y devuelve 
    la longitud de la lista. Se suma +1 cada vez que se le
    quita un elemento a la lista, hasta que no queden más 
    elementos.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: Int: la cantidad de elementos de la lista.
    -}


longitud :: [Int] -> Int
longitud a | a == [] = 0
           | otherwise = 1 + longitud (tail a)







{- |Documentación:

    - Nombre: longitudpm
    - Declaración: [Int] -> Int
    - Descripción: Recibe una lista de enteros y devuelve 
    la longitud de la lista. Se suma +1 cada vez que se le
    quita un elemento a la lista, hasta que no queden más 
    elementos.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: Int: la cantidad de elementos de la lista.
    -}


longitudpm :: [Int] -> Int
longitudpm [] = 0
longitudpm (_:c) = 1 + longitudpm c






{- Ejercicio 3: pertenece :: Int -> [Int] -> Bool
que indica si un elemento aparece en la lista. Por ejemplo:
pertenece 9 []             = False
pertenece 9 [1,2,3]        = False
pertenece 9 [1,2,9,9,-1,0] = True   -}

{- |Documentación:

    - Nombre: pertenece
    - Declaración: Int -> [Int] -> Bool
    - Descripción: Recibe un entero n y una lista de enteros
    y evalúa si la lista contiene n. La lista se fija si n es
    la cabeza de la lista, y sino achica la lista y se vuelve
    a fijar (hasta que la lista no tenga más elementos).
    - Parámetros: Int: Entero que quiero ver si pertenece. 
                  [Int]: Lista de enteros.
    - Retorna: Bool: Si el entero se encuentra en la lista.
    -}


pertenece :: Int -> [Int] -> Bool
pertenece n a | a == [] = False
              | head a == n = True
              | otherwise = pertenece n (tail a)





{- |Documentación:

    - Nombre: pertenecepm
    - Declaración: Int -> [Int] -> Bool
    - Descripción: Recibe un entero n y una lista de enteros
    y evalúa si la lista contiene n. La lista se fija si n es
    la cabeza de la lista, y sino achica la lista y se vuelve
    a fijar (hasta que la lista no tenga más elementos).
    - Parámetros: Int: Entero que quiero ver si pertenece. 
                  [Int]: Lista de enteros.
    - Retorna: Bool: Si el entero se encuentra en la lista.
    -}


pertenecepm :: [Int] -> Int -> Bool
pertenecepm [] _    = False
--pertenecepm (n:_) n = True                <- No anda :(
pertenecepm (h:t) n = pertenecepm t n







{- Ejercicio 4: primermultiplode45345 :: [Int] -> Int
que indica el primer elemento de la lista que es múltiplo 
de 45345 que encuentre en la lista. -}

{- |Documentación:

    - Nombre: primermúltiplode45345
    - Declaración: [Int] -> Int
    - Descripción: Recibe una lista de enteros y devuelve
    el primer múltiplo de 45345 que encuentra. La función
    se fija si el primer elemento es múltiplo de 45345,
    y si no es le quita el primer elemento y vuelve a comparar.
    La función termina cuando no tiene más elementos para 
    evaluar.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: Int: El primer múltiplo de 45345 que encuentra.
    -}


primermúltiplode45345 :: [Int] -> Int
primermúltiplode45345 a | a == [] = 0
                        | mod (head a) 45345 == 0 = head a
                        | otherwise = primermúltiplode45345 (tail a) 






{- Ejercicio 1: productoria [Int] -> Int
que devuelve la productoria de los elementos -}


{- |Documentación:

    - Nombre: productoria
    - Declaración: [Int] -> Int
    - Descripción: Recibe una lista de enteros y devuelve 
    el producto de todos los elementos. La función toma el
    primer elemento de la lista, achica la lista, y multiplica
    el primer elemento con el primer elemento de la lista
    achicada. La función termina cuando la lista ya no tiene más
    elementos.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: Int: el producto de todos los elementos de la lista.
    -}


productoria :: [Int] -> Int
productoria l | l == [] = 1
              | otherwise = (head l) * productoria (tail l)






{- |Documentación:

    - Nombre: productoriapm
    - Declaración: [Int] -> Int
    - Descripción: Recibe una lista de enteros y devuelve 
    el producto de todos los elementos. La función toma el
    primer elemento de la lista, achica la lista, y multiplica
    el primer elemento con el primer elemento de la lista
    achicada. La función termina cuando la lista ya no tiene más
    elementos.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: Int: el producto de todos los elementos de la lista.
    -}


productoriapm :: [Int] -> Int
productoriapm [] = 1
productoriapm (h:t) = h * productoria t







{- Ejercicio 2: sumarn :: Int -> [Int] -> [Int]
que dado un número n y una lista xs, suma n a cada elemento de s-}

{- |Documentación:

    - Nombre: sumarn
    - Declaración: Int -> [Int] -> [Int]
    - Descripción: La función recibe un número entero n y una
    lista de enteros, y suma n a todos los elementos de la lista.
    La función suma n a la cabeza de la lista, y lo repite
    recursivamente con la cola de la lista.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: Int: el producto de todos los elementos de la lista.
    -}

sumarn :: Int -> [Int] -> [Int]
sumarn n l | l == [] = []
           | otherwise = (n + head l) : sumarn n (tail l)







{- |Documentación:

    - Nombre: sumarnPM
    - Declaración: Int -> [Int] -> [Int]
    - Descripción: La función recibe un número entero n y una
    lista de enteros, y suma n a todos los elementos de la lista.
    La función suma n a la cabeza de la lista, y lo repite
    recursivamente con la cola de la lista.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: Int: el producto de todos los elementos de la lista.
    -}


sumarnPM :: Int -> [Int] -> [Int]
sumarnPM n [] = []
sumarnPM n (x:s) = (n + x) : sumarnPM n s







{- Ejercicio 3: sumarelprimero :: [Int] -> [Int] 
que dada una lista no vacía xs, suma el primer elemento a cada
elemento de xs. Ejemplo sumarelprimero [1,2,3] = [2,3,4] -}


{- |Documentación:

    - Nombre: sumarelprimero
    - Declaración: [Int] -> [Int]
    - Descripción: La función recibe una lista y suma la cabeza
    de la lista a todos los elementos de la lista.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: [Int]: Lista de enteros a cuyos elementos
    se les sumó la cabeza de la lista.
    - Necesita: sumarn
    -}


sumarelprimero :: [Int] -> [Int]
sumarelprimero (x:xs) = sumarn x (x:xs) 







{- Ejercicio 4:  sumarelultimo :: [Int] -> [Int]
que dada una lista no vacía xs, suma el último elemento a 
cada elemento de xs. Ejemplo sumarelultimo [1,2,3] = [4,5,6] -}



{- |Documentación:

    - Nombre: ultimo
    - Declaración: [a] -> a
    - Descripción: Dado una lista de cualquier tipo, la función
    devuelve el último elemento. La función achica la lista hasta
    que solo queda un elemento, y devuelve ese elemento.
    - Parámetros: [a]: Lista de cualquier tipo
    - Retorna: a: el último elemento de la lista
    -} 


ultimo :: [a] -> a
ultimo l | length l == 1 = (head l)
         | otherwise = ultimo (tail l)







{- |Documentación:

    - Nombre: sumarelultimo
    - Declaración: [Int] -> [Int]
    - Descripción: La función recibe una lista y suma el último
    elemento a todos los elementos de la lista.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: [Int]: Lista de enteros a cuyos elementos
    se les sumó el último de la lista.
    - Necesita: sumarn y ultimo
    -}

sumarelultimo :: [Int] -> [Int]
sumarelultimo l = sumarn (ultimo l) l







{- Ejercicio 5:  pares :: [Int] -> [Int]
que devuelve una lista con los elementos pares de la lista
original. Ejemplo pares [1,2,3,5,8] = [2,8] -}

{- |Documentación:

    - Nombre: pares
    - Declaración: [Int] -> [Int]
    - Descripción: La función recibe una lista y devuelve una
    lista que contiene todos los elementos pares de la lista
    original. La función se fija si la cabeza de la función es
    par, y si es par, la agrega a los siguientes pares. Si no es
    par, achica la lista e intenta de vuelta. La función termina
    cuando la lista no tiene más elementos.
    - Parámetros: [Int]: Lista de enteros.
    - Retorna: [Int]: Lista con los elementos pares de la lista 
    anterior.
    - Necesita: espar (importado con FuncionesClase01)
    -}

pares :: [Int] -> [Int]
pares l | l  == [] = []
        | espar (head l) == True = (head l) : (pares (tail l))
        | otherwise = pares (tail l)







{- Ejercicio 6: quitar :: Int -> [Int] -> [Int] 
que elimina la primera aparición del elemento en la lista
(de haberla). -}

{- |Documentación:

    - Nombre: quitar
    - Declaración: Int -> [Int] -> [Int]
    - Descripción: La función recibe un número entero n y una 
    lista de enteros l, y quita la primer aparición (si la hay)
    de n en l. La función evalúa si la cabeza de la lista es n, y si
    no lo es, la concatena con el resto de la lista. Si la cabeza es
    igual a n, la saltea y continua con el resto de la lista.
    - Parámetros: Int: Número a quitar
                 [Int]: Lista de enteros.
    - Retorna: [Int]: Lista sin el elemento n.
    -}

quitar :: Int -> [Int] -> [Int]
quitar n l | l == [] = []
           | (head l) == n = (tail l)
           | otherwise = (head l) : (quitar n (tail l))







{- Ejercicio 7: quitarTodas :: Int -> [Int] -> [Int]
que elimina todas las apariciones del elemento
en la lista (de haberla). -}

{- |Documentación:

    - Nombre: quitartodas
    - Declaración: Int -> [Int] -> [Int]
    - Descripción: La función recibe un número entero n y una 
    lista de enteros l, y quita todas las apariciones (si las hay)
    de n en l. La función evalúa si la cabeza de la lista es n, y si
    no lo es, la concatena con el resto de la lista. Si la cabeza es
    igual a n, la saltea y continua con el resto de la lista.
    - Parámetros: Int: Número a quitar
                 [Int]: Lista de enteros.
    - Retorna: [Int]: Lista sin el elemento n.
    -}

quitartodas :: Int -> [Int] -> [Int]
quitartodas n l | l == [] = []
                | (head l) == n = quitartodas n (tail l)
                | otherwise = (head l) : (quitartodas n (tail l))
    







{- Ejercicio 8:  hayrepetidos :: [Int] -> Bool
que indica si una lista tiene elementos repetidos. -}

{- |Documentación:

    - Nombre: contiene
    - Declaración: Int -> [Int] -> Bool
    - Descripción: La función recibe un número entero n y una lista
    de enteros l, y decide si l contiene a n. La lista se fija si el
    primer elemento de l es igual a n. Si es igual devuelve True, sino
    achica la lista y sigue probando hasta que la lista no tenga
    elementos, entonces devuelve False.
    - Parámetros: Int: el número
                 [Int]: La lista
    - Retorna: Bool: Si la lista contiene o no al número
    -}


contiene :: Int -> [Int] -> Bool
contiene n l | l == []     = False
             | head l == n = True
             | otherwise = contiene n (tail l)








{- |Documentación:

    - Nombre: hayrepetidos
    - Declaración: [Int] -> Bool
    - Descripción: La función recibe una lista de enteros, y decide
    si algún elemento se repite. La función toma el primer elemento
    de la lista, y se fija si la cola lo contiene. Si la cola lo
    contiene, devuelve True, sino achica la lista y sigue probando
    hasta que la lista no tenga elementos, entonces devuelve False.
    y se fija si la cola contiene el
    - Parámetros: [Int]: La lista
    - Retorna: Bool: Si la lista tiene o no repetidos.
    - Necesita: contiene
    -}


hayrepetidos :: [Int] -> Bool
hayrepetidos l | l == [] = False
               | contiene (head l) (tail l) == True = True
               | otherwise = hayrepetidos (tail l) 








{- Ejercicio 9: eliminarrepetidosalfinal :: [Int] -> [Int]
que deja en la lista la primera aparición de cada elemento, 
eliminando las repeticiones adicionales. -}

{-
eliminarrepetidosalfinal :: [Int] -> [Int]   -- <- no se hacerlo :(
-}

-- ¿dando vuelta la lista?









{- Ejercicio 10:  eliminarrepetidosalinicio :: [Int] -> [Int]
que deja en la lista la última aparición de cada elemento, eliminando
las repeticiones adicionales. -}


{- |Documentación:

    - Nombre: eliminarrepetidosalinicio
    - Declaración: [Int] -> Bool
    - Descripción: La función recibe una lista de enteros, y devuelve
    la misma lista sin las primeras apariciones de los elementos
    repetidos. La lista evalúa si el primer elemento de la lista está
    en la cola de la lista. Si no está, agrega la cabeza y sigue
    evaluando. Si está, lo saltea y sigue evaluando (achicando la lista).
    La función termina cuando la lista no tiene más elementos.
    - Parámetros: [Int]: La lista
    - Retorna: [Int]: La misma lista sin la primera aparición de los
    elementos repetidos.
    - Necesita: contiene
    -}


eliminarrepetidosalinicio :: [Int] -> [Int]
eliminarrepetidosalinicio l | l == [] = []
                            | contiene (head l) (tail l) == True = eliminarrepetidosalinicio (tail l)
                            | otherwise = (head l) : (eliminarrepetidosalinicio (tail l))







{- Ejercicio 11: 1 maximo :: [Int] -> Int
que calcula el máximo elemento de una lista no vacía. -}


{- |Documentación:

    - Nombre: maximo
    - Declaración: [Int] -> Int
    - Descripción: La función recibe una lista de enteros, y devuelve
    el elemento más grande de la lista. La función compara la cabeza
    de la lista (n) y la cabeza de la cola de la lista (m). Si n >= m,
    quita m y sigue evaluando a n con el resto de la lista. Si m > n, 
    quita n y sigue evaluando. La función termina cuando la lista tiene
    un sólo elemento.
    - Parámetros: [Int]: La lista
    - Retorna: [Int]: El elemento más grande de la lista.
    -}


maximo :: [Int] -> Int
maximo (x:xs) | length (x:xs) == 1 = x
              | x >= head (xs) = maximo (x : (tail xs))
              | otherwise = maximo xs

maximo' :: [Int] -> Int
maximo' [x] = x
maximo' (x:y:xs) | x >= y    = maximo' (x:xs)
                 | otherwise  = maximo' (y:xs)

maximo'' :: [Int] -> Int
maximo'' [x] = x
maximo'' (x:xs)
 | x > m = x
 | otherwise       = m xs
  where m = maximo'' xs






{- Ejercicio 11: ordenar :: [Int] -> [Int]
que ordena los elementos de forma creciente. -}

{- |Documentación:

    - Nombre: ordenard
    - Declaración: [Int] -> [Int]
    - Descripción: La función recibe una lista de enteros y los
    ordena de manera decreciente. La función busca el máximo, y lo
    ubica en la cabeza, y luego lo concatena con el máximo de la cola.
    Ésto se repite hasta que la lista no tenga más elementos.
    - Parámetros: [Int]: La lista
    - Retorna: [Int]: La misma lista ordenada decrecientemente
    - Necesita: maximo y quitar
    -}

ordenard :: [Int] -> [Int]
ordenard l | l == [] = []
           | otherwise = max : ordenard (quitar max l)
           where max = maximo l







{- |Documentación:

    - Nombre: minimo
    - Declaración: [Int] -> Int
    - Descripción: Dada una lista de enteros, devuelve el elemento más
    chiquito de la lista. La funcion el primer elemento de la lista 
    (la cabeza, n1) y lo compara con el segundo elemento (la cabeza de 
    la cola, n2). Si n1 <= n2, la función quita a n2 y sigue comparando
    a n1 con el siguiente elemento. Si n2 < n1, la función quita a n1, y 
    sigue comparando a n2 con los siguientes elementos. La función termina
    cuando la lista tiene un único elemento.
    - Parámetros: [Int]: La lista
    - Retorna: Int: El elemento más chico.
    -}



minimo :: [Int] -> Int
minimo (x:xs) | length (x:xs) == 1 = x
              | x <= head (xs) = minimo (x : (tail xs))
              | otherwise = minimo xs







{- |Documentación:

    - Nombre: ordenarc
    - Declaración: [Int] -> [Int]
    - Descripción: La función recibe una lista de enteros y los
    ordena de manera creciente. La función busca el mínimo, y lo
    ubica en la cabeza, y luego lo concatena con el mínimo de la cola.
    Ésto se repite hasta que la lista no tenga más elementos.
    - Parámetros: [Int]: La lista
    - Retorna: [Int]: La misma lista ordenada crecientemente
    - Necesita: minimo y quitar
    -}


ordenarc :: [Int] -> [Int]
ordenarc l | l == [] = []
           | otherwise = min : ordenarc (quitar min l)
           where min = minimo l







{- Ejercicio 13: reverso :: [Int] -> [Int]
que dada una lista invierte su orden. -} 

{- |Documentación:

    - Nombre: reverso
    - Declaración: [Int] -> [Int]
    - Descripción: La función recibe una lista de enteros y 
    devuelve la lista invertida. La función quita el último elemento
    y lo ubica en el lugar de la cabeza. Luego, lo repite con el nuevo
    último elemento. La función se repite hasta que todos los elementos
    se mandaron al principio, es decir, queda una lista vacia.
    - Parámetros: [Int]: La lista
    - Retorna: [Int]: La lista ordenada al revés.
    - Necesita: quitarultimo
    -}


reverso :: [Int] -> [Int]
reverso l | l == [] = []
          | otherwise = (ultimo l) : reverso (quitarultimo l)








{- Ejercicio 14: concatenar :: [Int] -> [Int] -> [Int]
que devuelve la concatenación de la primera lista con la segunda.
Ejemplo concatenar [1,2,3] [4,5,6] = [1,2,3,4,5,6],
        concatenar []      [4,5,6] = [4,5,6]. 
Esta operación está en el prelude y se escribe como (++). -}



{- |Documentación:

    - Nombre: quitarultimo
    - Declaración: [Int] -> [Int]
    - Descripción: La función recibe una lista de enteros y
    le quita el último elemento
    - Parámetros: [Int]: La lista
    - Retorna: [Int]: La lista sin el último elemento
    - Necesita: ultimo y quitar
    -}



quitarultimo :: [Int] -> [Int]
quitarultimo l = quitar (ultimo l) l



{- |Documentación:

    - Nombre: concatenar
    - Declaración: [Int] -> [Int] -> [Int]
    - Descripción: La función recibe dos listas de enteros a y b, y las
    concatena. La función concatena el último elemento de a y lo agrega 
    a b, y luego lo repite. La función termina cuando ya no quedan
    más elementos en a. 
    - Parámetros: [Int]: a y b, dos listas
    - Retorna: [Int]: Las dos listas concatenadas (a:b)
    - Necesita: quitarultimo
    -}


concatenar :: [Int] -> [Int] -> [Int]
concatenar a b | a == [] = b
               | otherwise = concatenar (quitarultimo a) ((ultimo a):b) 
     






{- Ejercicio 15:  zipi :: [a] -> [b] -> [(a,b)] 
que devuelve una lista de tuplas, cada tupla contiene elementos de ambas
listas que ocurren en la misma posición. En caso que tengan distintas
longitudes, la longitud de la lista resultado es igual a la longitud de la 
lista más chica pasada por parámetro. 
Ejemplo zipi [1,2,3] ['a','b','c'] = [(1,'a'), (2,'b'), (3,'c')]
        zipi [1,2,3] ['a','b']     = [(1,'a'), (2,'b')].
 Esta operación está en el prelude y se escribe como zip. -}


{- |Documentación:

    - Nombre: elementoN
    - Declaración: Int -> [a] -> a
    - Descripción: Dado un número entero n y una lista de elementos, 
    la función devuelve el enésimo elemento de la lista. La función
    evalúa si n = 1, y devuelve el primer elemento de la lista. Si 
    n /= 1, le quita el primer elemento a la lista y achica n. La 
    función termina cuando n = 1. 
    - Parámetros: Int: n
                  [a]: la lista
    - Retorna: a: el enésimo elemento de la lista
    -}


elementoN :: Int -> [a] -> a
elementoN 0 (x:xs) = undefined
elementoN 1 (x:xs) = x
elementoN n (x:xs) = elementoN (n-1) xs






 



{- |Documentación:

    - Nombre: zipidesde
    - Declaración: Int -> Int -> [a] -> [b] -> [(a,b)]
    - Descripción: Dados dos números enteros n y m, y dos listas,
    la función devuelve un zip de las listas desde n hasta m. Ejemplo
    de zip:

     zip [1,2,3] [True, False, True] = [(1, True), (2, False), (3, True)]

     La función lo que hace es tomar el elemento n de a y el elemento n
     de b, los junta en una dupla y lo concatena con las siguientes duplas.
     La función se repite hasta que n alcanza m, es decir, hasta que n = m
    - Parámetros: Int: n y m
                  [a] y [b]: las listas
    - Retorna: [(a,b)]: Una lista cuyos elementos son duplas con el respectivo
    elemento de las listas a y b.
    - Necesita: elementoN
    -}

zipidesde :: Int -> Int -> [a] -> [b] -> [(a,b)]
zipidesde n m a b | n == m = [(elementoN n a, elementoN n b)]
                  | otherwise = (elementoN n a, elementoN n b) : (zipidesde (n+1) m a b) 








{- |Documentación:

    - Nombre: longitudmaspeque
    - Declaración: [a] -> [b] -> Int
    - Descripción: Dadas dos listas, devuelve la longitud de 
    la lista más corta. La función compara las longitudes de a y b,
    y devuelve la longitud más chiquita.
    - Parámetros: [a] y [b]: las listas a comparar.
    - Retorna: Int: La longitud de la lista más corta
    -}

longitudmaspeque:: [a] -> [b] -> Int
longitudmaspeque a b | length a <= length b = length a
                     | otherwise            = length b








{- |Documentación:

    - Nombre: zipi
    - Declaración: [a] -> [b] -> [(a,b)]
    - Descripción: Dadas dos listas a y b, la función devuelve un
    zip entre las dos listas. Ejemplo de zip:

     zip [1,2,3] [True, False, True] = [(1, True), (2, False), (3, True)]

    - Parámetros: [a] y [b]: las listas
    - Retorna: [(a,b)]: Una lista cuyos elementos son duplas con el respectivo
    elemento de las listas a y b.
    - Necesita: zipidesde y longitudmaspeque
    -}

zipi :: [a] -> [b] -> [(a,b)]
zipi a b = zipidesde 1 (longitudmaspeque a b) a b








-- no anda, devuelve las listas al revés porque arranca en n y termina en 1

zipihasta :: Int -> [a] -> [b] -> [(a,b)]
zipihasta n a b | n == 1 = [(elementoN 1 a, elementoN 1 b)]
                | otherwise = (elementoN n a, elementoN n b) : (zipihasta (n-1) a b)