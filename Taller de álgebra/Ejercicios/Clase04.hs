sumatoria :: Int -> Int
sumatoria n | n == 0 = 0
            | n > 0 = (n + sumatoria (n-1))

{-Ejercicio 1:

         n
f1 (n) = E    2^i    n e N0
         i=0                -}

f1 :: Int -> Int
f1 n | n == 0 = 1
     | n > 0 = (2^n + f1(n-1))
     | otherwise = undefined


{-Ejercicio 2:

            n
f2 (n, q) = E    q^i    n e N   y   q e R
            i=1                           -}  

f2 :: Int -> Float -> Float
f2 n q | n == 1 = q
       | n > 1 = q^n + f2 (n-1) q
       | otherwise = undefined


{-Ejercicio 3:

            2n
f3 (n, q) = E    q^i    n e N0   y   q e R
            i=0                           -}  

--No entendí muy bien xq es así

f3 :: Int -> Float -> Float
f3 n q | n == 0 = 0
       | n > 0 = (q ^ (2*n)) + (q^(2*n-1) + f3 (n-1) q)
       | otherwise = undefined

f3' :: Int -> Float -> Float
f3' n q = f2 (2*n) q


{-Ejercicio 4:

            2n
f4 (n, q) = E    q^i    n e N0   y   q e R
            i=n                           -}  


-- q^n + q^(n+1) + q^(n+2) + ... + q^(2*n-2) + q^(2*n-1) + q^2*n


f4 :: Int -> Float -> Float
f4 n q | n == 0 = 1
       | n > 0 = q^(2*n-1) + q^(2*n) - q^(n-1) + f4 (n-1) q
       | otherwise = undefined

f4' :: Int -> Float -> Float
f4' n q = (f3 n q) - (f2 (n-1) q)

{-Ejercicio 5 

eAprox :: Integer -> Float

       n
ê(n) = E 1/i!
       i=0        -}

--fromIntegral :: Int -> Float  (transforma Ints en Floats)


factorial :: Float -> Float
factorial n | n == 0 = 1
            | n > 0 = n * factorial (n-1)

superfactorial :: Integer -> Integer
superfactorial n | n == 0 = 1
                 | n > 0 = n * superfactorial (n-1)

eaprox :: Integer -> Float
eaprox n | n == 0 = 1
         | n > 0 = 1/(factorial(fromIntegral n)) + eaprox (n-1)

e :: Float
e = eaprox (10)



{- Ejercicio 6

           n    m
f6 (n,m) = E    E   (i^j)
           i=1  i=2           -}
                     
{-                                     n q   (q es la base)
    | 1^1, 1^2, 1^3, ..., 1^m | <- f2 (m 1)  (q es n de f6)
    | 2^1, 2^2, 2^3, ..., 2^m | <- f2 (m 2)
    | ...                     |
    | n^1, n^2, n^3, ..., n^m | <- f2 (m n)  (f2 sabe hacer esto!)-}


f6 :: Float -> Int -> Float
f6 n m | n == 0 = 0
       | otherwise = f2 m n + f6 (n-1) m
       | n < 0 || m < 0 = undefined



{- Ejercicio 7
Implementar una función sumapotencias q n m que sume todas
potencias de la forma q^(a+b) con 1 <= a <= n y 1 <= b <= m 

 | q^(1+1), q^(1+2), q^(1+3), q^(1+(m-1)), ..., q^(1+m) |
 | q^(2+1), q^(2+2), q^(2+3), q^(2+(m-1)), ..., q^(2+m) |
 | q^(n+1), q^(n+2), q^(n+3), q^(n+(m-1)), ..., q^(n+m) |
                                                 
        ____________^____________               ^ 
       / sumapotencias (q n m-1) \      q^(m+n) = q^m * f2 (n q)

-}


sumapotencias :: Float -> Int -> Int -> Float
sumapotencias q n m | m == 0 = 0
                    | q > 0 = (q^m)*(f2 n q) + sumapotencias q n (m-1) 



{- Ejercicio 8
Implementar una función sumaracionales n m que sume todos los
números racionales de la forma p/q con 1 <= p <= n  y 1 <= q <=m 

| 1/1, 1/2, 1/3, 1/4, ..., 1/m |
| 2/1, 2/2, 2/3, 2/4, ..., 2/m |
| ...                          |
| n/1, n/2, n/3, n/4, ..., n/m |

-}

racionales :: Float -> Float -> Float
racionales n m | n == 0 = 0
               | otherwise = (n / m) + (racionales (n-1) m)

--sumaracionales :: Float -> Float -> Float
--sumaracionales n m | m == 1 = n
--                   | otherwise = racionales n m + sumaracionales n (m-1)

sumaracionales :: Int -> Int -> Float
sumaracionales n m | m == 0 = 0
                   | otherwise = sumaracionales n (m-1) + fromIntegral(sumatoria n) / fromIntegral (m)




--Tarea

{- Ejercio  1: Implementar la siguiente función

           n
g1(i, n) = E i^j     <- n tiene q ser mayor a i
           j=i    
                        
| i^i, i^(i+1), i^(i+2), ..., i^(n-1), i^n | 

g1 3 2 = undefined              b
g1 2 3 = 2^3 + 2^2 = 8+4 = 12   b
g1 1 1 = 1^1 = 1                b
g1 2 4 = 2^4 + 2^3 + 2^2 = 28   b
g1 3 5 = 3^5 + 3^4 + 3^3        b
-}


g1 :: Integer -> Integer -> Integer
g1 i n | n == i = i^n
       | n < i = undefined
       | otherwise = i^n + g1 i (n-1)

{- Ejercio  2: Implementar la siguiente función

        n    n
g2(n) = E    E   i^j
        i=1  j=i    

|1^1, 1^(1+1), 1^(1+2), ..., 1^(1+n) | a = 1^1 * (1^1 + 1^2 + 1^3 + ... + 1^n)
|2^2, 2^(2+1), 2^(2+2), ..., 2^(2+n) | b = 2^2 * (2^1 + 2^2 + 2^3 + ... + 2^n)
|...                                 | c
|n^n, n^(n+1), n^(n+2), ..., n^(n+n) | d = n^n * (n^1 + n^2 + n^3 + ... + n^n)


|1^1 + 1^2 + 1^3 + ... 1^n    --> g1 1 n
|      2^2 + 2^3 + ... 2^n    --> g2 2 n
|          + 3^3 + ... 3^n    --> g2 3 n
|                  ... n^n    --> g2 n n     <--- ES ASI!!!

| 2^1
| 3^1 + 3^2
| 4^1 + 4^2 + 4^3
| n^1 + n^2 + n^3 + ... n^(n-1)

ej: g2 3 = 3^3 + 2^2 + 2^3 + 1^1 + 1^2 + 1^3
-}


aux :: Integer -> Integer -> Integer
aux b p | p == 0 = 0
        | otherwise = b ^ p + aux b (p-1)

-- aux 3 4 = 3^4 + 3^3 + 3^2 + 3^1

aux2 :: Integer -> Integer -> Integer
aux2 b p | p < b = undefined
         | p == b = p ^ b
         | otherwise = b ^ p + aux2 b (p-1)

-- aux 3 4 = 3^4 + 3^3 = 108
-- aux 4 4 = 4^4

g2 :: Integer -> Integer
g2 n | n == 0 = 0
     | otherwise = n^n + aux2 (n-1) n 

-- g2 4 = 396
-- g2 3 = 42
-- g2 1 = 1


g2' :: Integer -> Integer -> Integer
g2' n m | n == 1 = m
        | otherwise = n^n + aux2 (n-1) m 


-- FALTA TERMINAR!!


{- Ejercicio 3: Implementar la siguiente función

         n
g3 (n) = E    2^i 
         i=1 
         i es par

Ej: g3 11 = 2^2 + 2^4 + 2^6 + 2^8 + 2^10  -}

espar :: Integer -> Bool
espar n | mod n 2 == 0 = True
        | otherwise = False

g3 :: Integer -> Integer
g3 n | n == 0 = 0
     | mod n 2 /= 0 = g3 (n-1)
     | otherwise = 2^n + g3 (n-2)

g3x :: Integer -> Integer
g3x n | n == 0 = 0
      | espar n == False = g3 (n-1)
      | otherwise = 2^n + g3 (n-2)


{- Ejercio 4: Implementar una función que dado un n, sume todos 
los números naturales menores o iguales que n que tengan todos
los dígitos iguales -}

--Importo soniguales, que necesita digitounidades y digitodecenas

soniguales :: Int -> Bool
soniguales n | n < 0 = soniguales (abs n) 
             | n < 10 = True
             | digitounidades n == digitodecenas n = soniguales (div n 10)
             | otherwise = False

digitounidades :: Int -> Int
digitounidades n | n < 0 = digitounidades (abs n)
                 | n < 10 = n
                 | otherwise = (mod n 10)

digitodecenas :: Int -> Int 
digitodecenas n | n < 0 = digitodecenas (abs n)
                | n < 10 = undefined
                | otherwise = mod (div n 10) 10

g4 :: Int -> Int
g4 n | n == 0 = 0 
     | soniguales n == True = n + g4 (n-1)
     | otherwise = g4 (n-1)

bestnumero :: Int
bestnumero = 54