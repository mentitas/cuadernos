identidad :: a -> a
identidad x = x

primero :: a -> b -> a
primero x y = x

segundo :: a -> b -> b
segundo x y = y


{-Haskell me dice que hay que declararlo así, 
poniendo que d (osea 5) es un número -}
constantecinco :: Num d => a -> b -> c -> d
constantecinco x y z = 5

--cómo lo hubiese declarado yo
constantecinco' :: a -> b -> c -> Int 
constantecinco' x y z = 5

--si son del mismo tipo, devuelve True; sino devuelve error
mismotipo :: t -> t -> Bool
mismotipo x y = True

-- Es Num porque se multiplica xd
-- Puedo poner Num t sin parentesis también
triple :: (Num t) => t -> t
triple x = 3*x

{-Es Ord porque x e y tienen que tener un orden para compararlos
osea, poder hacerles mayor, menor o igual-}
maximo :: (Ord a) => a -> a -> a
maximo x y | x >= y = x
           | otherwise = y

-- es Eq porque se tiene que ver si x e y son iguales o distintos
distintos :: (Eq a) => a -> a -> Bool
distintos x y = x /= y


{- Num t porque en b y c se aplican multiplicacion y potencia
   Ord t porque después d tiene que compararse con igual o mayor-}
cantidaddesoluciones :: (Num t, Ord t) => t -> t -> Int
cantidaddesoluciones b c | d > 0 = 2
                         | d == 0 = 1
                         | otherwise = 0
                         where d = b^2 - 4*c

--Floating t porque se le aplica **, Ord t porque se compara
f1 :: (Floating t, Ord t) => t -> t -> t -> Bool
f1 x y z = x**y + z <= x + y**z

--Floating porque usa / y sqrt
f2 :: (Floating t) => t -> t -> t
f2 x y = (sqrt x) / (sqrt y)

--No anda: sqrt devuelve float, y div solo anda con enteros
f3 :: (Floating t, Integral t) => t -> t -> t
f3 x y = div (sqrt x) (sqrt y)


-- devuelve siempre z
-- a es Flotante porque usa **, es Eq porque usa ==
-- z es de cualquier tipo
f4 :: (Floating a, Eq a) => a -> a -> b -> b
f4 x y z | x == y = z
         | x ** y == y = z
         | otherwise = z
{-No puede devolver tipos distintos, voy a declarar que 
x y z son del mismo tipo. Sino por ejemplo 1 1 True me 
devuelve error. -}
f5 :: (Floating a, Eq a) => a -> a -> a -> a
f5 x y z | x == y = z
         | x ** y == y = x
         | otherwise = y

sumavectores :: (Num a, Num b) => (a, b) -> (a, b) -> (a, b)
sumavectores (u1, u2) (v1, v2) = ( u1 + v1, u2 + v2)


--El _ indica que es False con cualquier otra dupla
--esorigen1 es igual a esorigen2
esorigen1 :: (Float, Float) -> Bool
esorigen1 (0, 0) = True
esorigen1 _ = False

esorigen2 :: (Float, Float) -> Bool
esorigen2 (0, 0) = True
esorigen2 (_, _) = False

angulocero :: (Float, Float) -> Bool
angulocero (_, 0) = True
angulocero (_, _) = False

angulo45 :: (Float, Float) -> Bool
angulo45 (x, y) = x == y

{-esto está mal porq no puedo usar la x dos veces
angulo45mal :: (Float, Float) -> Bool
angulo45mal (x, x) = True
angulo45mal (_, _) = False           -}

normavectorial1 :: Float -> Float -> Float
normavectorial1 x y = sqrt (x^2 + y^2)

normavectorial2 :: (Float, Float) -> Float
normavectorial2 (x, y) = sqrt (x^2 + y^2)

sumanormal1 :: (Float, Float) -> (Float, Float) -> Float
sumanormal1 (v1, v2) (u1, u2) = normavectorial1 (fst s) (snd s) 
 where s = sumavectores (v1, v2) (u1, u2)

sumanormal2 :: (Float, Float) -> (Float, Float) -> Float
sumanormal2 v u = normavectorial2 (sumavectores v u)






--ejercicios de tarea

{-estanRelacionados: dados dos números reales, decide si 
están relacionados considerando la relación de equivalencia 
en R cuyas clases de equivalencia son:
(−∞, 3], (3, 7] y (7, ∞). -}

--no se hacerlo




--prodInt: calcula el producto interno entre dos vectores de R2
prodInt :: (Num a, Num a) => (a, a) -> (a, a) -> a
prodInt (v1, v2) (u1, u2) = (v1*u1 + v2*u2)




{- todoMenor: dados dos vectores de R2, decide si es cierto 
que cada coordenada del primer vector es menor a la coordenada 
correspondiente del segundo vector. -}

todoMenor1 :: (Ord a) => (a, a) -> (a, a) -> Bool
todoMenor1 (v1, v2) (u1, u2) = (v1 < u1 && v2 < u2)

{- Según Haskell, los tipos se declararían así:
todoMenor :: (Ord a1, Ord a2) => (a1, a2) -> (a1, a2) -> Bool -}




-- distanciaPuntos: calcula la distancia entre dos puntos de R2
-- usé dos funciones auxiliares: "normavectorial2 y "restavectores"

distanciaPuntos :: (Float, Float) -> (Float, Float) -> Float
distanciaPuntos v1 v2 = normavectorial2 (restavectores v2 v1)

restavectores :: (Float, Float) -> (Float, Float) -> (Float, Float)
restavectores (v1, v2) (u1, u2) = (v1 - u1, v2 - u2)




{- sumaTerna: dada una terna de enteros, calcula la suma 
de sus tres elementos. -}
{-Los tres elementos tienen el mismo tipo porque tienen que
sumarse entre si -}
sumaTerna :: (Num a) => (a, a, a) -> a
sumaTerna (x, y, z) = x + y + z




{-  posicPrimerPar: dada una terna de enteros, devuelve la 
posici´on del primer número par si es que hay alguno, y 
devuelve 4 si son todos impares -}
-- usé la función auxiliar "esPar"

posicPrimerPar :: (Int, Int, Int) -> Int
posicPrimerPar (x, y, z) | esPar x == True = 1
                         | esPar y == True = 2
                         | esPar z == True = 3
                         | otherwise = 4

esPar :: Int -> Bool
esPar n = mod n 2 == 0 





{- crearPar :: a -> b -> (a, b): crea un par a partir de sus 
dos componentes dadas por separado (debe funcionar para
elementos de cualquier tipo). -}

crearPar :: a -> b -> (a, b)
crearPar a b = (a, b)




{- invertir :: (a, b) -> (b, a): invierte los elementos del 
par pasado como parámetro (debe funcionar para elementos de 
cualquier tipo). -}

invertir :: (a, b) -> (b, a)
invertir (x, y) = (y, x)