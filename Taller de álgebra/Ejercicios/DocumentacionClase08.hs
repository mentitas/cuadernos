DocumentacionClase08.hs

{- |Documentación:

    - Nombre: combinatorio 
    - Declaración:Int -> Int -> Int
    - Descripción: Dados dos números enteros n y k, la función
    calcula el combinatorio de n k.
    - Parámetros: Int: n y k
    - Retorna: Int: el resultado del combinatorio
    - Necesita: factorial
    -}
    