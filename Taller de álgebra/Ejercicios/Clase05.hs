import FuncionesClase03 -- Lo importo porque voy a necesitar usar factorial 
import FuncionesClase01 -- Lo importo porque voy a necesitar usar espar 
      

{- Ejercicio 1: sumadivisoreshasta :: Int -> Int -> Int
que devuelve la suma de los divisores de un número hasta cierto punto -}

{- Documentación (no anda como documentación tho)

 - Nombre: sumadivisoreshasta
 - Declaración: Int -> Int -> Int 
 - Descripción: Dados dos números enteros n y m, devuelve la suma de
  todos los divisores de n hasta m. Por ejemplo:
  sumadivisoreshasta 5 6 = 1 + 2 + 3
 - Parámetros: Int: n (número) y m (hasta dónde se suman los divisores
  del número)
 - Retorna: La suma de los divisores de n hasta m -}

sumadivisoreshasta :: Int -> Int -> Int
sumadivisoreshasta n m | m == 1 = 1
                       | mod n m == 0 = m + sumadivisoreshasta n (m-1)
                       | otherwise = sumadivisoreshasta n (m-1)

{- Ejercicio 2: sumadivisores :: Int -> Int 
que calcule la suma de los divisores un entero positivo. -}

{-| Documentación
 
  - Nombre: sumadivisores
  - Declaración: Int -> Int
  - Descripción: Dados un número entero positivo, devuelve la suma de
  todos sus divisores
  - Parámetros: Int: n
  - Retorna: Int: La suma de todos los divisores de n -}

sumadivisores :: Int -> Int
sumadivisores n = sumadivisoreshasta n n


{- Ejercicio 3: menordivisor :: Int -> Int 
que calcule el menor divisor (mayor que 1) de un natural n -}

{- |Documentación

  - Nombre: menordivisordesde
  - Declaración: Int -> Int -> Int
  - Descripción: Dados dos números enteros n y m, se fija si m es 
  divisor de n. Si es divisor, devuelve m, sino prueba con (m+1).
  Termina cuando n = m, es decir, cuando buscamos todos los divisores
  desde m hasta m
  - Parámetros: Int: n (número) y m (desde dónde buscamos divisores)
  - Retorna: Int: el menor divisor -}

menordivisordesde :: Int -> Int -> Int
menordivisordesde n m | n == m = m
                      | mod n m == 0 = m
                      | otherwise = menordivisordesde n (m + 1)


{- |Documentación

  - Nombre: menordivisor
  - Declaración: Int -> Int
  - Descripción: Dado un número entero n, empieza a buscar divisores 
  de n desde 1 hasta n, y devuelve el primer divisor que encuentra.
  - Parámetros: Int: n (el número)
  - Retorna: Int: el menor divisor 
  - Necesita: menordivisordesde -}

menordivisor :: Int -> Int
menordivisor n = menordivisordesde n 2 

{- Ejercicio 4:  Implementar la función esprimo :: Int -> Bool.-}

{- |Documentación

  - Nombre: esprimo
  - Declaración: Int -> Bool
  - Descripción: Dado un número entero n, devuelve si es o no es primo.
  Usa la funcion menordivisor. Busca el menor divisor de n, y si devuelve
  n, es porque no tiene otros divisores entre el 1 y el n, es decir,
  es primo.
  - Parámetros: Int: n (el número)
  - Retorna: Bool: si es o no es primo
  - Necesita: menordivisor (menordivisor necesita menordivisordesde) -}

esprimo :: Int -> Bool
esprimo n | menordivisor n == n = True
          | otherwise = False


{- Ejercicio 5: Implementar la función nesimoprimo :: Int -> Int que 
devuelve el n-esimo primo (n ≥ 1, el primer primo es el 2, el segundo 
es el 3, el tercero es el 5, etc.) -}

{- |Documentación

  - Nombre: nesimoprimo
  - Declaración: Int -> Int
  - Descripción: Dado un número entero, busca el siguiente primo al enésimo primo anterior.
   Ej: si el tercer primo es 5, el cuarto primo es el primo siguiente a 5, es decir 7.
  - Parámetros: Int: n
  - Retorna: Int: El enésimo primo
  - Necesita: primosiguiente (que necesita esprimo)
  -}

nesimoprimo :: Int -> Int
nesimoprimo 1 = 2 
nesimoprimo n = primosiguiente (nesimoprimo (n-1))

nesimoprimo' :: Int -> Int
nesimoprimo' 1 = 2 
nesimoprimo' n = primosiguiente (nesimoprimo' (n-1))

{- nesimoprimo 4 = primosiguiente (nesimoprimo 3)
                 = primosiguiente (primosiguiente (nesimoprimo 2))
                 = primosiguiente (primosiguiente (primosiguiente (nesimoprimo 1)))
                 = primosiguiente (primosiguiente (primosiguiente 2))
                 = primosiguiente (primosiguiente 3)
                 = primosiguiente 5
                 = 7 -}


{- |Documentación

  - Nombre: primoanterior
  - Declaración: Int -> Int
  - Descripción: Dado un número entero n, devuelve el primer primo anterior a n. 
    Si n es primo, devuelve el primo anterior.
  - Parámetros: Int: n (desde dónde busca el primo (restando, acercandose a 0))
  - Retorna: Int: el primer primo anterior a n
  - Necesita: esprimo
  -}

-- Dados un número, devuelve el primo anterior
primoanterior :: Int -> Int
primoanterior n | esprimo (n-1) == True = (n-1)
                | otherwise = primoanterior (n-1)

{- |Documentación

  - Nombre: primosiguiente
  - Declaración: Int -> Int
  - Descripción: Dado un número entero n, devuelve el primer primo siguiente a n
    Si n es primo, devuelve el siguiente primo
  - Parámetros: Int: n (a partir de dónde busca el primo (sumando)
  - Retorna: Int: el primer primo siguiente a n
  - Necesita: esprimo
  -}

-- Dado un número, devuelve el siguiente primo al número
primosiguiente :: Int -> Int
primosiguiente n | esprimo (n+1) == True = (n+1)
                 | otherwise = primosiguiente (n+1)



{- Ejercicio 6: menorfactdesde :: Int -> Int 
que dado m ≥ 1 encuentra el mínimo n ≥ m tal que n = k! para algún k.-}
-- Dado un número, encuentra el primer factorial mayor al número

-- 1! 2! 3!  4!  5! 
-- 1, 2, 6, 24, 120

-- n = numero m= factorial
-- El primer factorial mayor a n

{- |Documentación

  - Nombre: menorfactdesde
  - Declaración: Int -> Int -> Int
  - Descripción: Dados dos números enteros n y m, calcula el 
  primer factorial mayor o igual a n desde m!.
  - Parámetros: Int: n (el número a comparar) y m (desde donde se
  busca el factorial mayor a n)
  - Retorna: Int: el primer factorial mayor a n
  - Necesita: factorial (importado en FuncionesClase03.hs)
  -}

menorfactdesde :: Int -> Int -> Int
menorfactdesde n m | factorial m >= n = factorial m
                   | otherwise = menorfactdesde n (m+1)


{- |Documentación

  - Nombre: menorfact
  - Declaración: Int -> Int
  - Descripción: Dado un número entero n, calcula el primer factorial
  mayor o igual a n desde el 1.
  - Parámetros: Int: n (el número a comparar)
  - Retorna: Int: el primer factorial mayor o igual a n
  - Necesita: menorfactdesde (que necesita factorial)
  -}

menorfact :: Int -> Int
menorfact n = menorfactdesde n 0



{- Ejercicio 7: mayorFactHasta :: Int -> Int 
que dado m ≥ 1 encuentra el máximo n ≤ m tal que n = k! para algún k. -}

{- |Documentación

  - Nombre: mayorfactdesde
  - Declaración: Int -> Int -> Int
  - Descripción: Dados dos números enteros n y m, calcula el primer
  factorial menor o igual a n desde m!
  - Parámetros: Int: n (el número a comparar) y m (desde donde
  empezamos a buscar factoriales)
  - Retorna: Int: el primer factorial menor o igual a n
  - Necesita: factorial (importado en FuncionesClase03.hs)
  -}

mayorfactdesde :: Int -> Int -> Int
mayorfactdesde n m | factorial m > n = factorial (m-1)
                   | otherwise = mayorfactdesde n (m+1)
--si m==1, calcula el mayorfact


{- |Documentación

  - Nombre: mayorfact
  - Declaración: Int -> Int
  - Descripción: Dado un número entero n, calcula el primer factorial
  menor o igual a n desde el 1.
  - Parámetros: Int: n (el número a comparar)
  - Retorna: Int: el primer factorial menor o igual a n
  - Necesita: mayorfactdesde (que necesita factorial)
  -}

mayorfact :: Int -> Int
mayorfact n = mayorfactdesde n 0




{- Ejercicio 8: Implementar esFact :: Int -> Bool 
que dado n ≥ 0 decide si existe un número entero k ≥ 0 tal que n = k!-}


{- |Documentación

  - Nombre: esfact
  - Declaración: Int -> Bool
  - Descripción: Dado un número entero n, calcula si primer
  factorial menor o igual a n es n, y devuelve true o false
  - Parámetros: Int: n (el número)
  - Retorna: Bool: si es o no es factorial
  - Necesita: menorfact (que necesita menorfactdesde (que
  necesita factorial))
  -}

esfact :: Int -> Bool
esfact n | menorfact n == n = True
         | otherwise = False


{- Ejercicio 9: esFibonacci :: Int -> Bool 
que dado un número entero n ≥ 0 decide si n es un número 
de Fobonacci.-}
 
 {- |Documentación

  - Nombre: esfibdesde
  - Declaración: Int -> Bool
  - Descripción: Dados dos números enteros n y m, averigua si
  n es un número de fibonacci buscando a partir del emésimo
  fibonacci. La función retorna true si encuentra que
  para algún número mayor o igual a m, el emésimo fibonacci es 
  igual a n. La función retorna False cuando los fibonaccis son 
  mayores estrictos a n, es decir, no encontró un fib igual a n.
  - Parámetros: Int: n (el número a comparar), y m (desde dónde
  busco fibonaccis)
  - Retorna: Bool: si es o no es fibonacci
  - Necesita: fib (importado con FuncionesClase03.hs)
  -}

 --decide si n es fib buscando desde m
esfibdesde :: Int -> Int -> Bool
esfibdesde n m | m > n = False
               | fib m == n = True
               | otherwise = esfibdesde n (m+1)


{- |Documentación

  - Nombre: esfibhasta
  - Declaración: Int -> Bool
  - Descripción: Dados dos números enteros n y m, la función
  averigua si n es un número de fib buscando desde m hasta el 0. 
  Cuando la función encuentra que algún fib m = n, devuelve true.
  Cuando se pasa de largo, es decir, cuando fib m < n, devuelve
  false.
  - Parámetros: Int: n (el número a comparar), y m (desde dónde
  busco fibonaccis (restando, acercándome al 0.))
  - Retorna: Bool: si es o no es fibonacci
  - Necesita: fib (importado con FuncionesClase03.hs)
  -}

--decide si es fib buscando desde m hasta el 0
esfibhasta :: Int -> Int -> Bool
esfibhasta n m | m < n = False
               | fib m == n = True
               | otherwise = esfibhasta n (m-1)



{- |Documentación

  - Nombre: esfib
  - Declaración: Int -> Bool
  - Descripción: Dado un número entero n, averigua si n es un 
  número de fibonacci, comparando todos los números de fibonacci
  desde el cero hasta que encuentre uno igual o mayor. Si
  encuentra uno igual, devuelve true, si se pasa de largo,
  devuelve false
  - Parámetros: Int: n (el número a comparar)
  - Retorna: Bool: si es o no es fibonacci
  - Necesita: esfibdesde (que necesita fib)
  -}

esfib :: Int -> Bool             ---REVISAR
esfib n = esfibdesde n 1

{- |Documentación

  - Nombre: esfibtrucha
  - Declaración: Int -> Bool
  - Descripción: Dado un número entero n, averigua si n es un 
  número de fibonacci, comparando todos los números de fibonacci
  desde el n hasta que encuentre uno igual o menor. Si
  encuentra uno igual, devuelve true, si se pasa de largo,
  devuelve false.
  - Observación: La función no está muy buena porque cuando 
  ingresás números grandes (por ejemplo 120), tiene que calcular
  fib 120, fib 119, etc etc, hasta que encuentre o no si es fib.
  - Parámetros: Int: n (el número a comparar)
  - Retorna: Bool: si es o no es fibonacci
  - Necesita: esfibhasta (que necesita fib)
  -}

esfibtrucha :: Int -> Bool
esfibtrucha n = esfibhasta n n


{- Ejercicio 10: essumainicialdeprimos :: Int -> Bool 
que dado un número entero n ≥ 0 decide si n es igual a la suma 
de los m primeros números primos, para algún m. -}

{- |Documentación

  - Nombre: sumaprimoshasta
  - Declaración: Int -> Int
  - Descripción: Dado un número entero n, suma todos los números primos desde
  el enésimo primo hasta el 2.
  - Parámetros: Int: n (hasta cuál primo hay que sumar)
  - Retorna: Int: La suma de los primeros primos hasta el enésimo primo
  - Necesita: nesimoprimo
  -}

sumaprimoshasta :: Int -> Int
sumaprimoshasta n | n == 1 = 2
                  | otherwise = nesimoprimo (n) + sumaprimoshasta (n-1) 

{- |Documentación

  - Nombre: essumainicialdeprimosdesde
  - Declaración: Int -> Int -> Bool
  - Descripción: Dados dos números enteros n y m, averigua n es la suma de los primeros
  m primos. Si la suma de los primeros m primos es menor a n, prueba con (m+1). Si resulta
  que son iguales, devuelve True; si se pasa de largo, devuelve False
  - Parámetros: Int: n (el número a comparar) y m (hasta qué primo hay que sumar)
  - Retorna: Bool: si es o no es suma de los primeros m números primos
  - Necesita: sumaprimoshasta (que necesita nesimoprimo)
  -}

essumainicialdeprimosdesde :: Int -> Int -> Bool
essumainicialdeprimosdesde n m | sumaprimoshasta m > n  = False
                               | sumaprimoshasta m == n = True
                               | otherwise = essumainicialdeprimosdesde n (m+1)

{- |Documentación

  - Nombre: essumainicialdeprimos
  - Declaración: Int -> Bool
  - Descripción: Dado un número entero n, devuelve si es o no la suma de los primeros
  m números primos para algún m. Compara si es la suma de los primeros m números primos
  desde m=1 hasta que la suma es mayor o igual a n. Si es igual devuelve True, si es 
  mayor devuelve False.
  - Parámetros: Int: n (el número a comparar)
  - Retorna: Bool: si es o no es suma de los primeros m números primos
  - Necesita: sumainicialdeprimosdesde (que necesita sumaprimoshasta (que necesita 
  nesimoprimo))
  -}

essumainicialdeprimos :: Int -> Bool
essumainicialdeprimos n = essumainicialdeprimosdesde n 1


{- Ejercicio 11: tomaValorMax :: Int -> Int -> Int 
que dado un número entero n1 ≥ 1 y un n2 ≥ n1 devuelve algún m entre n1 y n2 tal que
sumaDivisores(m) = max {sumaDivisores(i) | n1 ≤ i ≤ n2}  -}

--Dados dos números, devuelva el valor entre esos números que tenga la sumadivisores más grande. 


--compara a y b, y devuelve el más grande

comparar :: Int -> Int -> Int
comparar a b | a >= b = a
             | otherwise = b

{- |Documentación

  - Nombre: maxsumadivisores
  - Declaración: Int -> Int -> Int
  - Descripción: Dados dos números enteros a y b, devuelve cuál de los dos
  tiene el sumadivisores más grande.
  - Parámetros: Int: a y b (los números a comparar)
  - Retorna: Int: el valor con mayor sumadivisores
  - Necesita: sumadivisores
  -}

maxsumadivisores :: Int -> Int -> Int
maxsumadivisores a b | sumadivisores a >= sumadivisores b = a
                     | otherwise = b

{- |Documentación

  - Nombre: tomavalormax
  - Declaración: Int -> Int -> Int
  - Descripción: Dados dos números enteros a y b, devuelve un valor m entre
  a y b (a <= m <= b) tal que m tenga el sumadivisores más grande.
  - Parámetros: Int: a y b (los números a comparar)
  - Retorna: Int: el valor con mayor sumadivisores
  - Necesita: maxsumadivisores.
  -}

tomavalormax :: Int -> Int -> Int
tomavalormax a b | a == b = a
                 | otherwise = maxsumadivisores (tomavalormax a (b-1)) b

{- Cómo funciona:
tomavalormax 6 9 = maxsumadivisores (tomavalormax 6 8) 9
tomavalormax 6 9 = maxsumadivisores (maxsumadivisores (tomavalormax 6 7) 8) 9
tomavalormax 6 9 = maxsumadivisores (maxsumadivisores (maxsumadivisores (tomavalormax 6 6) 7) 8) 9
tomavalormax 6 9 = maxsumadivisores (maxsumadivisores (maxsumadivisores 6 7) 8) 9
tomavalormax 6 9 = maxsumadivisores (maxsumadivisores 6 8) 9
tomavalormax 6 9 = maxsumadivisores 8 9
tomavalormax 6 9 = 8
-}

{-
n = 1 2 3 4 5  6 7  8  9 10
sd= 1 3 4 7 6 12 8 15 13 18  -}

{- Ejercicio 12: tomaValorMin :: Int -> Int -> Int 
que dado un número entero n1 ≥ 1 y un n2 ≥ n1 devuelve algún m
entre n1 y n2 tal que sumaDivisores(m) = min{sumaDivisores(i) | n1 ≤ i ≤ n2} -}

{- |Documentación

  - Nombre: minsumadivisores
  - Declaración: Int -> Int -> Int
  - Descripción: Dados dos números enteros a y b, devuelve cuál de los dos
  tiene el sumadivisores más chico.
  - Parámetros: Int: a y b (los números a comparar)
  - Retorna: Int: el valor con menor sumadivisores
  - Necesita: sumadivisores
  -}

minsumadivisores :: Int -> Int -> Int
minsumadivisores a b | sumadivisores a <= sumadivisores b = a
                     | otherwise = b


{- |Documentación

  - Nombre: tomavalormin
  - Declaración: Int -> Int -> Int
  - Descripción: Dados dos números enteros a y b, devuelve un valor m entre
  a y b (a <= m <= b) tal que m tenga el sumadivisores más chico.
  - Parámetros: Int: a y b (los números a comparar)
  - Retorna: Int: el valor con menor sumadivisores
  - Necesita: minsumadivisores.
  -}

tomavalormin :: Int -> Int -> Int
tomavalormin a b | a == b = a
                 | otherwise = minsumadivisores (tomavalormin a (b-1)) b


{- Cómo funciona:
tomavalormin 6 9 = minsumadivisores (tomavalormin 6 8) 9
tomavalormin 6 9 = minsumadivisores (minsumadivisores (tomavalormin 6 7) 8) 9
tomavalormin 6 9 = minsumadivisores (minsumadivisores (minsumadivisores (tomavalormin 6 6) 7) 8) 9
tomavalormin 6 9 = minsumadivisores (minsumadivisores (minsumadivisores 6 7) 8) 9
tomavalormin 6 9 = minsumadivisores (minsumadivisores 7 8) 9
tomavalormin 6 9 = minsumadivisores 7 9
tomavalormin 6 9 = 7
-}


{- Ejercicio 13: essumadedosprimos :: Int -> Bool 
que, dado un número natural n, determine si puede escribirse como suma de dos 
números primos.-}

{- |Documentación

  - Nombre: sumarprimosnm
  - Declaración: Int -> Int -> Int
  - Descripción: Dados dos números enteros n y m, devuelve la 
  suma del enésimo y el emésimo primo.
  - Parámetros: Int: n y m
  - Retorna: Int: la suma del enésimo y el emésimo primo.
  - Necesita: nesimoprimo.
  -}

sumarprimosnm :: Int -> Int -> Int
sumarprimosnm n m = nesimoprimo n + nesimoprimo m


{- |Documentación

  - Nombre: essumadeprimosmuevoMdesde
  - Declaración: Int -> Int -> Int -> Bool
  - Descripción: Dados tres números enteros p, n y m, evualúa
  si p es la suma entre el enésimo primo y algún primo b, siendo
  b>=m. n es un número fijo. El primo b se empieza a buscar en m,
  y va aumentando de a uno hasta que la suma de los primos n y b
  sea igual o mayor a p. Si la suma es igual, devuelve True; sino 
  devuelve False.
  - Parámetros: Int: p (número a evaluar), n (es fijo) y m (desde dónde 
  parte b)
  - Retorna: Bool: Si es suma del primo n y el primo b.
  - Necesita: sumarprimosnm.
  -}

-- evalúa si p es suma del primo n y algún primo a (a>=m)

essumadeprimosmuevoMdesde :: Int -> Int -> Int -> Bool
essumadeprimosmuevoMdesde p n m | sumarprimosnm n m > p  = False
                                | sumarprimosnm n m == p = True
                                | otherwise = essumadeprimosmuevoMdesde p n (m+1)

{- |Documentación

  - Nombre: essumadeprimosmuevoMyNdesde
  - Declaración: Int -> Int -> Int -> Bool
  - Descripción: Dados tres números enteros p, n y m, evualúa
  si p es la suma entre algún primo a y algún primo b, siendo
  a>=n y b>=m. El primo a se empieza a buscar en n, y se lo evalúa
  con todos los valores posibles que puedan cumplir con la suma
  usando la función essumadeprimosmuevoMdesde. Si la función auxiliar
  retorna True, devuelvo True; y sino sigo intentando aumentando n hasta 
  que la suma de los primos a y b sea mayor a p, entonces retorno False.
  - Parámetros: Int: p (número a evaluar), n (desde dónde parte a) y m
  (desde dónde parte b).
  - Retorna: Bool: Si es suma del primo a y el primo b.
  - Necesita: essumadeprimosmuevoMdesde.S
  -}

-- evalúa si p es suma de algún primo a (a>=n) y algún primo b (b>=m) 

essumadeprimosmuevoMyNdesde :: Int -> Int -> Int -> Bool
essumadeprimosmuevoMyNdesde p n m | sumarprimosnm n m > p  = False
                                  | essumadeprimosmuevoMdesde p n m == True = True
                                  | otherwise = essumadeprimosmuevoMyNdesde p (n+1) m


{- |Documentación

  - Nombre: essumadedosprimos
  - Declaración: Int -> Bool
  - Descripción: Dados un número entero p, la función determina si 
  p es la suma entre un primo a y un primo b, (a>=1 y b>=1).  
  - Retorna: Bool: Si es suma del primo a y el primo b.
  - Necesita: essumadeprimosmuevoMyNdesdeS
  -}

-- evalúa si p es suma de algún primo a (a>=1) y algún primo b (b>=1)

essumadedosprimos :: Int -> Bool
essumadedosprimos p = essumadeprimosmuevoMyNdesde p 1 1


-------------------------otra cosa--------------------------------
-- Misma función de recíen,
-- pero devuelve si p es suma de qué n m números primos

quienesM :: Int -> Int -> Int -> Int
quienesM p n m | sumarprimosnm n m > p  = 0
               | sumarprimosnm n m == p =  m
               | otherwise = quienesM p n (m+1)

quienesN :: Int -> Int -> Int -> (Int, Int)
quienesN p n m | sumarprimosnm n m > p  = (0, 0)
               | quienesM p n m /= 0    = (n, quienesM p n m)
               | otherwise              = quienesN p (n+1) m


--solo funciona con p > 0. Si p es primo devuelve (0,0)
sumadequienesprimos :: Int -> (Int, Int)
sumadequienesprimos p = quienesN p 1 1




{- Ejercicio 14: Conjetura de Christian Goldbach, 1742:
todo n´umero par mayor que 2 puede escribirse como suma de dos números
primos. Escribir una función que pruebe la conjetura hasta un cierto punto. 
goldbach :: Int -> Bool (hasta al menos 4*10^18 debería ser cierto) -}

{- |Documentación

  - Nombre: goldbach
  - Declaración: Int -> Bool
  - Descripción: Dados un número entero p, la función determina si 
  todos los números pares hasta p son suma de dos primos. Si se cumple,
  devuelve True; si no se cumple o si p es impar, devuelve False.
  - Retorna: Bool: Si todos los números pares hasta p son o no
  son suma de dos primos. Si p es impar también devuelve False.
    - Necesita: essumadedosprimos
  -}


--aka goldbachhasta
goldbach :: Int -> Bool
goldbach n | n == 0 = True
           | n == 1 = False
           | essumadedosprimos n == True = goldbach (n-2)
           | otherwise = False

--------otras cosas ------------------------------------------------

t :: Int
t = 4*10^18

goldbach' :: Bool
goldbach' = goldbachdesde 2

goldbachdesde :: Int -> Bool
goldbachdesde n | n == t = True
                | essumadedosprimos n == True = goldbachdesde (n+2)
                | otherwise = False




{- Ejercicio 15: primosgem :: Int -> Int 
Los números naturales a y b forman un par de primos 
gemelos si b = a + 2 y tanto a como b son primos. Implementar 
primosgem, que devuelve la cantidad de pares de primos gemelos (a; b) 
que verifican b ≤ n. Por ejemplo: primosGem 5 = 1 (porque 3 y 5 es un 
par de primos gemelos) primosGem 14 = 3 (porque 3 y 5, 5 y 7, y 11 y 13 
son tres pares de primos gemelos) -}


{- |Documentación

  - Nombre: primosgemv
  - Declaración: Int -> Int -> Int
  - Descripción: Dados dos números enteros n y v, la función 
  devuelve cuántos pares de primos gemelos hay entre el 1 y n.
  v es el puntaje, aumenta un valor cuando encuentra un par de
  primos gemelos. Evalúa si n y (n-2) son primos; y si devuelve
  True, aumenta v y sigue evaluando con (n-1). Si devuelve False,
  sigue evaluando hasta que n == 3, porque en ese caso n-2=1.
  - Parámetros: n (hasta dónde se buscan los pares de primos 
  gemelos) y v (el puntaje, debería arrancar siendo cero)
  - Retorna: Int: v (el puntaje, la cantidad de pares encontrados)
  son suma de dos primos. Si p es impar también devuelve False.
    - Necesita: esprimo
  -}

--v es el puntaje. Arranca en 0 y si encuentra un par de primos gemelos, aumenta en uno.
--n termina en 3 porque 3-2 = 1, y si n<3 no hay primos menores que 1.

primosgemv :: Int -> Int -> Int
primosgemv n v | n == 3 = v 
               | esprimo n == True && esprimo (n-2) == True = primosgemv (n-1) (v+1)
               | otherwise = primosgemv (n-1) v


{- |Documentación

  - Nombre: primosgem
  - Declaración: Int -> Int
  - Descripción: Dado un número entero n, la función evalúa
  cuántos pares de primos gemelos hay entre 1 y n. 
  - Parámetros: n (hasta dónde se buscan los pares de primos 
  gemelos)
  - Retorna: Int: el puntaje, la cantidad de pares encontrados
  - Necesita: primosgemv
  -}

primosgem :: Int -> Int
primosgem n = primosgemv n 0



--Primos= 2 3 5 7 11 13 17 19 23
-- gem= (3,5), (5,7), (11, 13), (17, 19)


{- Ejercicio 16: proxprimosgem :: Int -> (Int,Int)
Conjetura de los primos gemelos: Existen infinitos pares de primos 
gemelos. Implementar la funciónn proxprimosgem que dado n 
devuelve el primer par de gemelos (a; b) tal que a > n.-}

{- |Documentación

  - Nombre: proxprimosgem
  - Declaración: Int -> (Int, Int)
  - Descripción: Dado un número entero n, la función busca el siguiente
  par de primos gemelos (a,b) tal que a>n. La función busca el siguiente primo
  de n, y se fija si es igual proximo proximo primo menos dos. Si son iguales,
  devuelve el proximo primo y el proximo proximo primo. Sino, sigue buscando.
  La función no termina en un caso base, porque se supone que hay infinitos
  pares de primos gemelos.
  - Parámetros: n (desde dónde se busca el próximo par de primos gemelos)
  - Retorna: (Int, Int): El primer par de primos gemelos encontrados.
  - Necesita: primosiguiente
  -}


proxprimosgem :: Int -> (Int, Int)
proxprimosgem n | primosiguiente n == primosiguiente (n+1) - 2 = (primosiguiente n, primosiguiente (n+1))
                | otherwise = proxprimosgem (n+1)


{-Ejercicio 17: Conjetura de Lothar Collatz, 1937: sea la siguiente definición:

a(n+1) = { a(n)/2      si a(n) es par
         { 3a(n) + 1   si a(n) es impar

Empezando a1 con cualquier entero positivo siempre se llega a 1. Por ejemplo, si 
a1 = 13, obtenemos la siguiente secuencia: 
13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1 (9 reducciones, o sea 9 flechas). -}


{- Ejercicio 17, a: largosecuencia :: Int -> Int 
que dado un n > 0 devuelve la cantidad de reducciones desde a1 = n hasta
llegar a 1. Por ejemplo, largoSecuencia 13 es 9. -}

{- |Documentación

  - Nombre: largosecuenciav
  - Declaración: Int -> Int -> Int
  - Descripción: Dado un número entero a, la función determina si es o no es par y
   aplica la reducción correspondiente según la conjetura de Lothar Collatz.
   Luego de cada reducción, incrementa v. Cuando a es igual a 1, devuelve v,
   es decir la cantidad de reducciones realizadas. Para que devuelva la cantidad
   correcta de reducciones, v tiene que ser inicialmente cero.
  - Parámetros: Int: a (valor inicial)
  - Retorna: Int: v (cantidad de reducciones)
  -}

largosecuenciav :: Int -> Int -> Int
largosecuenciav a v | a == 1 = v
                    | espar a == True  = largosecuenciav (div a 2) (v+1)
                    | espar a == False = largosecuenciav (3*a + 1) (v+1)
 

{- |Documentación

  - Nombre: largosecuencia
  - Declaración: Int -> Int
  - Descripción: Dado un número entero a, la función devuelve la cantidad de
  reducciones realizadas según la conjetura de Lothar Collatz. 
  - Parámetros: Int: a (valor inicial)
  - Retorna: Int: cantidad de reducciones
  - Necesita: largosecuenciav
  -}

largosecuencia :: Int -> Int
largosecuencia a = largosecuenciav a 0


{- Ejercicio 17, b: Resolver usando Haskell: ¿Qué número menor a 10.000 para a1
produce la secuencia de números más larga hasta llegar a 1? 
Sugerencia: usar la idea de la función del ejercicio 11. -}

maxlargosecuencia :: Int -> Int -> Int
maxlargosecuencia a b | largosecuencia a >= largosecuencia b = a
                      | otherwise = b

maxlargosecuenciahasta :: Int -> Int -> Int
maxlargosecuenciahasta a b | a == b = a
                           | otherwise = maxlargosecuencia (maxlargosecuenciahasta a (b-1)) b

maxlargosecuenciafinal :: Int -> Int
maxlargosecuenciafinal b = maxlargosecuenciahasta 1 b
  
---RTA: El número menor a 10.000 con la secuencia más larga es el 6171.


{- como funciona:
maxhasta 1 5 = max (maxhasta 1 4) 5
maxhasta 1 5 = max (max (maxhasta 1 3) 4) 5
maxhasta 1 5 = max (max (max (maxhasta 1 2) 3) 4) 5
maxhasta 1 5 = max (max (max (max (maxhasta 1 1) 2) 3) 4) 5
maxhasta 1 5 = max (max (max (max (1) 2) 3) 4) 5
maxhasta 1 5 = max (max (max (2) 3) 4) 5
maxhasta 1 5 = max (max (3) 4) 5
maxhasta 1 5 = max (3) 5
maxhasta 1 5 = 5                                 -}
