import FuncionesClase09
import FuncionesClase07
import FuncionesClase05

{- Ejercicio 1: solucionEc :: (Int, Int, Int) -> (Int, Int)
que resuelve una ecuación lineal de congruencia .-}

solucionEc :: (Int, Int, Int) -> (Int, Int)
solucionEc (a, b, m) | mod b d == 0 = (mod (b*s) m , m)
                     | otherwise    = undefined
 where (d, s, t) = emcd a m


{- Ejercicio 2: sistemaSimplifEquiv :: [(Int, Int, Int)] -> [(Int, Int)]
que dado un sistema lineal de ecuaciones de congruencia, devuelve un
sistema simplificado equivalente -}

sistemaSimplifEquiv' :: [(Int, Int, Int)] -> [(Int, Int)]
sistemaSimplifEquiv' sist = map solucionEc sist

sistemaSimplifEquiv :: [(Int, Int, Int)] -> [(Int, Int)]
sistemaSimplifEquiv []     = []
sistemaSimplifEquiv (e:es) = (solucionEc e) : (sistemaSimplifEquiv es)


{- Ejercicio 3: todosLosPrimosMalos :: [(Int, Int)] -> [Int]
que devuelve una lista con todos los primos malos para un
sistema simplificado -}

listaDeModulos :: [(Int, Int)] -> [Int]
listaDeModulos []         = []
listaDeModulos ((a,m):ms) = m : listaDeModulos ms

listaDeDivisoresHasta :: Int -> Int -> [Int]
listaDeDivisoresHasta n 1 = []   --no tiene en cuenta el 1
listaDeDivisoresHasta n i
 | mod n i == 0 = i : (listaDeDivisoresHasta n (i-1))
 | otherwise    =     (listaDeDivisoresHasta n (i-1))

listaDeDivisores :: Int -> [Int]
listaDeDivisores n = listaDeDivisoresHasta n n

listaDeDivisoresDeModulos :: [Int] -> [Int]
listaDeDivisoresDeModulos []     = []
listaDeDivisoresDeModulos (m:ms) =  (listaDeDivisores m) `union'` (listaDeDivisoresDeModulos ms)

union' :: [Int] -> [Int] -> [Int]
union' []     bs = bs
union' (a:as) bs = as `union'` (a:bs)

quitarNoRepes :: [Int] -> [Int]
quitarNoRepes [] = []
quitarNoRepes (x:xs)
 | x `pertenece` xs = x : quitarNoRepes xs
 | otherwise        =     quitarNoRepes xs

todosLosPrimosMalos :: [(Int, Int)] -> [Int]
todosLosPrimosMalos sist = quitarNoRepes (listaDeDivisoresDeModulos (listaDeModulos sist))


{- Ejercicio 4: solucSistemaPotenciasPrimo :: [(Int, Int)] -> (Int, Int)
que resuelve un sistema en el que todos los módulos son potencias de un mismo
primo. -}

solucSistemaPotenciasPrimo :: [(Int, Int)] -> (Int, Int)
solucSistemaPotenciasPrimo []  = (0,0)
solucSistemaPotenciasPrimo [e] = e
solucSistemaPotenciasPrimo (e1:e2:es)
 | m2 >  m1                        = solucSistemaPotenciasPrimo (e2:e1:es)
 | m1 >= m2 && mod (a1-a2) m2 == 0 = solucSistemaPotenciasPrimo (e1:es)
 | otherwise = undefined
 where (a1, m1) = e1
       (a2, m2) = e2


{- Ejercicio 4: desdoblarSistemaEnFcionPrimo :: [(Int, Int)] -> Int -> ([(Int, Int)], [(Int, Int)])
que dado un sistema y un primo p, devuelve los dos sistemas que surgen al desdoblar cada ecuación del
sistema original según el primo p -}

--asumo que ya se que p divide a m
quePotenciaLoDivideDesde :: Int -> Int -> Int -> Int
quePotenciaLoDivideDesde m p k
 | mod m (p^k) == 0 = quePotenciaLoDivideDesde m p (k+1)
 | mod m (p^k) /= 0 && mod m (p^(k-1)) == 0 = (k-1)

quePotenciaLoDivide :: Int -> Int -> Int
quePotenciaLoDivide m p = quePotenciaLoDivideDesde m p 0


desdoblarSistemaEnFcionPrimo :: [(Int, Int)] -> Int -> ([(Int, Int)], [(Int, Int)])
desdoblarSistemaEnFcionPrimo [] p = ([], [])
desdoblarSistemaEnFcionPrimo (e:xs) p
 | k == 0           = (pri, (e:seg))
 | div m (p^k) == 1 = ((e:pri), seg)
 | div m (p^k) /= 1 = ((a, (p^k)) : pri,   (a, div m (p^k)) : seg)
 where k = (quePotenciaLoDivide m p)
       (a, m) = e
       (pri, seg) = (desdoblarSistemaEnFcionPrimo xs p)


{- Ejercicio 5: sistemaEquivSinPrimosMalos :: [(Int, Int)] -> [(Int, Int)]
que dado un sistema, devuelve un sistema equivalente sin primos malos. -}

desdoblarSistemaTodosLosPrimos :: [(Int, Int)] -> [Int] -> [(Int, Int)]
desdoblarSistemaTodosLosPrimos sist []     = sist
desdoblarSistemaTodosLosPrimos sist (p:ps)
 | pri /= [] = (solucSistemaPotenciasPrimo pri) : (desdoblarSistemaTodosLosPrimos seg ps)
 | otherwise = (desdoblarSistemaTodosLosPrimos seg ps)
 where (pri, seg) = desdoblarSistemaEnFcionPrimo sist p

sistemaEquivSinPrimosMalos :: [(Int, Int)] -> [(Int, Int)]
sistemaEquivSinPrimosMalos sist = desdoblarSistemaTodosLosPrimos sist (todosLosPrimosMalos sist)


{- Ejercicio 6: solucSistemaModCoprimos :: [(Int, Int)] -> (Int, Int)
que resuelve un sistema simplificado en el que los módulos son coprimos dos a dos. -}

tcr :: (Int, Int) -> (Int, Int) -> (Int, Int)
tcr e1 e2 = (r, m1*m2)
 where (r1, m1) = e1
       (r2, m2) = e2
       (d, s, t) = emcd m1 m2
       r = mod (r1*t*m2 + r2*s*m1) (m1*m2)

solucSistemaModCoprimos :: [(Int, Int)] -> (Int, Int)
solucSistemaModCoprimos [e] = e
solucSistemaModCoprimos (e1:e2:es) = solucSistemaModCoprimos ((tcr e1 e2) : es)


{- Ejercicio 7: solucSistema :: [(Int, Int, Int)] -> (Int, Int)
que resuelve un sistema general. -}

solucSistema :: [(Int, Int, Int)] -> (Int, Int)
solucSistema sist = solucSistemaModCoprimos (sistemaEquivSinPrimosMalos (sistemaSimplifEquiv sist))



{- Ejercicio 8: cadaEcTieneSoluc :: [(Int, Int, Int)] -> Bool que,
dado un sistema general, decide si cada una de sus ecuaciones vista 
independientemente de las otras, tiene solución. -}

cadaEcTieneSoluc :: [(Int, Int, Int)] -> Bool
cadaEcTieneSoluc [] = True
cadaEcTieneSoluc ((a,b,m):es) = (mod b d == 0) && (cadaEcTieneSoluc es)
 where d = mcd a m

queEcNoTieneSoluc :: [(Int, Int, Int)] -> [(Int, Int, Int)]
queEcNoTieneSoluc [] = []
queEcNoTieneSoluc ((a,b,m):es)
 | mod b d == 0 = queEcNoTieneSoluc es
 | otherwise = (a,b,m): queEcNoTieneSoluc es
 where d = mcd a m


{- Ejercicio 9: tieneSolucionSimplif :: [(Int, Int)] -> Bool
que, dado un sistema simplificado, decide si tiene solución. -}



{- Ejercicio 10: tieneSolucion :: [(Int, Int, Int)] -> Bool
que, dado un sistema general, decide si tiene solución. -}

-- fiaca


{- Ejercicio 11: dirichlet :: Int -> Int -> Int
que dados dos números coprimos r y m con 1 ≤ r < m, encuentra un
número primo en la clase de congruencia X ≡ r (mod m). -}

-- primo = m*k + r

dirichletDesde :: Int -> Int -> Int -> Int
dirichletDesde r m i 
 | esprimo (m*i + r) = (m*i + r)
 | otherwise = dirichletDesde r m (i+1)

dirichlet :: Int -> Int -> Int
dirichlet r m = dirichletDesde r m 0


