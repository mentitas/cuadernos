-- Trabajo Práctico 1, Florencia Rosenzuaig


--------------------(Ejercicio 1)--------------------
{- Implementar med :: Float -> Float -> Int -> Float
tal que med i0 b n sea la cantidad de infectados luego de n
días para un modelo con i0 infectados iniciales y tasa de
infección b.

. I(n) = I(n-1) + I(n-1) · b                               -}



med :: Float -> Float -> Int -> Float
med i0 b 0 = i0
med i0 b n = med i0 b (n-1) + b * (med i0 b (n-1))



--------------------(Ejercicio 2)--------------------
{- Implementar mld ::  mld :: Float -> Float -> Float -> Int -> Float
tal que mld p i0 b n sea la cantidad de infectados luego de n días
para un modelo con una población total de p personas, i0 infectados
iniciales y tasa de infección b.

. I(n) =  I(n-1)  +  I(n-1) · b · (S(n-1) / p)

. S(n) = p - I(n)                                                   -}
 


mld :: Float -> Float -> Float -> Int -> Float
mld p i0 b n | n == 0    = i0 
             | otherwise = i + i * b * (s/p)
             where i = mld p i0 b (n-1)
                   s = (p - i)

-- i = infectados del día anterior
-- s = sanos del día anterior



--------------------(Ejercicio 3)--------------------
{- Implementar una función sir :: (Float, Float, Float) -> Float -> Float 
-> Int -> (Float, Float, Float) tal que sir (s0, i0, r0) b g n sea una tupla
con la cantidad de individuos sanos, infectados y recuperados luego de n 
días, para un modelo de población total de s0 individuos sanos, i0 individuos
infectados, r0 individuos recuperados, factor b de contagio y factor g de
recuperación.


. S(n) =  S(n-1)   -   b · I(n-1) · S(n-1)

. I(n) =  I(n-1)   +   b · I(n-1) · S(n-1)   -   g · I(n-1)

. R(n) =  R(n-1)   +   g · I(n-1)                                             -}



first :: (ta, tb, tc) -> ta
first (a, b, c) = a

second :: (ta, tb, tc) -> tb
second (a, b, c) = b

third :: (ta, tb, tc) -> tc
third (a, b, c) = c


sir :: (Float, Float, Float) -> Float -> Float -> Int -> (Float, Float, Float)
sir (s0, i0, r0) b g n | n == 0 = (s0, i0, r0)
                       | otherwise = (s1 - b * i1 * s1,   i1 + b * i1 * s1 - g * i1,   r1 + g * i1)
                        where sirprevio = sir (s0, i0, r0) b g (n-1)
                              s1 = first  (sirprevio) 
                              i1 = second (sirprevio)
                              r1 = third  (sirprevio)


-- s1 = sanos del día anterior
-- i1 = infectados del día anterior
-- r1 = recuperados del día anterior



--------------------(Ejercicio 4)--------------------
{- Implementar una función maxsir :: (Float, Float, Float) -> Float -> 
Float -> Int -> Float que calcule la cantidad máxima de infectados en 
un período de tiempo (definido como la cantidad de infectados en el día 
con mayor cantidad de infectados). maxsir (s0,i0,r0) b g n debe retornar 
el valor máximo entre {i0, i1, ..., in} para un modelo con una población
inicial de (s0, i0, r0) personas en cada situación, y factores b y g. -}



-- Devuelve sólo el segundo valor de sir (es decir, los infectados).

infectadosdesir :: (Float, Float, Float, Float, Float) -> Int -> Float
infectadosdesir (s0, i0, r0, b, g) n = second (sir (s0, i0, r0) b g n)



-- Compara dos días con los mismos valores iniciales, y devuelve el día con más infectados.

diamaximo :: (Float, Float, Float, Float, Float) -> Int -> Int -> Int
diamaximo (s0, i0, r0, b, g) n m | infectadosn >= infectadosm  = n
                                 | otherwise                   = m
                                 where infectadosn = infectadosdesir (s0, i0, r0, b, g) n
                                       infectadosm = infectadosdesir (s0, i0, r0, b, g) m




-- Compara todos los días hasta n y devuelve el día con mayor infectados.

diamaximohasta :: (Float, Float, Float, Float, Float) -> Int -> Int
diamaximohasta (s0, i0, r0, b, g) n | n == 0    = 0
                                    | otherwise = diamaximo c0 n (diamaximohasta c0 (n-1))
                                    where c0 = (s0, i0, r0, b, g)


-- c0 = condiciones iniciales y factores de contagio y recuperación.




-- Devuelve los infectados del día con mayor infectados.

maxsir :: (Float, Float, Float) -> Float -> Float -> Int -> Float
maxsir (s0, i0, r0) b g n = infectadosdesir c0 (diamaximohasta c0 n) 
 where c0 = (s0, i0, r0, b, g)


-- c0 = condiciones iniciales y factores de contagio y recuperación.