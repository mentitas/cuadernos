// 1. Enunciar el axioma 5, que establece la precondición más débil de un ciclo. ¿Por qué no se puede
//    usar este axioma habitualmente para demostrar la corrección de un ciclo?¿Existe algún ciclo cuya
//    corrección se pueda demostrar usando directamente este axioma?

Es dificil establecer la precondición más débil de un ciclo ya que desde su estado inicial hasta el final,
el programa atraviesa tantos estados como iteraciones.

Si E₀, E₁, ..., Eₙ es la secuencia de estados que atraviesa el programa, entonces intuitivamente su precondición
más débil sería:


{P} while B do S {Q}
































// 2. Decimos que una secuencia es alternante si cada elemento ubicado en una posición par es menor que el
//    elemento siguiente, y cada elemento ubicado en una posición impar es mayor que el elemento siguiente.
//    Por ejemplo, la secuencia ⟨4,7,2,9,4,6,2,8⟩ es alternante, pero la secuencia ⟨4,6,3,5,7,9⟩ no lo es
//    (por los valores 5 y 7).
// 
//    a. Dar una especificación para el problema de determinar si una secuencia es alternante

pred esAlternante?(in s: seq⟨ℤ⟩, out res: bool){
	Pre:  { |s|≠0 }
	Post: { res = true ⟺
	        (∀ i: ℤ)(0 ≤ i < |s|-1 ⟹L (i mod 2 = 0 ∧ s[i] < s[i+1]) ∨ (i mod 2 = 1 ∧ s[i] > s[i+1]))
	      }
}


//    b. Dar un algoritmo para este problema

i := 0;
res := true;
while( i < |s|-1 ) do
	if (i mod 2 = 0) then
		res := res && s[i] < s[i+1];
	else 
		res := res && s[i] > s[i+1];
	fi
	i:=i+1;
endwhile


//    c. Demostrar que este algoritmo es correcto para la especificación propuesta, utilizando las
//       técnicas vistas en la materia


Para demostrar que éste algoritmo es correcto tengo que ver que la tripla de Hoare
{P} while B do S {Q} sea válida, siendo:

P ≡ (i=0 ∧ res=true)
B ≡ i < |s|-1
S1 ≡ if (i mod 2 = 0) then (res := res && s[i] < s[i+1]) else (res := res && s[i] > s[i+1]) fi;
S2 ≡ i:=i+1;
Q ≡ (res = true ⟺
	(∀ j: ℤ)(0 ≤ j < |s|-1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))

Para demostrarlo, propongo el siguiente invariante:

I ≡ 0 ≤ i ≤ |s|-1 ∧L
   (res = true ⟺ (∀ j: ℤ)(0 ≤ j < i ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))


Ahora voy a ver si se cumplen los 5 puntos del teorema de corrección de ciclos:

---------------------------------------------------------------------------------------------------
1. P ⟹ I

Sabiendo que se cumple P:
	. i=0
	. res=true

Quiero ver si se cumple I:
	. 0 ≤ i ≤ |s|-1 → se cumple porque |s|≠0
	. (res = true ⟺ (∀ j: ℤ)(0 ≤ j < i ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))

→ se cumple porque el rango (0 ≤ j < i) es vacio, por lo que el lado izquierdo del "implica" es siempre
  false, entonces el "paratodo" es siempre true. Luego, como res=true y el "paratodo" es siempre true,
  se cumple el "sii".

  ⟹ Se cumple P ⟹ I
  ⟹ Se cumple 1.


2. {I ∧ B} S {I}

Quiero demostrar que (I ∧ B) ⟹ wp(S, I)

S1 ≡ if (i mod 2 = 0) then (res := res && s[i] < s[i+1]) else (res := res && s[i] > s[i+1]) fi;
S2 ≡ i:=i+1;
I ≡ 0 ≤ i ≤ |s|-1 ∧L
   (res = true ⟺ (∀ j: ℤ)(0 ≤ j < i ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))


wp(S1;S2, I) ≡ wp(S1, wp(S2, I))

wp(S2, I) ≡ wp(i:=i+1, I)
				  ≡ def(i+1) ∧L Iⁱᵢ₊₁
				  ≡ true ∧L Iⁱᵢ₊₁
				  ≡ Iⁱᵢ₊₁
				  ≡ 0 ≤ i+1 ≤ |s|-1 ∧L
   (res = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))

   				≡ -1 ≤ i ≤ |s|-2 ∧L
   				(res = true ⟺ (∀ j: ℤ)(0 ≤ j ≤ i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))

wp(S1;S2, I) ≡ wp(S1, wp(S2, I))
             ≡ wp(S1, Iⁱᵢ₊₁)
             ≡ wp(if G then A else B fi, Iⁱᵢ₊₁)
             ≡ def(G) ∧L ((G ∧ wp(A, Iⁱᵢ₊₁)) ∨ (¬G ∧ wp(B, Iⁱᵢ₊₁)))


**Recordatorio: G ≡ true ≡ i mod 2 = 0 **

wp(A, Iⁱᵢ₊₁) ≡ wp(res := res ∧ s[i] < s[i+1], Iⁱᵢ₊₁)
	           ≡ wp(res := res ∧ s[i] < s[i+1], -1 ≤ i ≤ |s|-2 ∧L (res = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1]))))
             ≡ def(res ∧ s[i] < s[i+1]) ∧L  (-1 ≤ i ≤ |s|-2 ∧L (res ∧ s[i] < s[i+1] = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))))
	           ≡ 0 ≤ i < |s| ∧ 0 ≤ i+1 < |s| ∧ -1 ≤ i ≤ |s|-2 ∧L (res ∧ s[i] < s[i+1] = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))))
						 ≡ 0 ≤ i < |s|-1 ∧L (res ∧ s[i] < s[i+1] = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))))

** Evaluo cada caso: **

Si i mod 2 = 0 y s[i] < s[i+1]

≡ 0 ≤ i < |s|-1 ∧L (res ∧ s[i] < s[i+1] = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))))
≡ 0 ≤ i < |s|-1 ∧L (res ∧ true = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (res = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (res = true ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (res = true ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ⟹L ((j = i ∧ (i mod 2 = 0 ∧ s[i] < s[i+1]) ∨ (i mod 2 = 1 ∧ s[i] > s[i+1]))
	                                                    ∨ (j ≠ i ∧ (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (res = true ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ⟹L ((j = i ∧ (true ∧ true)                 ∨ (j mod 2 = 1 ∧ s[j] > s[j+1]))
	                                                    ∨ (j ≠ i ∧ (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (res = true ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ⟹L ((j = i ∧ true)
	                                                    ∨ (j ≠ i ∧ (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (res = true ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ⟹L ((j = i)
	                                                    ∨ (j ≠ i ∧ (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))

																											** reescribo **

≡ 0 ≤ i < |s|-1 ∧L (res = true ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ∧ j ≠ i ⟹L ((j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (res = true ⟺ (∀ j: ℤ)(0 ≤ j < i ⟹L ((j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))


Si i mod 2 = 0 y s[i] ≥ s[i+1]

≡ 0 ≤ i < |s|-1 ∧L (res ∧ s[i] < s[i+1] = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))))
≡ 0 ≤ i < |s|-1 ∧L (res ∧ false = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (false = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (false ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (false ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ⟹L ((j = i ∧ (i mod 2 = 0 ∧ s[i] < s[i+1]) ∨ (i mod 2 = 1 ∧ s[i] > s[i+1]))
	                                                    ∨ (j ≠ i ∧ (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (false ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ⟹L ((j = i ∧ ((true ∧ false) ∨ (false ∧ true))
	                                                    ∨ (j ≠ i ∧ (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (false ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ⟹L ((j = i ∧ (false ∨ false)
	                                                    ∨ (j ≠ i ∧ (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (false ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ⟹L (j = i ∧ false
	                                                    ∨ (j ≠ i ∧ (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))

≡ 0 ≤ i < |s|-1 ∧L (false ⟺ (∀ j: ℤ)(0 ≤ j ≤ i ∧ j ≠ i ⟹L ((j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
≡ 0 ≤ i < |s|-1 ∧L (false ⟺ (∀ j: ℤ)(0 ≤ j < i ⟹L ((j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))


***
wp(A, Iⁱᵢ₊₁) ≡ 0 ≤ i < |s|-1 ∧L (res = true ⟺ (∀ j: ℤ)(0 ≤ j < i ⟹L ((j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))
***



** Recordatorio: se cumple ¬G ≡ i mod 2 = 1 **
wp(B, Iⁱᵢ₊₁) ≡ wp(res := res ∧ s[i] > s[i+1],  Iⁱᵢ₊₁)
             ≡ def(res ∧ s[i] > s[i+1]) ∧L (-1 ≤ i ≤ |s|-2 ∧L (res = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))))
             ≡ 0 ≤ i < |s| ∧ 0 ≤ i+1 < |s| ∧ -1 ≤ i ≤ |s|-2 ∧L (res ∧ s[i] > s[i+1]) = true ⟺ (∀ j: ℤ)(0 ≤ j < i+1 ⟹L (j mod 2 = 0 ∧ s[j] < s[j+1]) ∨ (j mod 2 = 1 ∧ s[j] > s[j+1])))))










// AAAAAAAAAAAAAAAA sin terminar


// 3. ¿Cuál es la propiedad de distributividad del operador "precondición más débil"?¿Cuál es la intuición
//    detrás de ésta propiedad?























// 4. Una matriz cuadrada de enteros de n filas y n columnas es un cuadrado latino si:

	// a. cada fila contiene a todos los enteros entre 1 y n
	// b. los valores de cada fila son todos distintos
	// c. los valores de cada columna son todos distintos

// a. Especificar el problema de determinar si una matriz de enteros es un cuadrado latino.

proc esCuadradoLatino?(in m: seq(seq(ℤ)), out res: bool){
	Pre:  { (∀ i: ℤ)(0 ≤ i < |m| ⟹L |m[i]| = |m[0]|) } // es una matriz válida
	Post: { res = true ⟺ (
				|m| = |m[0]| // es una matriz cuadrada
		        ∧L (∀ i, j: ℤ)(0 ≤ i, j < |m| ⟹L 1 ≤ m[i][j] ≤ |m|)             // son elementos entre 1 y n
		        ∧  (∀ i, j, k: ℤ)(0 ≤ i, j, k < |m| ∧ k≠i ⟹L m[i][j] ≠ m[k][J]) // no se repiten verticalmente
            ∧  (∀ i, j, l: ℤ)(0 ≤ i, j, k < |m| ∧ l≠j ⟹L m[i][j] ≠ m[i][l]) // no se repiten horizontalmente
		        )}
}

// b. Proponer un algoritmo para resolver el problema especificado en el punto anterior.

// O(n)
bool esTodoTrue(vector<bool> v){
	res = true;
	for(int i=0; i<v.size(); i++){
		res &= v[i];
	}
	return res;
}

// O(n)
void hacerTodoFalse(vector<bool> &v){
	for(int i=0; i<v.size(); i++){
		v[i] = false;
	}
}

// Pertenece es un vector que "toma lista" de los elementos de otro lado.
// Por ejemplo, si mi vector "Pertenece" de una secuencia S es [true, false, true, false], entonces se que
// 1 y 3 pertenecen a ya que Pertenece[1-1] == Pertenece[3-1] == true. 
// Además, se que 2 y 4 no pertenecen a S, ya que Pertenece[2-1] == Pertenece[4-1] == false.
// Si además se que S tiene elementos entre 1 y 4, entonces se que S=[1,3] o S=[3,1].

// Voy a usar un vector Pertenece para verificar que todas las filas y columnas contienen
// a todos los elementos entre 1 y n.

bool esCuadradoLatino(vector<vector<int>> m){
	int n = m.size();
	vector<bool> pertenece(n, false); // Armo un vector de longitud n con todos los elementos "false".
	bool res = true;

	// ¿Es correcto horizontalmente?
	for(int i=0; i<n && !res; i++){
		for(int j=0; j<n; j++){
			if(1 ≤ m[i][j] ≤ n) pertenece[m[i][j]-1] = true;
		}
		res &= todoTrue(pertenece);
		hacerTodoFalse(pertenece);
	}

	// ¿Es correcto verticalmente?
	for(int j=0; j<n && !res; j++){
		for(int i=0; i<n; i++){
			if(1 ≤ m[i][j] ≤ n) pertenece[m[i][j]-1] = true;
		}
		res &= todoTrue(pertenece);
		hacerTodoFalse(pertenece);
	}

	return res;
}

// c. ¿Cuál es la complejidad computacional del algoritmo propuesto? (atención, esta pregunta tiene trampa!)

// En el peor caso, la matriz es un cuadrado latino y tengo que chequear la correctitud de todas las filas y
// columnas.

bool esCuadradoLatino(vector<vector<int>> m){
	int n = m.size();                 // O(1)
	vector<bool> pertenece(n, false); // O(n)
	bool res = true;                  // O(1)

	// ¿Es correcto horizontalmente?
	for(int i=0; i<n && !res; i++){                           // O(n²)
		for(int j=0; j<n; j++){                               // O(n)  
			if(1 ≤ m[i][j] ≤ n) pertenece[m[i][j]-1] = true;  // O(1)  
		}
		res &= todoTrue(pertenece); // O(n)
		hacerTodoFalse(pertenece);  // O(n)
	}

	// ¿Es correcto verticalmente?
	for(int j=0; j<n && !res; j++){                          // O(n²)
		for(int i=0; i<n; i++){                              // O(n)      
			if(1 ≤ m[i][j] ≤ n) pertenece[m[i][j]-1] = true; // O(1)            
		}
		res &= todoTrue(pertenece); // O(n)
		hacerTodoFalse(pertenece);  // O(n)
	}

	return res;
}

// Respuesta: la complejidad del algoritmo es O(n²).