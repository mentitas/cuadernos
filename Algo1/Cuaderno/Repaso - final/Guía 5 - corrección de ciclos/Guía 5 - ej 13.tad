Ejercicio 13.

Demostrar que el siguiente programa es correcto respecto a la especificación dada

Especificación:

proc esSimetrico ( in s: seq⟨ℤ⟩, out r: Bool) {
	Pre { True }
	Post { r = True ⟺ (∀ i: ℤ)(0 ≤ i < |s|) ⟹L s[i] = s[|s|-(i+1)]}
}



Implementación en SmallLang:

i := 0;
j := s.size()-1
r := true;
while(i < s.size()) do
	if (s[i]!=s[j]) then
		r := false
	else
		skip
	endif;
	i := i+1;
	j := j-1;
endwhile



*******************************************************************************

Invariante:

I ≡ 0 ≤ i ≤ |s| ∧ j = |s|-i-1 ∧L (r = true ⟺ (∀ k: ℤ)(0 ≤ k < i ⟹L s[k] = s[|s|-k-1]))

-------------------------------------------------------------------------------
1. Pc ⟹ I

Sabiendo que se cumple Pc:
	. i = 0
	. j = |s|-1
	. r = true

Quiero ver que se cumple I:
	. 0 ≤ i ≤ |s|            	                       → se cumple
	. j = |s|-i-1            	                       → se cumple porque i=0 y j=|s|-1
	. (r = true ⟺ (∀ k: ℤ)(0 ≤ k < i ⟹L s[i] = s[j]))  → se cumple porque i=0, entonces la parte izquierda
	                                                     del implica siempre va a dar false, por lo que
	                                                     la parte derecha del sii va a dar true.

	⟹ Se cumple Pc ⟹ I
	⟹ Se cumple 1.


-------------------------------------------------------------------------------
2. {I ∧ B} S {I}

I ∧ B ≡  0 ≤ i < |s| ∧ j = |s|-i-1 ∧L (r = true ⟺ (∀ k: ℤ)(0 ≤ k < i ⟹L s[k] = s[|s|-k-1]))

S1 ≡ if s[i]!=s[j] then r:=false else skip fi
S2 ≡ i := i+1
S3 ≡ j := j-1

I ≡ 0 ≤ i ≤ |s| ∧ j = |s|-i-1 ∧L (r = true ⟺ (∀ k: ℤ)(0 ≤ k < i ⟹L s[k] = s[|s|-k-1]))



Quiero demostrar que I ∧ B ⟹ wp(S1;S2;S3, I)


wp(S1;S2;S3) ≡ wp(S1, wp(S2, wp(S3, I)))

// vamos x partes (dijo jack)

wp(S3, I) ≡ wp(j:=j-1, I)
          ≡ def(j-1) ∧L Iʲⱼ₋₁
          ≡ true ∧L 0 ≤ i ≤ |s| ∧ j-1 = |s|-i-1 ∧L (r = true ⟺ (∀ k: ℤ)(0 ≤ k < i ⟹L s[k] = s[|s|-k-1]))