/*************************(Ejercicio 2)*************************/
bool esSumaDePares(vector<int> &s){ 
  bool res=true;
  for(int i=0; i<v.size();i=i+3){ // O(n) 
    res&=(s[i] + s[i+1] ==s[i+2]);
  }
  return res;
}

/*************************(Ejercicio 3)*************************/
bool existe(vector<int> s, int elem, int desde, int hasta){
  bool res=false;
  for(int i=desde; i<hasta; i++){
    if(s[i]==elem){
      res = true;
    }
  }
  return res;
}

int cantidadDeDistintos(vector<int> s, int desde, int hasta){
  int res=0;
  for(int i=desde; i<hasta; i++){
    if(!existe(s, s[i], desde, i)){
      res++;
    }
  }
}

int mitadDeDistintos(vector<int> s){
  int res=-1;
  for(int i=0; i<s.size(); i++){
    if(cantidadDeDistintos(s, 0, i) == cantidadDeDistintos(s, i+1, s.size())){
      res = i;
    }
  }
  return res;
}

/*************************(Ejercicio 4)*************************/
int largo(vector<vector<int>> m, int fCabeza, int cCabeza){  bool cola = true;
  vector<pair<int,int>> viborita{make_pair(fCabeza, cCabeza)};
  if(meMuevoArriba(m,viborita){
    viborita.push_back(m[fCabeza][cCabeza+1]);
  }
  //y bla bla
     
  while(cola){
    cola = buscoViborita(m, viborita);
  }
  return viborita.size();
}

bool evaluar (vector<vector<int>> m, int i, int j){
  return m[i][j];
}

bool buscoViborita(vector<vector<int>> m, vector<pair<int,int>> &v){
  bool res = false;
  pair<int,int> ultimo = v[v.size() - 1];
  pair<int,int> anteultimo =  v[v.size() - 2];
  
  if(ultimo.first+1<m.size()){
    bool arriba = m[ultimo.first+1][ultimo.second];
    pair<int,int> cArriba = make_pair(ultimo.first+1, ultimo.second);
    if(cArriba!=anteultimo && arriba){
      //nos movemos para arriba
      res = true;
      v.push_back(cArriba);
    }
  }
  if(ultimo.first-1<m.size()){
    bool abajo = m[ultimo.first-1][ultimo.second];
    pair<int,int> cAbajo = make_pair(ultimo.first-1, ultimo.second);
    if(cAbajo!=anteultimo && abajo){
      //nos movemos abajo
      res = true;
      v.push_back(cAbajo);
    }
  }
  if(ultimo.second+1<m.size()){
    bool derecha = m[ultimo.first][ultimo.second+1];
    pair<int,int> cDerecha = make_pair(ultimo.first, ultimo.second+1);
    if(cDerecha!=anteultimo && derecha){
    //nos movemos a la derecha
    res = true;
    v.push_back(cDerecha);
    }
  }
  if(ultimo.second-1<m.size()){
     bool izquierda = m[ultimo.first][ultimo.second-1];
     pair<int,int> cIzquierda = make_pair(ultimo.first, ultimo.second-1);
     if(cIzquierda!=anteultimo && izquierda){
      //nos movemos a la izquierda
      res = true;
      v.push_back(cIzquierda);
     }
  }
  return res;
}

/*************************(Ejercicio 4)*************************/

// a.

int largo(int F, int C, vector<vector<bool>> T, int fCabeza, int cCabeza){
    int res=0;
    pair<int,int> posicion = make_pair(fCabeza, cCabeza);
    string ultimoMov = "";
    bool termine=false;
    while(!termine){ //O(res) --> res=longitud de viborita
        if(puedoMoverArriba(T,posicion) && ultimoMov!="abajo"){ //O(1)
            //me muevo arriba
            ultimoMov = "arriba";
            posicion.first++;
            res++;
        } else if(puedoMoverAbajo(T,posicion) && ultimoMov!="arriba"){ //O(1)
            //me muevo abajo
            ultimoMov = "abajo";
            posicion.first--;
            res++;
        } else if (puedoMoverDerecha(T,posicion) && ultimoMov!="izquierda"){ //O(1)
            //me muevo a la derecha
            ultimoMov = "derecha";
            posicion.second++;
            res++;
        } else if(puedoMoverIzquierda(T,posicion) && ultimoMov!="derecha"){ //O(1)
            //me muevo a la izquierda
            ultimoMov = "izquierda";
            posicion.second--;
            res++;
        } else {
            termine = true;
        }
    }
    return res;
}

bool puedoMoverArriba(vector<vector<bool>> T, pair<int,int> pos){
    int i=pos.first;
    int j=pos.second;
    return i+1<T.size() && T[i+1][j];
}

bool puedoMoverAbajo(vector<vector<bool>> T, pair<int,int> pos){
    int i=pos.first;
    int j=pos.second;
    return i-1>0 && T[i-1][j];
}

bool puedoMoverDerecha(vector<vector<bool>> T, pair<int,int> pos){
    int i=pos.first;
    int j=pos.second;
    return i+1<T[0].size() && T[i][j+1];
}

bool puedoMoverIzquierda(vector<vector<bool>> T, pair<int,int> pos){
    int i=pos.first;
    int j=pos.second;
    return i-1>0 && T[i][j-1];
}