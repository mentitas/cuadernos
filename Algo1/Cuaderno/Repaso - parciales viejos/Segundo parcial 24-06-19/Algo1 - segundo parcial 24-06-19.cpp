/*************************(Ejercicio 1)*************************/
//a. Para cada casillero i j de la matriz, la función f la modifica para que diga las sumas acumuladas hasta ese momento.
//   por ejemplo, para la matriz t1, luego de aplicarle f queda la matriz t2. En cada casillero ahora están la suma del elemento
//   prexistente con la suma de todos los elementos anteriores.

t1 = {{1,1,1},
	  {1,1,1},
	  {1,1,1}}

t2 = {{1,2,3},
	  {4,5,6},
	  {7,8,9}}

//b. el tiempo de ejecucion de aux en el peor caso es O(M), y el ciclo de f también es O(M), pero como en cada iteración de
// f llamamos a aux, entonces f es O(M^2)

//c.

//O(M)
int sumasAcumuladas(vector<vector<int>> &m){
	int suma = 0;
	for(int i=0; i<m.size(); i++){
		for(int j=0; j<m[0].size(); j++){
			suma+=m[i][j];
			m[i][j]=suma;
		}
	}
}


/*************************(Ejercicio 2)*************************/

//a.Resolver el problema con un tiempo de ejecución en el peor caso de O(n^2)
int charToInt (char a){
	return a;
}

void swap (string &palabra, int i, int j){
	char aux = palabra[i];
	palabra[i] = palabra[j];
	palabra[j] = aux;
}

void bubbleSort(string &palabra){
	for(int i=0; i<palabra.size(); i++){
		for(int j=0; j<palabra.size(); j++){
			if(charToInt(palabra[j])>charToInt(palabra[i])){
				swap(palabra, i, j);
			}
		}
	}
}

//O(n)
int encontrarMinimoDesde (string s, int desde){
	int minimo = 0;
	for(int i=desde; i<s.size(); i++){
		if(charToInt(s[i])<charToInt(s[minimo])){
			minimo = i;
		}
	}
	return minimo;
}

//O(n^2)
void selectionSort (string &s){
	for(int i=0; i<s.size(); i++){
		int minimo = encontrarMinimoDesde(s, i);
		swap(s, i, minimo);
	}
}

void ordenarString (string &s){
	selectionSort(s);
}

//O(n^2)
bool sonPermutacion (string v, string w){
	string vOrdenado = v;
	string wOrdenado = w;
	ordenarString(vOrdenado);
	ordenarString(wOrdenado);
	return vOrdenado==wOrdenado;
}


//b. Resolver el problema con un tiempo de ejecución en el peor caso de O(min(|v|, |w|)) sabiendo que v y w estan compuestas 
//   con caracteres de la 'a' a la 'z'

bool sonPermutacion (string v, string w){
	vector<int> indiceV(26);// considerando que 'a' es el elemento 0 y 'z' el elemento 25
	vector<int> indiceW(26);
	for(int i=0; i<v.size(); i++){
		indiceV[charToInt(v[i])]++;
	}
	for(int j=0; j<w.size(); j++){
		indiceW[charToInt(w[j])]++;
	}
	return indiceV==indiceW;
}

bool sonPermutacion_2 (string v, string w){
	vector<int> indiceV(26);// considerando que 'a' es el elemento 0 y 'z' el elemento 25
	vector<int> indiceW(26);
	bool res = true;
	if(v.size()!=w.size()){
		res = false;
	}
	for(int i=0; !res && i<v.size(); i++){
		indiceV[charToInt(v[i])]++;
	}
	for(int j=0; !res && j<w.size(); j++){
		indiceW[charToInt(w[j])]++;
	}
	return res && indiceV==indiceW;
}


bool sonPermutacion_3 (string v, string w){
	vector<int> indiceV(26);// considerando que 'a' es el elemento 0 y 'z' el elemento 25
	vector<int> indiceW(26);
	bool res = true;
	if(v.size()==w.size()){
		for(int i=0; i<v.size(); i++){
			indiceV[charToInt(v[i])]++;
		}
		for(int j=0; j<w.size(); j++){
			indiceW[charToInt(w[j])]++;
		}
	} else {
		res = false;
	}
	return res && indiceV==indiceW;
}