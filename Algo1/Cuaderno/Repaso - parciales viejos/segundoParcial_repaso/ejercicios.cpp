#include "ejercicios.h"
#include <vector>
#include <string>

int devolverTres (int n){
    return 3;    
}

/*************************(segundo parcial 15-11-20)*************************/
//Ejercicio 2

bool hayInterseccion(vector<int> s, vector<int> t){
	int i=s.size();
	int j=t.size();
	while (i>1 && j>1 && t[j-1]!=s[i-1]){
		if(s[i-1]>t[j-1]){
			i=i-1;
		}else{
			j=j-1;
		}
	}
	while(j>1 && t[j-1]!=s[i-1]){
		j=j-1;
	}
	while(i>1 && t[j-1]!=s[i-1]){
		i=i-1;
	}
	return t[j-1]==s[i-1];
}

//Ejercicio 3
int menorDivisor(int n){
	int res=-1;
	for(int i=0; res==-1 && i<n/2; i++){
		if(n%i==0){
			res = i;
		}
	}
	return res;
}

bool algunProductoEsDividido(vector<int> s, int n){
	int p = menorDivisor(n); //O(n)
	int q = n%p;
	int existeElemConP = false;
	int existeElemConQ = false;
	for(int i=0; (!existeElemConP || !existeElemConQ) && i<s.size(); i++){ //O(|s|)
		if(s[i]%p==0){
			existeElemConP = true;
		}
		if(s[i]%q==0){
			existeElemConQ = true;
		}
	}
	return existeElemConP && existeElemConQ;
}

//Ejercicio 4
int cuantosEnComun (vector<int> A, vector<int> B){
	//ordenar(A);
	//ordenar(B);
	int i=0;
	int j=0;
	int res=0;
	while(i<A.size() && j<B.size()){
		if(A[i]>B[j]){
			j++;
		} else if (A[i]<B[j]){
			//A[i] no existe en B
			i++;
		} else {
			//lo encontré, A[i]==B[j]
			i++;
			j++;
			res++;
		}
	}
	return res;
}
