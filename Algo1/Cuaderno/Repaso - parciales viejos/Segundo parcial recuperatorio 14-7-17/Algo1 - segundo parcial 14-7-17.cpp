/*************************(Ejercicio 3)*************************/

vector<int> consecutivosNoContenidos (vector<int> a){
	ordenar(a);
	int saltoMaximo = 0;
	int saltoMaximoPos = 0;
	for(int i=0; i<a.size()-1; i++){
		int saltoActual = abs(a[i]-a[i+1])
		if(saltoActual>saltoMaximo){
			saltoMaximo = saltoActual;
			saltoMaximoPos = i;
		}
	}
	vector<int> res;
	for(int j=1; j<saltoMaximo; j++){
		int elem = a[saltoMaximoPos]+j;
		res.push_back(elem);
	}
	return res;
}

/*************************(Ejercicio 4)*************************/
//¿La escalera horizontal debe ser estrictamente creciente de izquierda a derecha y la escalera vertical de arriba hacia abajo?

bool hayEscaleraDeNEscalones (vector<vector<int>> m, int n){  
	return hayEscaleraHorizontal(m, n) || hayEscaleraVertical(m, n);
}

bool hayEscaleraHorizontal(vector<vector<int>> m, int n){
	bool encontre = false;
	for(int i=0; !encontre && i<m.size(); i++){
		int escalones = n;
		for(int j=0; !encontre && j<m[0].size()-1; j++){
			if(m[i][j]<m[i][j+1]){
				escalones--;
				if(escalones==0){
					encontre = true;
				}
			} else {
				escalones = n;
			}
		}
	}
	return encontre;
}

bool hayEscaleraVertical(vector<vector<int>> m, int n){
	bool encontre = false;
	for(int j=0; !encontre && j<m[0].size(); j++){
		int escalones = n;
		for(int i=0; !encontre && i<m.size()-1; i++){
			if (m[i][j]<m[i+1][j]){
				escalones--;
				if(escalones==0){
					encontre=true;
				}
			} else {
				escalones = n;
			}
		}
	}
	return encontre;
}