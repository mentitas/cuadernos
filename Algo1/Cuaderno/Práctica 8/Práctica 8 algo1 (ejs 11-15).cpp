// Ejercicio 10
// 2) Dar un programa que resuelva sumasAcumuladas cuyo tiempo de ejecución en peor caso sea O(n^2)

void sumasAcumuladas(vector<vector<int>> &m, int N){
	int suma = 0;
	for(int i=0; i<N-1; i++){
		for(int j=0; j<N-1; j++){
			m[i][j]+=suma;
			suma = m[i][j];
		}
	}
}

// Ejercicio 11
// Dada una secuencia de n números enteros, dar un programa que encuentre la máxima cantidad de elementos
// impares consecutivos cuya tiempo de ejecución de peor caso pertenezca a O(n).

int encontrarImparesConsecutivos(vector<int> v){
	int maximo = 0;
	int actual = 0;
	for(int i=0; i<v.size(); i++){
		if(v[i]%2==1){
			actual++;
		}else{
			actual=0;
		}
		if(actual>max){
			maximo = actual;
		}
	}
	return maximo;
}

// Ejercicio 12
// Escribir un programa que sea correcto respecto de la siguiente especificación, y cuyo tiempo de ejecución de
// peor caso pertenezca a O(|s|).

// proc restarAcumulado (in s: seq<Z>, in x: Z, out res: seq<Z>) {
// Pre  {True}
// Post {|res|=|s| ∧L (∀i:Z)(0 ≤ i < |s| →L res[i] = x − sum(j=0, i)[s[j]])}
// }

// Ejemplo: restarAcumulado({1,2,3,4,5}, 10) = {10-1, 10-(1+2), 10-(3+2+1), 10-(4+3+2+1), 10-(5+4+3+2+1)} = {9, 7, 4, 0, -5}

vector<int> restarAcumulado(vector<int> s, int x){
	int suma = 0;
	vector<int> res = v;
	for(int i=0; i<v.size(); i++){
		suma+=v[i];
		res[i]=x-suma;
	}
	return res;
}

// Ejercicio 13
// Sea m una matriz de N × N donde cada posición contiene el costo de recorrer dicha posición (dicho costo es
// positivo para todas las posiciones). Asumiendo que la única forma de recorrer la matriz es avanzando hacia abajo y hacia
// la derecha, escribir un programa que calcule el mı́nimo costo para llegar desde la posición (0, 0) hasta la posición (N-1, N-1) y
// cuyo tiempo de ejecución de peor caso pertenezca a O(N^2).

// Ejemplo: m = {$10, $90, $90, $90}
//              {$10, $10, $90, $90}
//              {$90, $10, $10, $90}
//              {$90, $90, $10, $10}
//
//              RTA: $70

// Tengo (2N)!/((N!*N!) formas de recorrerla
// Por ejemplo, si N=4, hay 70 formas

// vector<int> recorrerGastandoMenos(vector<vector<int>> m){
// 	vector<vector<int>> recorridosPosibles = armarCombinaciones(m.size());

// 	int costo = 0;
// 	int costoMinimo = 0;
// 	for(int i=0; i<recorridosPosibles.size(); i++){
// 		vector<int> recorrido = recorridosPosibles[i];
// 		pair<int,int> posicionActual = make_pair(0,0);
		
// 		for(int j=0; j<recorrido.size(); j++){
// 			int movimiento = recorrido[j];
			
// 			if(movimiento==0){
// 				posicion.second++; //me muevo a la derecha
// 			} else {
// 				posicion.first++; //me muevo a hacia abajo
// 			}
			
// 			costo+=m[posicion.first][posicion.second];
// 		}

// 		if(costoMinimo==0){
// 			costoMinimo = costo;
// 		}

// 		if(costo<costoMinimo){
// 			costoMinimo = costo;
// 		}

// 	}
// }

//No se hacerlo


// Ejercicio 14.
// Sea A una matriz cuadrada y n un número natural.
// a) Escribir un programa que calcule (productoriaDesde i=1 hasta n) de A (es decir A^n) (reutilizar el programa del ejercicio 9)
// ¿Cuál es el tiempo de ejecución de peor caso de esta función?
// b) Resolver el punto anterior suponiendo que n = 2^m (n potencia de 2) ¿Se pueden reutilizar cuentas ya realizadas? ¿Cuáles el tiempo
// de ejecución de peor caso para cada programa?

/*** a ***/

//Aclaración: |m|=N

//O(N^3)
vector<vector<int>> multiplicar(vector<vector<int>> m1, vector<vector<int>> m2){
	vector<vector<int>> res(m1.size(), vector<int>(m2[0].size()));
	for(int i=0; i<m1.size(); i++){
		for(int j=0; j<m2[0].size(); j++){
			for (int k=0; k<m1[0].size(); k++){
				res[i][j]+=m1[i][k]*m2[k][j];
			}
		}
	}
	return res;
}

//O(n*N^3) = ¿O(N^4)?
vector<vector<int>> potenciaDeMatriz(vector<vector<int>> m, int n){
	if(n>1){
		return potenciaDeMatriz(multiplicar(m,m), n-1);
	} else {
		return m;
	}
}



/*** b ***/

//O(N^3)
vector<vector<int>> cuadradoDeMatriz(vector<vector<int>> m){
	return multiplicar(m,m);
}

//O(log_2(n) * N^3) = O(N^3)
vector<potencia<int>> potenciaDeMatriz_2(vector<vector<int>> m, int n){
	if(n>1){
		return potenciaDeMatriz_2(cuadradoDeMatriz(m), n/2);
	} else {
		return m;
	}
}

//*** RTA: el segundo programa tiene un menor tiempo de ejecución ***



// Ejercicio 15
// Dada una matriz de booleanos de n filas y m columnas con n impar. Se sabe que hay exactamente una fila que no está
// repetida, y el resto se encuentra exactamente dos veces en la matriz.
// a) Escribir un programa que devuelva un vector con los valores de la fila que no se repite. ¿Cuál es su tiempo de ejecución
// de peor caso descripto ?
// b) ¿Es posible un programa que recorra cada casillero de la matriz sólo una vez? En caso de no haberlo resuelto con esta
// restricción, modificar el programa para que la cumpla. ¿Cuál es su tiempo de ejecución de peor caso ?
// c) La solución al punto anterior, ¿utiliza vectores auxiliares?, en caso que lo haga, escribir un programa que no los necesite.

//Aclaración: |m| = n


/*** a ***/

//O(n^3)
vector<bool> encuentroNoRepe_1 (vector<vector<bool>> m){
	vector<bool> indice(m.size(), false);
	for(int i=0; i<m.size(); i++)           //
		for(int j=i+1; j<m.size(); j++){    // O(n^3) (asumiendo que igualar dos vectores es O(n))
			if(i!=j && m[i]==m[j]){
				indice[i]=true;
				indice[j]=true;
			}
		}
	}
	vector<bool> res;
	for(int k=0; k<indice.size(); k++){    // O(n)
		if(!indice[k]){
			res = m[k];
		}
	}
	return res;
}


/*** b ***/

//O(n^2)
vector<bool> encuentroNoRepe_2 (vector<vector<bool>> m){
	vector<bool> res;
	for(int c=0; c<m[0].size(); c++){
		int suma=0;
		for(int f=0; f<m.size(); f++){
			suma+=m[f][c];
		}
		if(suma%2==0){
			res.push_back(false);
		} else {
			res.push_back(true);
		}
	}
	return res;
}


/*** c ***/

//O(n^2)
vector<bool> encuentroNoRepe_3 (vector<vector<bool>> &m){
	for(int c=0; c<m[0].size(); c++){
		int suma=0;
		for(int f=0; f<m.size(); f++){
			suma+=m[f][c];
		}
		if(suma%2==0){
			m[0][c]=(false);
		} else {
			m[0][c]=(true);
		}
	}
	return m[0];
}