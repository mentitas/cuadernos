from scapy.all import *

dump_de_frames =rdpcap("./dump_prueba.pcap")

# Defino variables
packages            = 0;
packages_with_ether = 0;
dst_dirs  = {}
protocols = {}

# Código principal
for p in dump_de_frames:
    packages += 1
    if p.haslayer(Ether):
    	packages_with_ether += 1

print("Paquetes con Ether: ", packages_with_ether)
print("Paquetes totales:   ", packages)
